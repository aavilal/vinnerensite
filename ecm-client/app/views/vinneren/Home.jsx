import React from 'react';
import AppActions from '../../flux/actions/AppActions.jsx';
import {InputText} from 'primereact/components/inputtext/InputText';
import { Link } from 'react-router-dom';
import { Growl } from 'primereact/components/growl/Growl';
import $ from 'jquery';

export default class Home extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			nameContact: '',
			nameContactError: '',
			emailContact: '',
			emailContactError: '',
			messageContact: '',
			messageContactError: ''
		}
		this.handleClickContact = this.handleClickContact.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleKeyUp = this.handleKeyUp.bind(this);
		this.validateEmail = this.validateEmail.bind(this);
		this.handleClickSend = this.handleClickSend.bind(this);
	}
	componentDidUpdate() {
		const {success, message} = this.props.data.contactus;
		if (success != null) {
			if (success)
				this.growl.show({severity: 'success', summary: 'Success', detail: message});
			else
				this.growl.show({severity: 'error', summary: 'Error', detail: message});

			AppActions.changeValue(['contactus', 'success'], null);
			AppActions.changeValue(['contactus', 'message'], '');

		}
	}
	handleClickContact(e) {
		let target =  $('.' + e);
		$('.main-view').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
    });
	}
	handleClickSend() {
		const {nameContact, emailContact, emailContactError, messageContact} = this.state;
		if (nameContact.length == 0)
			this.setState({ nameContactError: 'Required field' });
		if (emailContact.length == 0)
			this.setState({ emailContactError: 'Required field' });
		if (messageContact.length == 0)
			this.setState({ messageContactError: 'Required field' });

		if (emailContact.length > 0 && !this.validateEmail(emailContact)) {
			this.setState({ emailContactError: 'Invalid email format' });
		}

		let data = {
			email: emailContact,
			name: nameContact,
			message: messageContact
		}
		if (nameContact.length > 0 && emailContact.length > 0 && messageContact.length > 0 && emailContactError.length == 0) {
			AppActions.sendContactUs(data);
		}
	}
	handleChange(event) {
		let objState = {};
		this.setState({
			nameContactError: '',
			emailContactError: '',
			messageContactError: ''
		});
		objState[event.target.name] = event.target.value;
		this.setState(objState);
		if (event.target.name == 'emailContact') {
			if (!this.validateEmail(event.target.value)) {
				this.setState({
					emailContactError: 'Invalid email format'
				});
			}
		}
	}

	handleKeyUp(event) {
		if (event.keyCode == 13) {
			this.handleClickSend();
		}
	}
	validateEmail(emailtxt) {
		let regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (regexp.test(emailtxt)) {
			return true;
		}
		return false;
	}
	render() {
		let styleBanner = {
			backgroundImage: 'url(./images/vinneren-background.png)',
		}
		const {
			nameContact,
			nameContactError,
			emailContact,
			emailContactError,
			messageContact,
			messageContactError
		} = this.state;
		return (
			<div className="vinneren-view">
				<div className="vinneren-banner">
					<span className="vinneren-banner-title">Solutions Converted into Technology</span>
					<span className="vinneren-banner-text">Convert the needs of your business into scalable technology that allows you to perform strategic and growth activities instead of operational.</span>
					<div className="vinneren-banner-btn">
						<Link to="/#contact-us" onClick={()=>this.handleClickContact('vinneren-contact')}>
							<div>Contact Us</div>
						</Link>
					</div>
				</div>
				<div className="vinneren-whyus">
					<div className="vinneren-whyus-section">
						<span className="vinneren-whyus-section-title">Why us?</span>
						<span className="vinneren-whyus-section-text">Save costs with technology based on your abilities and needs.</span>
					</div>
					<div className="vinneren-whyus-section">
						<span className="vinneren-whyus-section-title">Why us?</span>
						<span className="vinneren-whyus-section-text">Consultant team with more than 10 years of experience, with best practices and market trends.</span>
					</div>
					<div className="vinneren-whyus-section">
						<span className="vinneren-whyus-section-title">Why us?</span>
							<span className="vinneren-whyus-section-text">Development team with the best working methodologies and technologies that are today.</span>
					</div>
				</div>
				<div className="vinneren-services">
					<div className="vinneren-services-title">
						Our Services
					</div>
					<div className="vinneren-services-section">
						<img src="/images/vinneren-service.png" />
						<span className="vinneren-services-section-title">Cloud Solutions</span>
						<span className="vinneren-services-section-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vestibulum enim eu mi lacinia, nec placerat neque pharetra. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
					</div>
					<div className="vinneren-services-section">
						<img src="/images/vinneren-service.png" />
						<span className="vinneren-services-section-title">Managed IT Services</span>
						<span className="vinneren-services-section-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vestibulum enim eu mi lacinia, nec placerat neque pharetra. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
					</div>
					<div className="vinneren-services-section">
						<img src="/images/vinneren-service.png" />
						<span className="vinneren-services-section-title">Disaster Recovery</span>
						<span className="vinneren-services-section-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vestibulum enim eu mi lacinia, nec placerat neque pharetra. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
					</div>
					<div className="vinneren-services-section">
						<img src="/images/vinneren-service.png" />
						<span className="vinneren-services-section-title">Cloud Desktop</span>
						<span className="vinneren-services-section-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vestibulum enim eu mi lacinia, nec placerat neque pharetra. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
					</div>
					<div className="vinneren-services-section">
						<img src="/images/vinneren-service.png" />
						<span className="vinneren-services-section-title">Network Solutions</span>
						<span className="vinneren-services-section-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vestibulum enim eu mi lacinia, nec placerat neque pharetra. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
					</div>
					<div className="vinneren-services-section">
						<img src="/images/vinneren-service.png" />
						<span className="vinneren-services-section-title">Support Consulting</span>
						<span className="vinneren-services-section-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vestibulum enim eu mi lacinia, nec placerat neque pharetra. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
					</div>
				</div>
				<div className="vinneren-products">
					<div className="vinneren-products-title">
						Products
					</div>
					<div className="vinneren-products-group-section">
						<div className="vinneren-products-section">
							<img src="/images/ecm-logo-dark.png" />
							<span className="vinneren-products-section-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vestibulum enim eu mi lacinia, nec placerat neque pharetra. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
							<div className="vinneren-products-section-btn">
								<Link to="/products/ecloud-messaging">
								<div>Read more</div>
								</Link>
							</div>
						</div>
					</div>
				</div>
				<div className="vinneren-contact">
					<div className="vinneren-contact-title">
						Contact Us
					</div>
					<div className="contact-form">
						<div className="contact-formelement">
							<span className="ui-float-label">
								<InputText name="nameContact"
													type="text"
													value={nameContact}
													className={(nameContact == '' ? '' : 'ui-state-filled') + (nameContactError == '' ? '' : ' error-input')}
													onChange={this.handleChange} onKeyUp={this.handleKeyUp}
													/>
								<label>Name</label>
							</span>
							<span className="contact-error">{nameContactError}</span>
						</div>
						<div className="contact-formelement">
							<span className="ui-float-label">
								<InputText name="emailContact"
													type="text"
													value={emailContact}
													className={(emailContact == '' ? '' : 'ui-state-filled') + (emailContactError == '' ? '' : ' error-input')}
													onChange={this.handleChange} onKeyUp={this.handleKeyUp}
													/>
								<label>Email</label>
							</span>
							<span className="contact-error">{emailContactError}</span>
						</div>
						<div className="contact-formelement">
							<span className="ui-float-label">
								<InputText name="messageContact"
													type="text"
													value={messageContact}
													className={(messageContact == '' ? '' : 'ui-state-filled') + (messageContactError == '' ? '' : ' error-input')}
													onChange={this.handleChange} onKeyUp={this.handleKeyUp}
													/>
								<label>Message</label>
							</span>
							<span className="contact-error">{messageContactError}</span>
						</div>
					</div>
					<div className="vinneren-contact-btn">
						<div onClick={this.handleClickSend}>Send</div>
					</div>
				</div>
				<Growl ref={(el) => { this.growl = el; }}></Growl>
			</div>
		);
	}
}
