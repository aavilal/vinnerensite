import React from 'react';
import AppActions from '../../flux/actions/AppActions.jsx';
import { DataGrid } from 'primereact/components/datagrid/DataGrid';
import { Panel } from 'primereact/components/panel/Panel';
import $ from 'jquery';

export default class Subscribers extends React.Component {
	constructor(props) {
		super(props);
		this.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		this.itemSubscriberTemplate = this.itemSubscriberTemplate.bind(this);
		this.formatDate = this.formatDate.bind(this);
		this.formatString = this.formatString.bind(this);
		this.formatNumber = this.formatNumber.bind(this);
	}
	componentDidMount() {
		AppActions.getSubscribers();
	}
	formatDate(date) {
		let dateObj = new Date(date);
		let dateTxt = this.months[dateObj.getMonth()] + ' ' + dateObj.getDate() + ', ' + dateObj.getFullYear();
		return dateTxt;
	}
	formatString(data) {
		return data.length > 25 ? data.substring(0, 25) + '...' : data;
	}
	formatNumber(number) {
		return (number + '').replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	}
	itemSubscriberTemplate(data) {
		if (!data)
    	return;
		let styleAvatar = {
			backgroundImage: 'url(./images/avatar.jpg)'
		}
		return (
			<div style={{ padding: '5px' }} className="ui-g-12 ui-md-3">
				<Panel header={this.formatDate(data.dateRegistered)} style={{ textAlign: 'center', background: '#FFFFFF', width: '300px', margin: '0 auto', padding: '0px' }}>
					<div className="item-img">
						<div className="account-avatar" style={styleAvatar} />
					</div>
					<div className="item-content">
						<div>
							<label>OS:</label>
							<span>{this.formatString(data.osName + ' ' + data.osVersion)}</span>
						</div>
						<div>
							<label>Browser:</label>
							<span>{this.formatString(data.browserName + ' ' + data.browserVersion)}</span>
						</div>
						<div>
							<label>Token:</label>
							<span>{this.formatString(data.token)}</span>
						</div>
					</div>
				</Panel>
			</div>
		);
	}

	render() {
		let es = {
			firstDayOfWeek: 1,
			dayNames: ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
    	dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
    	dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    	monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    	monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
		};
		return (
			<div className="subscribers-notify-view">
				<h2>Subscribers</h2>
				<div className="subscribers-table">
					{this.props.data.subscribers == null || this.props.data.subscribers.length == 0 ? null :
					<DataGrid value={this.props.data.subscribers}
										itemTemplate={this.itemSubscriberTemplate}
										paginator={this.props.data.subscribers != null && this.props.data.subscribers.length > 20 ? true : false} rows={20}
										/> }
				</div>
			</div>
		);
	}
}
