import React from 'react';
import AppActions from '../../flux/actions/AppActions.jsx';
import { Growl } from 'primereact/components/growl/Growl';
import { FileUpload } from 'primereact/components/fileupload/FileUpload';
import { InputText } from 'primereact/components/inputtext/InputText';
import { Button } from 'primereact/components/button/Button';
import $ from 'jquery';

export default class QuickNotify extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			notification: null,
			notificationMessage: '',
			titleNotify: '',
			titleNotifyError: '',
			bodyNotify: '',
			bodyNotifyError: '',
			urlNotify: '',
			urlNotifyError: '',
			imgNotifyError: '',
			iconNotifyError: '',
		}
		this.handleChange = this.handleChange.bind(this);
		this.handleKeyUp = this.handleKeyUp.bind(this);
		this.onBeforeUpload = this.onBeforeUpload.bind(this);
		this.onUpload = this.onUpload.bind(this);
		this.onUploadError = this.onUploadError.bind(this);
		this.handleClickSend = this.handleClickSend.bind(this);
		this.deleteFile = this.deleteFile.bind(this);
		this.validateUrl = this.validateUrl.bind(this);
		this.handleClickPreview = this.handleClickPreview.bind(this);
		this.handleClickDialog = this.handleClickDialog.bind(this);
		this.handleClickClose = this.handleClickClose.bind(this);
		this.handleClickEnable = this.handleClickEnable.bind(this);
	}
	componentDidMount() {
		AppActions.changeValue(['notify', 'bucketImgUrl'], '0');
		AppActions.changeValue(['notify', 'bucketImgName'], '0');
		AppActions.changeValue(['notify', 'imgName'], '');
		AppActions.changeValue(['notify', 'bucketIconUrl'], '0');
		AppActions.changeValue(['notify', 'bucketIconName'], '0');
		AppActions.changeValue(['notify', 'iconName'], '');
	}
	componentDidUpdate() {
		const {success, message} = this.props.data.notify;
		if (success != null && message.length > 0) {
			if (success)
				this.growl.show({severity: 'success', summary: 'Success', detail: message});
			else
				this.growl.show({severity: 'error', summary: 'Error', detail: message});

			AppActions.changeValue(['notify', 'success'], null);
			AppActions.changeValue(['notify', 'message'], '');
		}
	}
	deleteFile(type) {
		if (type == 'img') {
			AppActions.changeValue(['notify', 'bucketImgUrl'], '0');
			AppActions.changeValue(['notify', 'bucketImgName'], '0');
			AppActions.changeValue(['notify', 'imgName'], '');
		} else {
			AppActions.changeValue(['notify', 'bucketIconUrl'], '0');
			AppActions.changeValue(['notify', 'bucketIconName'], '0');
			AppActions.changeValue(['notify', 'iconName'], '');
		}
	}
	handleChange(event) {
		let objState = {};
		this.setState({
			titleNotifyError: '',
			iconNotifyError: '',
			bodyNotifyError: '',
			urlNotifyError: '',
			imgNotifyError: ''
		});
		objState[event.target.name] = event.target.value;
		this.setState(objState);
		if (event.target.name == 'urlNotify') {
			if (!this.validateUrl(event.target.value)) {
				this.setState({
					urlNotifyError: 'Invalid url format'
				});
			}
		}
	}
	handleKeyUp(event) {
		if (event.keyCode == 13) {
			this.handleClickSend();
		}
	}
	onBeforeUpload(event) {
		AppActions.changeValue(['loading'], true);
	}
	onUpload(event) {
		this.iconNotifyFU.fileInput.style.display = '';
		this.imgNotifyFU.fileInput.style.display = '';
		let res = JSON.parse(event.xhr.response);
		if (res.type == 'imgNotifyFU') {
			this.setState({
				imgNotifyError: '',
			});
			AppActions.changeValue(['notify', 'bucketImgUrl'], res.bucketImgUrl);
			AppActions.changeValue(['notify', 'bucketImgName'], res.bucketImgName);
			AppActions.changeValue(['notify', 'imgName'], res.imgName);
		} else {
			this.setState({
				iconNotifyError: '',
			});
			AppActions.changeValue(['notify', 'bucketIconUrl'], res.bucketImgUrl);
			AppActions.changeValue(['notify', 'bucketIconName'], res.bucketImgName);
			AppActions.changeValue(['notify', 'iconName'], res.imgName);
		}
		AppActions.changeValue(['loading'], false);
		this.growl.show({severity: 'success', summary: 'Success', detail: 'File Uploaded.'});

	}
	onUploadError(event) {
		this.iconNotifyFU.fileInput.style.display = '';
		this.imgNotifyFU.fileInput.style.display = '';
		let res = {
			message: "Couldn't upload image"
		}
		try {
			res = JSON.parse(event.xhr.response);
			if (res.type == 'imgNotifyFU') {
				this.setState({
					imgNotifyError: 'Error on uploading image.',
				});
				AppActions.changeValue(['notify', 'bucketImgUrl'], '0');
				AppActions.changeValue(['notify', 'bucketImgName'], '0');
				AppActions.changeValue(['notify', 'imgName'], '');
			} else {
				this.setState({
					iconNotifyError: 'Error on uploading image.',
				});
				AppActions.changeValue(['notify', 'bucketIconUrl'], '0');
				AppActions.changeValue(['notify', 'bucketIconName'], '0');
				AppActions.changeValue(['notify', 'iconName'], '');

			}
		} catch(err){}

		AppActions.changeValue(['loading'], false);

		this.growl.show({severity: 'error', summary: 'Error', detail: res.message});
	}
	handleClickPreview() {
		let _this = this;
		let timeOut = $('.dashboard-view').scrollTop() > 0 ? 1000 : 0;
		if (!("Notification" in window)) {
			$('.dashboard-view').animate({
	          scrollTop: 0
	        }, timeOut, function() {
						_this.setState({
							notification: 'nosupport',
							notificationMessage: 'This browser does not support desktop notification.' });

						$(_this.refs.dialog).fadeIn('slow');
	    });
  	} else if (Notification.permission === 'granted') {
			this.setState({
				notification: 'granted',
				notificationMessage: '' });

				const {titleNotify, bodyNotify, urlNotify, urlNotifyError} = this.state;
				if (titleNotify.length == 0)
					this.setState({ titleNotifyError: 'Required field' });
				if (bodyNotify.length == 0)
					this.setState({ bodyNotifyError: 'Required field' });
				if (urlNotify.length == 0)
					this.setState({ urlNotifyError: 'Required field' });
				if (this.props.data.notify.iconName.length == 0)
					this.setState({ iconNotifyError: 'Required field' });

				if (urlNotify.length > 0 && !this.validateUrl(urlNotify)) {
					this.setState({ urlNotifyError: 'Invalid url format' });
				}

				if (titleNotify.length > 100)
					this.setState({ titleNotifyError: 'Maximum 100 characters' });
				if (bodyNotify.length > 200)
					this.setState({ bodyNotifyError: 'Maximum 200 characters' });
				if (urlNotify.length > 200)
					this.setState({ urlNotifyError: 'Maximum 200 characters' });

				if (titleNotify.length > 0 && bodyNotify.length > 0 && urlNotify.length > 0 && urlNotifyError.length == 0 && this.props.data.notify.iconName.length > 0 && titleNotify.length <= 100 && bodyNotify.length <= 200 && urlNotify.length <= 200) {
					const notificationTitle = titleNotify;
					const notificationOptions = {
						body: bodyNotify,
						icon: _this.props.data.notify.bucketIconUrl,
						image: _this.props.data.notify.bucketImgUrl,
						vibrate: [200, 100, 200, 100, 200, 100, 200]
					};

						new Notification(notificationTitle, notificationOptions);
				}
		} else if (Notification.permission !== 'denied') {
			$('.dashboard-view').animate({
	          scrollTop: 0
	        }, timeOut, function() {
						_this.setState({
							notification: 'default',
							notificationMessage: 'Enable notifications in your settings browser to see preview notification.' });

						$(_this.refs.dialog).fadeIn('slow');
	    });
		} else {
			$('.dashboard-view').animate({
	          scrollTop: 0
	        }, timeOut, function() {
						_this.setState({
							notification: 'denied',
							notificationMessage: 'You have denied notifications in your browser, enable notifications in your browser to see preview notification.' });

						$(_this.refs.dialog).fadeIn('slow');
	    });
		}
	}
	handleClickDialog(ev) {
		if (ev.target.className.indexOf('message-dialog') != -1) {
			this.handleClickClose();
		}
	}
	handleClickClose() {
		$(this.refs.dialog).fadeOut('slow');
	}
	handleClickEnable() {
		let _this = this;
		$(this.refs.dialog).fadeOut('slow');
		Notification.requestPermission((permission) => {
			if (permission === 'granted') {
        _this.handleClickPreview();
      }
		});
	}
	handleClickSend() {
		const {titleNotify, bodyNotify, urlNotify, urlNotifyError} = this.state;
		if (titleNotify.length == 0)
			this.setState({ titleNotifyError: 'Required field' });
		if (bodyNotify.length == 0)
			this.setState({ bodyNotifyError: 'Required field' });
		if (urlNotify.length == 0)
			this.setState({ urlNotifyError: 'Required field' });
		if (this.props.data.notify.iconName.length == 0)
			this.setState({ iconNotifyError: 'Required field' });

		if (urlNotify.length > 0 && !this.validateUrl(urlNotify)) {
			this.setState({ urlNotifyError: 'Invalid url format' });
		}

		if (titleNotify.length > 100)
			this.setState({ titleNotifyError: 'Maximum 100 characters' });
		if (bodyNotify.length > 200)
			this.setState({ bodyNotifyError: 'Maximum 200 characters' });
		if (urlNotify.length > 200)
			this.setState({ urlNotifyError: 'Maximum 200 characters' });

		let data = {
			title: titleNotify,
			body: bodyNotify,
			url: urlNotify
		}
		if (titleNotify.length > 0 && bodyNotify.length > 0 && urlNotify.length > 0 && urlNotifyError.length == 0 && this.props.data.notify.iconName.length > 0 && titleNotify.length <= 100 && bodyNotify.length <= 200 && urlNotify.length <= 200)
			AppActions.sendNotify(data);

	}
	validateUrl(url) {
		let regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
    if (regexp.test(url)) {
			return true;
		}
		return false;
	}
	render() {
		return (
			<div className="quick-notify-view">
				<h2>Quick Notify</h2>
				<div className="quick-notify-card">
					<div className="quick-notify-card-header">
						<span>Info</span>
					</div>
					<div className="quick-notify-card-body">
						<div className="quick-notify-card-element">
							<span className="ui-float-label">
								<InputText name="titleNotify"
													type="text"
													className={(this.state.titleNotify == '' ? '' : 'ui-state-filled') + (this.state.titleNotifyError == '' ? '' : ' error-input')}
													onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
								<label>title</label>
							</span>
							<span className="notify-error">{this.state.titleNotifyError}</span>
						</div>
						<div className="quick-notify-card-element">
							<span className="ui-float-label">
								<InputText name="iconNotify"
													type="text"
													className={this.state.iconNotifyError == '' ? '' : 'error-input'}
													value={this.props.data.notify.iconName} onKeyUp={this.handleKeyUp}/>
								<label>icon</label>
								<i class="material-icons">&#xE226;</i>
								{this.props.data.notify.iconName == '' ? null : <i class="material-icons deletefile" onClick={(e) => this.deleteFile('icon')}>&#xE872;</i>}
								<FileUpload name="iconNotifyFU" url={"/api/upload/image/" + this.props.data.notify.bucketIconName}
														onUpload={this.onUpload} onError={this.onUploadError}
														onBeforeUpload={this.onBeforeUpload} auto={true}
														accept="image/*" mode="basic" chooseLabel="" readonly
														ref={(el) => { this.iconNotifyFU = el; }}/>
							</span>
							<span className="notify-error">{this.state.iconNotifyError}</span>
						</div>
						<div className="quick-notify-card-element">
							<span className="ui-float-label">
								<InputText name="bodyNotify"
													type="text"
													className={(this.state.bodyNotify == '' ? '' : 'ui-state-filled') + (this.state.bodyNotifyError == '' ? '' : ' error-input')}
													onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
								<label>body</label>
							</span>
							<span className="notify-error">{this.state.bodyNotifyError}</span>
						</div>
						<div className="quick-notify-card-element">
							<span className="ui-float-label">
								<InputText name="urlNotify"
													type="text"
													className={(this.state.urlNotify == '' ? '' : 'ui-state-filled') + (this.state.urlNotifyError == '' ? '' : ' error-input')}
													onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
								<label>url</label>
							</span>
							<span className="notify-error">{this.state.urlNotifyError}</span>
						</div>
						<div className="quick-notify-card-element last-element">
							<span className="ui-float-label">
								<InputText name="imgNotify"
													type="text"
													className={this.state.imgNotifyError == '' ? '' : 'error-input'}
													value={this.props.data.notify.imgName} onKeyUp={this.handleKeyUp}/>
								<label>image</label>
								<i class="material-icons">&#xE226;</i>
								{this.props.data.notify.imgName == '' ? null : <i class="material-icons deletefile" onClick={(e) => this.deleteFile('img')}>&#xE872;</i>}
								<FileUpload name="imgNotifyFU" url={"/api/upload/image/" + this.props.data.notify.bucketImgName}
														onUpload={this.onUpload} onError={this.onUploadError}
														onBeforeUpload={this.onBeforeUpload} auto={true}
														ref={(el) => { this.imgNotifyFU = el; }}
														accept="image/*" mode="basic" chooseLabel="" readonly/>
							</span>
							<span className="notify-error">{this.state.imgNotifyError}</span>
						</div>
						<div className="quick-notify-card-element first-element">
							<Button label="Preview" onClick={this.handleClickPreview}>
							</Button>
						</div>
						<div className="quick-notify-card-element">
							<Button label="Send" onClick={this.handleClickSend}>
							</Button>
						</div>
					</div>
				</div>
				<div ref="dialog" className="message-dialog hide" onClick={this.handleClickDialog}>
					<div className="message-card">
						<div className="message-card-header">
							<span>Message</span>
							<i class="material-icons" onClick={this.handleClickClose}>&#xE5CD;</i>
						</div>
						<div className="message-card-body">
							<div className="message-card-element">
								<span>{this.state.notificationMessage}</span>
							</div>
							{this.state.notification == 'default' ?
							<div className="message-card-element first-element">
								<Button label="Not yet" onClick={this.handleClickClose}>
								</Button>
							</div> : null}
							{this.state.notification == 'default' ?
							<div className="message-card-element">
								<Button label="Enable now!" onClick={this.handleClickEnable}>
								</Button>
							</div> : null}
							{this.state.notification != 'default' ?
							<div className="message-card-element">
								<Button label="Close" onClick={this.handleClickClose}>
								</Button>
							</div> : null}

						</div>
					</div>
				</div>
				<Growl ref={(el) => { this.growl = el; }}></Growl>
			</div>
		);
	}
}
