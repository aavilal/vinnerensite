import React from 'react';
import AppActions from '../../flux/actions/AppActions.jsx';

export default class HomeDashboard extends React.Component {
	constructor(props) {
		super(props);
		this.formatNumber = this.formatNumber.bind(this);
	}
	componentDidMount() {
		AppActions.getClicks();
	}
	formatNumber(number) {
		return (number + '').replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	}
	render() {
		return (
			<div className="home-dashboard-view">
				<h2>Clicks</h2>
				<div className="home-card">
					<div className="home-card-header">
						<span>Today</span>
					</div>
					<div className="home-card-body">
						<label>{this.formatNumber(this.props.data.home.today)}</label>
						<label>Clicks</label>
					</div>
				</div>

				<div className="home-card">
					<div className="home-card-header">
						<span>This Week</span>
					</div>
					<div className="home-card-body">
						<label>{this.formatNumber(this.props.data.home.week)}</label>
						<label>Clicks</label>
					</div>
				</div>

				<div className="home-card">
					<div className="home-card-header">
						<span>This Month</span>
					</div>
					<div className="home-card-body">
						<label>{this.formatNumber(this.props.data.home.month)}</label>
						<label>Clicks</label>
					</div>
				</div>
			</div>
		);
		return (

			<div className="home-dashboard-view">
				<h2>HomeDashboard</h2>
			</div>
		);
	}
}
