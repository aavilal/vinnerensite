import React from 'react';
import AppActions from '../../flux/actions/AppActions.jsx';
import {InputText} from 'primereact/components/inputtext/InputText';
import { Button } from 'primereact/components/button/Button';
import { Growl } from 'primereact/components/growl/Growl';
import { Link, withRouter  } from 'react-router-dom';

class UpdatePassword extends React.Component {
	constructor(props) {
		super(props);
		this.path = '/products/ecloud-messaging';
		this.id = null;
		this.state = {
			passwordLogin: '',
			passwordLoginError: '',
			passwordConfirmLogin: '',
			passwordConfirmLoginError: ''
		}
		this.handleClickUpdate = this.handleClickUpdate.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleKeyUp = this.handleKeyUp.bind(this);
		this.handleRedirectLogin = this.handleRedirectLogin.bind(this);
	}
	componentWillMount() {
		this.id = window.location.search.substring(window.location.search.indexOf('u=')+2, window.location.search.length);
		AppActions.validateURL(this.id);
	}
	componentDidUpdate() {
		const {success, message} = this.props.data.updatepwd;
		if (success != null && message.length > 0) {
			let messageTxt = message;
			let successTxt = success;
			if (successTxt)
				this.growl.show({severity: 'success', summary: 'Success', detail: messageTxt});
			else
				this.growl.show({severity: 'error', summary: 'Error', detail: messageTxt});
			AppActions.changeValue(['updatepwd', 'success'], null);
			AppActions.changeValue(['updatepwd', 'message'], '');
		}
	}
	handleRedirectLogin() {
		if (this.props.data.updatepwd.updated)
			this.props.history.push(this.path + '/login');
	}
	handleClickUpdate() {

		if (this.state.passwordLogin.length == 0)
			this.setState({ passwordLoginError: 'Required field' });

		if (this.state.passwordConfirmLogin.length == 0)
			this.setState({ passwordConfirmLoginError: 'Required field' });

		if (this.state.passwordConfirmLogin.length > 0 && this.state.passwordLogin.length > 0 && this.state.passwordConfirmLogin != this.state.passwordLogin)
			this.setState({ passwordConfirmLoginError: "Password confirmation doesn't match" });

			//ID
		if (this.state.passwordConfirmLogin.length > 0 && this.state.passwordLogin.length > 0 && this.state.passwordConfirmLogin == this.state.passwordLogin)
			AppActions.updatePassword(this.id, this.state.passwordLogin);

	}
	handleKeyUp(e) {
		if (e.keyCode == 13) {
			this.handleClickUpdate();
		}
	}
	handleChange(e) {
		let objState = {};
		this.setState({passwordLoginError: '', passwordConfirmLoginError: '' });

		objState[e.target.name] = e.target.value;
		this.setState(objState);

	}
	render() {
		let formUpdate = (
			<div className="updatepwd-form">
				<div className="updatepwd-tittle">
					<span>Update Password</span>
				</div>
				<div className="updatepwd-formelement">
					<span className="ui-float-label">
						<InputText name="passwordLogin"
										type="password"
										size="30"
										className={(this.state.passwordLogin == '' ? '' : 'ui-state-filled') + (this.state.passwordLoginError == '' ? '' : ' error-input')}
										onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
						<label>password</label>
					</span>
					<span className="updatepwd-error">{this.state.passwordLoginError}</span>
				</div>
				<div className="updatepwd-formelement">
					<span className="ui-float-label">
						<InputText name="passwordConfirmLogin"
										type="password"
										size="30"
										className={(this.state.passwordConfirmLogin == '' ? '' : 'ui-state-filled') + (this.state.passwordConfirmLoginError == '' ? '' : ' error-input')}
										onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
						<label>confirm password</label>
					</span>
					<span className="updatepwd-error">{this.state.passwordConfirmLoginError}</span>
				</div>
				<div className="updatepwd-formelement btn-updatepwd">
					<Button label="Update" onClick={this.handleClickUpdate} />
				</div>
			</div>
		);
		let wrongPage = (
			<div className="updatepwd-form">
				<div className="updatepwd-tittle">
					<span>Invalid token for reset password.</span>
				</div>
			</div>
		);
		return (
			<div className={"ecm-updatepwd-view " + this.props.deviceType}>
				{this.props.data.updatepwd.show == true ? formUpdate : null}
				{this.props.data.updatepwd.show == false ? wrongPage : null}
				<Growl ref={(el) => { this.growl = el; }} onRemove={this.handleRedirectLogin}></Growl>
			</div>
		);
	}
}
export default withRouter(UpdatePassword);
