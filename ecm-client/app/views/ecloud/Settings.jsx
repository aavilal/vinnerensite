import React from 'react';
import AppActions from '../../flux/actions/AppActions.jsx';
import { Growl } from 'primereact/components/growl/Growl';
import { FileUpload } from 'primereact/components/fileupload/FileUpload';
import { InputText } from 'primereact/components/inputtext/InputText';
import { Button } from 'primereact/components/button/Button';
import { ColorPicker } from 'primereact/components/colorpicker/ColorPicker';
import $ from 'jquery';

export default class Settings extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			titleAsk: '',
			titleAskError: '',
			subtitleAsk: '',
			subtitleAskError: '',
			acceptTextAsk: '',
			acceptTextAskError: '',
			cancelTextAsk: '',
			cancelTextAskError: '',
			imageAskError: '',
			colorAsk: '000000',
			colorAskError: ''
		}
		this.handleChangeColor = this.handleChangeColor.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleKeyUp = this.handleKeyUp.bind(this);
		this.onBeforeUpload = this.onBeforeUpload.bind(this);
		this.onUpload = this.onUpload.bind(this);
		this.onUploadError = this.onUploadError.bind(this);
		this.handleClickSave = this.handleClickSave.bind(this);
		this.deleteFile = this.deleteFile.bind(this);
		this.handleClickPreview = this.handleClickPreview.bind(this);
	}
	componentDidMount() {

		let objParam = {};
		let params = this.props.data.params;

		let foundParam = params.filter((param) => { return param.type == 'ASK_NOT' && param.param == 'TITLE'});
		if (foundParam.length > 0) {
			objParam.titleAsk = foundParam[0].value;
		}
		foundParam = params.filter((param) => { return param.type == 'ASK_NOT' && param.param == 'SUBTITLE'});
		if (foundParam.length > 0) {
			objParam.subtitleAsk = foundParam[0].value;
		}
		foundParam = params.filter((param) => { return param.type == 'ASK_NOT' && param.param == 'ACCEPT'});
		if (foundParam.length > 0) {
			objParam.acceptTextAsk = foundParam[0].value;
		}
		foundParam = params.filter((param) => { return param.type == 'ASK_NOT' && param.param == 'CANCEL'});
		if (foundParam.length > 0) {
			objParam.cancelTextAsk = foundParam[0].value;
		}
		foundParam = params.filter((param) => { return param.type == 'ASK_NOT' && param.param == 'COLOR'});
		if (foundParam.length > 0) {
			objParam.colorAsk = foundParam[0].value;
		}
		foundParam = params.filter((param) => { return param.type == 'ASK_NOT' && param.param == 'BUCKET_IMG_URL'});
		if (foundParam.length > 0) {
			AppActions.changeValue(['ask', 'bucketImgUrl'], foundParam[0].value);
		}
		foundParam = params.filter((param) => { return param.type == 'ASK_NOT' && param.param == 'BUCKET_IMG_NAME'});
		if (foundParam.length > 0) {
			AppActions.changeValue(['ask', 'bucketImgName'], foundParam[0].value);
		}
		foundParam = params.filter((param) => { return param.type == 'ASK_NOT' && param.param == 'IMG_NAME'});
		if (foundParam.length > 0) {
			AppActions.changeValue(['ask', 'imgName'], foundParam[0].value);
		}
		this.setState(objParam);

	}
	componentDidUpdate() {
		const {success, message} = this.props.data.notify;
		if (success != null && message.length > 0) {
			if (success)
				this.growl.show({severity: 'success', summary: 'Success', detail: message});
			else
				this.growl.show({severity: 'error', summary: 'Error', detail: message});

			AppActions.changeValue(['ask', 'success'], null);
			AppActions.changeValue(['ask', 'message'], '');
		}
	}
	deleteFile() {
		AppActions.changeValue(['ask', 'bucketImgUrl'], '0');
		AppActions.changeValue(['ask', 'bucketImgName'], '0');
		AppActions.changeValue(['ask', 'imgName'], '');
	}
	handleChangeColor(event) {
		this.setState({colorAsk: event.value.toUpperCase()})
	}
	handleChange(event) {
		let objState = {};
		this.setState({
			titleAskError: '',
			subtitleAskError: '',
			imageAskError: '',
			acceptTextAskError: '',
			cancelTextAskError: ''
		});
		objState[event.target.name] = event.target.value;
		this.setState(objState);
	}
	handleKeyUp(event) {
		if (event.keyCode == 13) {
			this.handleClickSave();
		}
	}
	onBeforeUpload(event) {
		AppActions.changeValue(['loading'], true);
	}
	onUpload(event) {
		this.imageAskFU.fileInput.style.display = '';
		let res = JSON.parse(event.xhr.response);
		this.setState({
			imageAskError: '',
		});
		AppActions.changeValue(['ask', 'bucketImgUrl'], res.bucketImgUrl);
		AppActions.changeValue(['ask', 'bucketImgName'], res.bucketImgName);
		AppActions.changeValue(['ask', 'imgName'], res.imgName);

		AppActions.changeValue(['loading'], false);
		this.growl.show({severity: 'success', summary: 'Success', detail: 'File Uploaded.'});

	}
	onUploadError(event) {
		this.imageAskFU.fileInput.style.display = '';
		let res = {
			message: "Couldn't upload image"
		}
		try {
			res = JSON.parse(event.xhr.response);
			this.setState({
				imageAskError: 'Error on uploading image.',
			});
			AppActions.changeValue(['ask', 'bucketImgUrl'], '0');
			AppActions.changeValue(['ask', 'bucketImgName'], '0');
			AppActions.changeValue(['ask', 'imgName'], '');
		} catch(err){}

		AppActions.changeValue(['loading'], false);

		this.growl.show({severity: 'error', summary: 'Error', detail: res.message});
	}
	handleClickPreview() {

	}
	handleClickSave() {
		const {titleAsk, subtitleAsk, acceptTextAsk, cancelTextAsk, colorAsk} = this.state;
		if (titleAsk.length == 0)
			this.setState({ titleAskError: 'Required field' });
		if (subtitleAsk.length == 0)
			this.setState({ subtitleAskError: 'Required field' });
		if (acceptTextAsk.length == 0)
			this.setState({ acceptTextAskError: 'Required field' });
		if (cancelTextAsk.length == 0)
			this.setState({ cancelTextAskError: 'Required field' });
		if (this.props.data.ask.imgName.length == 0)
			this.setState({ imageAskError: 'Required field' });

		if (titleAsk.length > 200)
	 		 this.setState({ titleAskError: 'Maximum 200 characters' });
	 	 if (subtitleAsk.length > 200)
	 		 this.setState({ subtitleAskError: 'Maximum 200 characters' });
	 	 if (acceptTextAsk.length > 200)
	 		 this.setState({ acceptTextAskError: 'Maximum 200 characters' });
	 	 if (cancelTextAsk.length > 200)
	 		 this.setState({ cancelTextAskError: 'Maximum 200 characters' });

		let data = [];
		data.push({
			type: 'ASK_NOT',
			param: 'IMG_NAME',
			value: this.props.data.ask.imgName
		});
		data.push({
			type: 'ASK_NOT',
			param: 'BUCKET_IMG_URL',
			value: this.props.data.ask.bucketImgUrl
		});
		data.push({
			type: 'ASK_NOT',
			param: 'BUCKET_IMG_NAME',
			value: this.props.data.ask.bucketImgName
		});
		data.push({
			type: 'ASK_NOT',
			param: 'TITLE',
			value: titleAsk
		});
		data.push({
			type: 'ASK_NOT',
			param: 'SUBTITLE',
			value: subtitleAsk
		});
		data.push({
			type: 'ASK_NOT',
			param: 'COLOR',
			value: colorAsk
		});
		data.push({
			type: 'ASK_NOT',
			param: 'ACCEPT',
			value: acceptTextAsk
		});
		data.push({
			type: 'ASK_NOT',
			param: 'CANCEL',
			value: cancelTextAsk
		});
		if (titleAsk.length > 0 && subtitleAsk.length > 0 && acceptTextAsk.length > 0 && cancelTextAsk.length > 0 && this.props.data.ask.imgName.length > 0
			&& titleAsk.length <= 200 && subtitleAsk.length <= 200 && acceptTextAsk.length <= 200 && cancelTextAsk.length <= 200)
			AppActions.updateParams(data);

	}

	render() {
		return (
			<div className="settings-view">
				<h2>Ask Notifications</h2>
				<div className="settings-card">
					<div className="settings-card-header">
						<span>Info</span>
					</div>
					<div className="settings-card-body">
						<div className="settings-card-element">
							<span className="ui-float-label">
								<InputText name="titleAsk"
													type="text"
													value={this.state.titleAsk}
													className={(this.state.titleAsk == '' ? '' : 'ui-state-filled') + (this.state.titleAskError == '' ? '' : ' error-input')}
													onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
								<label>title</label>
							</span>
							<span className="ask-error">{this.state.titleAskError}</span>
						</div>
						<div className="settings-card-element">
							<span className="ui-float-label">
								<InputText name="subtitleAsk"
													type="text"
													value={this.state.subtitleAsk}
													className={(this.state.subtitleAsk == '' ? '' : 'ui-state-filled') + (this.state.subtitleAskError == '' ? '' : ' error-input')}
													onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
								<label>subtitle</label>
							</span>
							<span className="ask-error">{this.state.subtitleAskError}</span>
						</div>
						<div className="settings-card-element">
							<span className="ui-float-label">
								<InputText name="imageAsk"
													type="text"
													className={this.state.imageAskError == '' ? '' : 'error-input'}
													value={this.props.data.ask.imgName} onKeyUp={this.handleKeyUp}/>
								<label>image</label>
								<i class="material-icons">&#xE226;</i>
								{this.props.data.ask.imgName == '' ? null : <i class="material-icons deletefile" onClick={(e) => this.deleteFile()}>&#xE872;</i>}
								<FileUpload name="imageAskFU" url={"/api/upload/image/" + this.props.data.ask.bucketImgName}
														onUpload={this.onUpload} onError={this.onUploadError}
														onBeforeUpload={this.onBeforeUpload} auto={true}
														accept="image/*" mode="basic" chooseLabel="" readonly
														ref={(el) => { this.imageAskFU = el; }}/>
							</span>
							<span className="ask-error">{this.state.imageAskError}</span>
						</div>
						<div className="settings-card-element">
							<span className="ui-float-label">
								<InputText name="colorAsk"
													type="text"
													className={this.state.colorAskError == '' ? '' : 'error-input'}
													value={this.state.colorAsk} onKeyUp={this.handleKeyUp}/>
								<label>color</label>
								<ColorPicker onChange={this.handleChangeColor}/>
							</span>
							<span className="ask-error">{this.state.colorAskError}</span>
						</div>
						<div className="settings-card-element">
							<span className="ui-float-label">
								<InputText name="acceptTextAsk"
													type="text"
													value={this.state.acceptTextAsk}
													className={(this.state.acceptTextAsk == '' ? '' : 'ui-state-filled') + (this.state.acceptTextAskError == '' ? '' : ' error-input')}
													onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
								<label>accept text</label>
							</span>
							<span className="ask-error">{this.state.acceptTextAskError}</span>
						</div>
						<div className="settings-card-element">
							<span className="ui-float-label">
								<InputText name="cancelTextAsk"
													type="text"
													value={this.state.cancelTextAsk}
													className={(this.state.cancelTextAsk == '' ? '' : 'ui-state-filled') + (this.state.cancelTextAskError == '' ? '' : ' error-input')}
													onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
								<label>cancel text</label>
							</span>
							<span className="ask-error">{this.state.cancelTextAskError}</span>
						</div>
						<div className="settings-card-element">
							<Button label="Save" onClick={this.handleClickSave}>
							</Button>
						</div>
					</div>
				</div>
				<Growl ref={(el) => { this.growl = el; }}></Growl>
			</div>
		);
	}
}
