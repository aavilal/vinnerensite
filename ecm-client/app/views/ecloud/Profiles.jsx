import React from 'react';
import AppActions from '../../flux/actions/AppActions.jsx';
import { DataGrid } from 'primereact/components/datagrid/DataGrid';
import { Growl } from 'primereact/components/growl/Growl';
import { InputText } from 'primereact/components/inputtext/InputText';
import { Button } from 'primereact/components/button/Button';
import { Panel } from 'primereact/components/panel/Panel';
import { Checkbox } from 'primereact/components/checkbox/Checkbox';
import $ from 'jquery';

export default class Profiles extends React.Component {
	constructor(props) {
		super(props);
		this.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		this.state = {
			id: null,
			nameProfile: '',
			nameProfileError: '',
			descriptionProfile: '',
			descriptionProfileError: '',
			menuOptions: []
		};
		this.handleClickAdd = this.handleClickAdd.bind(this);
		this.handleClickRefresh = this.handleClickRefresh.bind(this);
		this.handleClickDelete = this.handleClickDelete.bind(this);
		this.handleClickEdit = this.handleClickEdit.bind(this);
		this.handleClickClose = this.handleClickClose.bind(this);
		this.itemProfileTemplate = this.itemProfileTemplate.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleKeyUp = this.handleKeyUp.bind(this);
		this.handleClickSave = this.handleClickSave.bind(this);
		this.formatString = this.formatString.bind(this);
		this.handleClickDialog = this.handleClickDialog.bind(this);
		this.onMenuChange = this.onMenuChange.bind(this);

	}
	componentDidMount() {
		AppActions.getMenuOptions();
		AppActions.getProfiles();
	}
	componentDidUpdate() {
		const {success, message} = this.props.data.profiles;

		if (success != null && message.length > 0) {
			let messageTxt = message;
			let successTxt = success;
			if (successTxt) {
				this.growl.show({severity: 'success', summary: 'Success', detail: messageTxt});
				this.handleClickClose();
			} else {
				this.growl.show({severity: 'error', summary: 'Error', detail: messageTxt});
			}
			AppActions.changeValue(['profiles', 'success'], null);
			AppActions.changeValue(['profiles', 'message'], '');

		}
	}
	onMenuChange(e) {
		let menuOp = [...this.state.menuOptions];
    if(e.checked)
        menuOp.push(e.value);
    else
        menuOp.splice(menuOp.indexOf(e.value), 1);

    this.setState({menuOptions: menuOp});
	}
	handleClickRefresh() {
		AppActions.getProfiles();
	}
	handleClickDialog(ev) {
		if (ev.target.className.indexOf('profiles-dialog') != -1) {
			this.handleClickClose();
		}
	}
	handleClickAdd() {
		let _this = this;
		let timeOut = $('.dashboard-view').scrollTop() > 0 ? 1000 : 0;
		$('.dashboard-view').animate({
          scrollTop: 0
        }, timeOut, function() {
					_this.setState({
						id: null,
						nameProfile: '',
						nameProfileError: '',
						descriptionProfile: '',
						descriptionProfileError: '',
						menuOptions: []
					});

					$(_this.refs.dialog).fadeIn('slow');
        });

	}
	handleClickDelete(event, data) {
		AppActions.deleteProfiles(data);
	}
	handleClickEdit(event, data) {
		let _this = this;
		let timeOut = $('.dashboard-view').scrollTop() > 0 ? 1000 : 0;
		$('.dashboard-view').animate({
          scrollTop: 0
        }, timeOut, function() {
					_this.setState({
						id: data.id,
						nameProfile: data.name,
						nameProfileError: '',
						descriptionProfile: data.description,
						descriptionProfileError: '',
						menuOptions: data.menuOptions
					});

					$(_this.refs.dialog).fadeIn('slow');
        });

	}
	handleClickClose() {
		$(this.refs.dialog).fadeOut('slow');
	}
	formatString(data) {
		return data.length > 25 ? data.substring(0, 25) + '...' : data;
	}
	itemProfileTemplate(data) {
		if (!data)
    	return;

		return (
			<div style={{ padding: '5px' }} className="ui-g-12 ui-md-3">
				<Panel header={data.name.toUpperCase()} style={{ textAlign: 'center', background: '#FFFFFF', width: '300px', margin: '0 auto', padding: '0px' }}>
					<div className="item-img">
						<div>{data.name.substring(0,1).toUpperCase()}</div>
					</div>
					<div className="item-content">
						<span>{this.formatString(data.description)}</span>
					</div>
					<div className="item-btn">
						<Button label="Edit" onClick={(e) => this.handleClickEdit(this, data)}>
						</Button>
						<Button label="Delete" onClick={(e) => this.handleClickDelete(this, data)} >
						</Button>
					</div>
				</Panel>
			</div>
		);
	}

	handleChange(event) {
		let objState = {};
		this.setState({
			nameProfileError: '',
			descriptionProfileError: ''
		});
		objState[event.target.name] = event.target.value;
		this.setState(objState);
	}

	handleKeyUp(event) {
		if (event.keyCode == 13) {
			this.handleClickSave();
		}
	}

	handleClickSave() {
		const {
		id,
		nameProfile,
		nameProfileError,
		descriptionProfile,
		descriptionProfileError,
		menuOptions} = this.state;
		if (nameProfile.length == 0)
			this.setState({ nameProfileError: 'Required field' });
		if (descriptionProfile.length == 0)
			this.setState({ descriptionProfileError: 'Required field' });

		if (nameProfile.length > 50)
			this.setState({ nameProfileError: 'Maximum 50 characters' });
		if (descriptionProfile.length > 100)
			this.setState({ descriptionProfileError: 'Maximum 100 characters' });

		let data = {
			id: id,
			name: nameProfile,
			description: descriptionProfile,
			menuOptions: menuOptions
		}
		if (nameProfile.length > 0 && descriptionProfile.length > 0 && nameProfile.length <= 50 && descriptionProfile.length <= 100) {
			if (id == null)
				AppActions.setProfiles(data);
			else
				AppActions.updateProfiles(data);
		}


	}

	render() {
		let es = {
			firstDayOfWeek: 1,
			dayNames: ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
    	dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
    	dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    	monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    	monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
		};
		const {
		id,
		nameProfile,
		nameProfileError,
		descriptionProfile,
		descriptionProfileError,
		menuOptions} = this.state;

		let menu = [];
		for (let i = 0; i < this.props.data.menuOptions.length; i++) {
			const {id, name} = this.props.data.menuOptions[i];
			menu.push(
			<div>
					<Checkbox inputId={id + '-cb'} value={id} onChange={this.onMenuChange} checked={this.state.menuOptions.indexOf(id) != -1}></Checkbox>
					<label htmlFor={id + '-cb'}>{name}</label>
			</div>);
		}
		return (
			<div className="profiles-view">
				<h2>Profiles</h2>
				<div className="profiles-table">
					{this.props.data.profilesAll == null || this.props.data.profilesAll.length == 0 ? null :
					<DataGrid value={this.props.data.profilesAll}
										itemTemplate={this.itemProfileTemplate}
										paginator={this.props.data.profilesAll != null && this.props.data.profilesAll.length > 20 ? true : false} rows={20}
										/> }
				</div>
				<div className="profiles-bubble" onClick={this.handleClickAdd}>
					<i class="material-icons">&#xE145;</i>
				</div>
				<div ref="dialog" className="profiles-dialog hide" onClick={this.handleClickDialog}>
					<div className="profiles-card">
						<div className="profiles-card-header">
							<span>Info</span>
							<i class="material-icons" onClick={this.handleClickClose}>&#xE5CD;</i>
						</div>
						<div className="profiles-card-body">
							<div className="profiles-card-element">
								<span className="ui-float-label">
									<InputText name="nameProfile"
														type="text"
														value={nameProfile}
														className={(nameProfile == '' ? '' : 'ui-state-filled') + (nameProfileError == '' ? '' : ' error-input')}
														onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
									<label>name</label>
								</span>
								<span className="profiles-error">{nameProfileError}</span>
							</div>
							<div className="profiles-card-element">
								<span className="ui-float-label">
									<InputText name="descriptionProfile"
														type="text"
														value={descriptionProfile}
														className={(descriptionProfile == '' ? '' : 'ui-state-filled') + (descriptionProfileError == '' ? '' : ' error-input')}
														onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
									<label>description</label>
								</span>
								<span className="profiles-error">{descriptionProfileError}</span>
							</div>
							<div className="profiles-card-element menu">
								<label>menu options:</label>
								<div className="profiles-menu-content">
								{menu}
								</div>
							</div>
							<div className="profiles-card-element">
								<Button label="Save" onClick={this.handleClickSave}>
								</Button>
							</div>
						</div>
					</div>
				</div>
				<Growl ref={(el) => { this.growl = el; }}></Growl>
			</div>
		);
	}
}
