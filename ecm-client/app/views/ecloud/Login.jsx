import React from 'react';
import AppActions from '../../flux/actions/AppActions.jsx';
import {InputText} from 'primereact/components/inputtext/InputText';
import { Button } from 'primereact/components/button/Button';
import { Link } from 'react-router-dom';

export default class Login extends React.Component {
	constructor(props) {
		super(props);
		this.path = '/products/ecloud-messaging';
		this.state = {
			userLogin: '',
			userLoginError: '',
			passwordLogin: '',
			passwordLoginError: ''
		}
		this.handleClickLogin = this.handleClickLogin.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleKeyUp = this.handleKeyUp.bind(this);
	}
	handleClickLogin() {

		if (this.state.userLogin.length == 0)
			this.setState({ userLoginError: 'Required field' });

		if (this.state.passwordLogin.length == 0)
			this.setState({ passwordLoginError: 'Required field' });

		if (this.state.userLogin.length > 0 && this.state.passwordLogin.length > 0 && this.state.userLoginError.length == 0)
			AppActions.getLogin(this.state.userLogin, this.state.passwordLogin);

	}
	handleKeyUp(e) {
		if (e.keyCode == 13) {
			this.handleClickLogin();
		}
	}
	handleChange(e) {
		let objState = {};
		this.setState({passwordLoginError: '' });
		if (e.target.name == 'userLogin') {
			let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if (e.target.value == '') {
				this.setState({ userLoginError: ''});
			} else {
				if (!(re.test(e.target.value))) {
					this.setState({ userLoginError: 'Invalid email format'});
				} else {
					this.setState({ userLoginError: ''});
				}
			}

		}
		objState[e.target.name] = e.target.value;
		this.setState(objState);
		if (this.props.data.user.messageError != '')
			AppActions.changeValue(['user', 'messageError'], '')
	}
	render() {
		return (
			<div className={"ecm-login-view " + this.props.deviceType}>
				<div className="login-form">
					<div className="login-tittle">
						<span>Log in</span>
					</div>
					<div className="login-error-section">
						<span className="login-error">{this.props.data.user.messageError}</span>
					</div>
					<div className="login-formelement">
						<span className="ui-float-label">
							<InputText name="userLogin" autocomplete="off"
												type="text"
												size="30"
												className={(this.state.userLogin == '' ? '' : 'ui-state-filled') + (this.state.userLoginError == '' ? '' : ' error-input')}
												value={this.state.userLogin}
												onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
							<label>email</label>
						</span>
						<span className="login-error">{this.state.userLoginError}</span>
					</div>
					<div className="login-formelement">
						<span className="ui-float-label">
							<InputText name="passwordLogin"
											type="password"
											size="30"
											className={(this.state.passwordLogin == '' ? '' : 'ui-state-filled') + (this.state.passwordLoginError == '' ? '' : ' error-input')}
											onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
							<label>password</label>
						</span>
						<span className="login-error">{this.state.passwordLoginError}</span>
					</div>
					<div className="login-formelement lbl-forgot">
						<Link to={this.path + "/forgot-password"}>
							<span>Forgot password?</span>
						</Link>
					</div>
					<div className="login-formelement btn-login">
						<Button label="Log in" onClick={this.handleClickLogin} />
					</div>
				</div>
			</div>
		);
	}
}
