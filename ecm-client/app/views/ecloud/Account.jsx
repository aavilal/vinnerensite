import React from 'react';
import AppActions from '../../flux/actions/AppActions.jsx';
import { Growl } from 'primereact/components/growl/Growl';
import { FileUpload } from 'primereact/components/fileupload/FileUpload';
import { InputText } from 'primereact/components/inputtext/InputText';
import { Button } from 'primereact/components/button/Button';

export default class Account extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			avatarAccount: '0',
			avatarAccountName: '0',
			nameAccount: '',
			nameAccountError: '',
			passwordAccount: '',
			passwordAccountError: '',
			passwordConfirmAccount: '',
			passwordConfirmAccountError: '',
			passwordOldAccount: '',
			passwordOldAccountError: ''
		}
		this.handleChange = this.handleChange.bind(this);
		this.handleKeyUp = this.handleKeyUp.bind(this);
		this.handleClickSave = this.handleClickSave.bind(this);
		this.onBeforeUpload = this.onBeforeUpload.bind(this);
		this.onUpload = this.onUpload.bind(this);
		this.onUploadError = this.onUploadError.bind(this);
	}
	componentDidUpdate() {
		const {success, message} = this.props.data.user;
		if (success != null && message && message.length > 0) {
			if (success)
				this.growl.show({severity: 'success', summary: 'Success', detail: message});
			else
				this.growl.show({severity: 'success', summary: 'Error', detail: message});

			AppActions.changeValue(['user', 'message'], '');
		}
		if (this.props.data.user.invalidPassword) {
			AppActions.changeValue(['user', 'invalidPassword'], false);
			this.setState({ passwordOldAccountError: 'Invalid password' });
		}

	}
	handleChange(event) {
		let objState = {};
		this.setState({
			nameAccountError: '',
			passwordAccountError: '',
			passwordConfirmAccountError: '',
			passwordOldAccountError: ''
		});
		objState[event.target.name] = event.target.value;
		this.setState(objState);
	}
	handleKeyUp(event) {
		if (event.keyCode == 13) {
			this.handleClickSave();
		}
	}
	onBeforeUpload(event) {
		AppActions.changeValue(['loading'], true);
	}
	onUpload(event) {
		this.avatarAccount.fileInput.style.display = '';
		let res = JSON.parse(event.xhr.response);
		this.setState({
			avatarAccountError: '',
			avatarAccount: res.bucketImgUrl,
			avatarAccountName: res.bucketImgName
		});
		AppActions.changeValue(['user', 'avatar'], res.bucketImgUrl);

		AppActions.changeValue(['loading'], false);
		this.growl.show({severity: 'success', summary: 'Success', detail: 'File Uploaded.'});
	}
	onUploadError(event) {
		this.avatarAccount.fileInput.style.display = '';
		let img = this.props.data.user.avatar;
		let imgName = this.props.data.user.avatarName;
		let res = {
			message: "Couldn't upload image"
		}
		try {
			res = JSON.parse(event.xhr.response);
			this.setState({
				avatarAccountError: 'Error on uploading image.',
				avatarAccount: img,
				avatarAccountName: imgName
			});
			AppActions.changeValue(['user', 'avatar'], '0');

		} catch(err){}

		AppActions.changeValue(['loading'], false);

		this.growl.show({severity: 'error', summary: 'Error', detail: res.message});

	}
	handleClickSave() {
		const {nameAccount, passwordAccount, passwordConfirmAccount, passwordOldAccount, avatarAccount, avatarAccountName} = this.state;
		if (nameAccount.length == 0)
			this.setState({ nameAccountError: 'Required field' });
		if (passwordAccount.length == 0)
			this.setState({ passwordAccountError: 'Required field' });
		if (passwordConfirmAccount.length == 0)
			this.setState({ passwordConfirmAccountError: 'Required field' });
		if (passwordOldAccount.length == 0)
			this.setState({ passwordOldAccountError: 'Required field' });


		if (passwordConfirmAccount != passwordAccount)
			this.setState({ passwordConfirmAccountError: "Password doesn't match" });

		let data = {
			name: nameAccount,
			passwordold: passwordOldAccount,
			password: passwordAccount,
			avatar: avatarAccount,
			avatarName: avatarAccountName
		}
		if (nameAccount.length > 0 && passwordAccount.length > 0 && passwordConfirmAccount.length > 0 && passwordOldAccount.length > 0 && passwordAccount == passwordConfirmAccount)
			AppActions.updateUser(data);

	}
	componentDidMount() {
		const {name, avatar, avatarName} = this.props.data.user;
		this.setState({
			nameAccount: name,
			avatarAccount: avatar,
			avatarAccountName: avatarName
		});
	}
	render() {
		let styleAvatar = {
			backgroundImage: this.state.avatarAccount == '0' ? 'url(./images/avatar.jpg)' : 'url(' + this.state.avatarAccount + ')',
		}
		return (
			<div className="account-view">
				<h2>My Account</h2>
				<div className="account-card">
					<div className="account-card-header">
						<span>{this.props.data.user.email}</span>
					</div>
					<div className="account-card-body">
						<div className="account-card-element">
							<div className="account-avatar" style={styleAvatar}>
							<FileUpload name="avatarAccount" url={"/api/upload/image/" + this.state.avatarAccountName}
													onUpload={this.onUpload} onError={this.onUploadError}
													onBeforeUpload={this.onBeforeUpload} auto={true}
													accept="image/*" mode="basic" chooseLabel="" readonly
													ref={(el) => { this.avatarAccount = el; }}/>
							</div>
						</div>
						<div className="account-card-element">
							<span className="ui-float-label">
								<InputText name="nameAccount"
													type="text" value={this.state.nameAccount}
													className={(this.state.nameAccount == '' ? '' : 'ui-state-filled') + (this.state.nameAccountError == '' ? '' : ' error-input')}
													onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
								<label>name</label>
							</span>
							<span className="account-error">{this.state.nameAccountError}</span>
						</div>
						<div className="account-card-element">
							<span className="ui-float-label">
								<InputText name="passwordOldAccount"
													type="password"
													className={(this.state.passwordOldAccount == '' ? '' : 'ui-state-filled') + (this.state.passwordOldAccountError == '' ? '' : ' error-input')}
													onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
								<label>old password</label>
							</span>
							<span className="account-error">{this.state.passwordOldAccountError}</span>
						</div>
						<div className="account-card-element">
							<span className="ui-float-label">
								<InputText name="passwordAccount"
													type="password"
													className={(this.state.passwordAccount == '' ? '' : 'ui-state-filled') + (this.state.passwordAccountError == '' ? '' : ' error-input')}
													onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
								<label>password</label>
							</span>
							<span className="account-error">{this.state.passwordAccountError}</span>
						</div>
						<div className="account-card-element">
							<span className="ui-float-label">
								<InputText name="passwordConfirmAccount"
													type="password"
													className={(this.state.passwordConfirmAccount == '' ? '' : 'ui-state-filled') + (this.state.passwordConfirmAccountError == '' ? '' : ' error-input')}
													onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
								<label>confirm password</label>
							</span>
							<span className="account-error">{this.state.passwordConfirmAccountError}</span>
						</div>
						<div className="account-card-element">
							<Button label="Save" onClick={this.handleClickSave}>
							</Button>
						</div>
					</div>
				</div>
				<Growl ref={(el) => { this.growl = el; }}></Growl>
			</div>
		);
	}
}
