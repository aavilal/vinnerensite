import React from 'react';
import AppActions from '../../flux/actions/AppActions.jsx';
import { DataGrid } from 'primereact/components/datagrid/DataGrid';
import { Panel } from 'primereact/components/panel/Panel';
import $ from 'jquery';

export default class History extends React.Component {
	constructor(props) {
		super(props);
		this.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		this.itemHistoryTemplate = this.itemHistoryTemplate.bind(this);
		this.formatDate = this.formatDate.bind(this);
		this.formatString = this.formatString.bind(this);
		this.formatNumber = this.formatNumber.bind(this);
	}
	componentDidMount() {
		AppActions.getHistory();
	}
	formatDate(date, time) {
		let dateObj = new Date(date);
		let dateTxt = this.months[dateObj.getMonth()] + ' ' + dateObj.getDate() + ', ' + dateObj.getFullYear();
		let timeObj = new Date(time);
		let timeTxt = (timeObj.getHours() < 10 ? '0' : '') + timeObj.getHours() + ':' + (timeObj.getMinutes() < 10 ? '0' : '') + timeObj.getMinutes();
		return dateTxt + ' ' + timeTxt;
	}
	formatString(data) {
		return data.length > 25 ? data.substring(0, 25) + '...' : data;
	}
	formatNumber(number) {
		return (number + '').replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	}
	itemHistoryTemplate(data) {
		if (!data)
    	return;

		return (
			<div style={{ padding: '5px' }} className="ui-g-12 ui-md-3">
				<Panel header={this.formatDate(data.dateSend, data.timeSend)} style={{ textAlign: 'center', background: '#FFFFFF', width: '300px', margin: '0 auto', padding: '0px' }}>
					<div className="item-img">
						<img src={data.bucketImgUrl != '0' ? data.bucketImgUrl : data.bucketIconUrl}/>
					</div>
					<div className="item-content">
						<label>{this.formatString(data.title)}</label>
						<span>{this.formatString(data.body)}</span>
						<span>{this.formatString(data.url)}</span>
					</div>
					<div className="item-click">
						<label>CLICKS</label>
						<label>{this.formatNumber(data.clicks)}</label>
					</div>
				</Panel>
			</div>
		);
	}

	render() {
		let es = {
			firstDayOfWeek: 1,
			dayNames: ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
    	dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
    	dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    	monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    	monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
		};
		return (
			<div className={"history-notify-view" + " " + this.props.deviceType}>
				<h2>History</h2>
				<div className="history-table">
					{this.props.data.history == null || this.props.data.history.length == 0 ? null :
					<DataGrid value={this.props.data.history}
										itemTemplate={this.itemHistoryTemplate}
										paginator={this.props.data.history != null && this.props.data.history.length > 20 ? true : false} rows={20}
										/> }
				</div>
			</div>
		);
	}
}
