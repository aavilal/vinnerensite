import React from 'react';
import AppActions from '../../flux/actions/AppActions.jsx';
import {InputText} from 'primereact/components/inputtext/InputText';
import { Button } from 'primereact/components/button/Button';
import { Growl } from 'primereact/components/growl/Growl';
import { Link } from 'react-router-dom';

export default class ForgotPassword extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			userLogin: '',
			userLoginError: ''
		}
		this.handleClickReset = this.handleClickReset.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleKeyUp = this.handleKeyUp.bind(this);
	}
	handleClickReset() {

		if (this.state.userLogin.length == 0)
			this.setState({ userLoginError: 'Required field' });

		if (this.state.userLogin.length > 0 && this.state.userLoginError.length == 0)
			AppActions.setForgotPassword(this.state.userLogin);

	}
	componentDidUpdate() {
		const {success, message} = this.props.data.user;

		if (success != null && message.length > 0) {
			let messageTxt = message;
			let successTxt = success;
			if (successTxt)
				this.growl.show({severity: 'success', summary: 'Success', detail: messageTxt});
			else
				this.growl.show({severity: 'error', summary: 'Error', detail: messageTxt});
			AppActions.changeValue(['user', 'success'], null);
			AppActions.changeValue(['user', 'message'], '');

		}
	}
	handleKeyUp(e) {
		if (e.keyCode == 13) {
			this.handleClickReset();
		}
	}
	handleChange(e) {
		let objState = {};

		if (e.target.name == 'userLogin') {
			let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if (e.target.value == '') {
				this.setState({ userLoginError: ''});
			} else {
				if (!(re.test(e.target.value))) {
					this.setState({ userLoginError: 'Invalid email format'});
				} else {
					this.setState({ userLoginError: ''});
				}
			}

		}
		objState[e.target.name] = e.target.value;
		this.setState(objState);
		if (this.props.data.user.messageError != '')
			AppActions.changeValue(['user', 'messageError'], '')
	}
	render() {
		return (
			<div className={"ecm-forgotpwd-view " + this.props.deviceType}>
				<div className="forgotpwd-form">
					<div className="forgotpwd-tittle">
						<span>Recover Password</span>
					</div>
					<div className="forgotpwd-error-section">
						<span className="forgotpwd-error">{this.props.data.user.messageError}</span>
					</div>
					<div className="forgotpwd-formelement">
						<span className="ui-float-label">
							<InputText name="userLogin" autocomplete="off"
												type="text"
												className={(this.state.userLogin == '' ? '' : 'ui-state-filled') + (this.state.userLoginError == '' ? '' : ' error-input')}
												size="30"
												onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
							<label>email</label>
						</span>
						<span className="forgotpwd-error">{this.state.userLoginError}</span>
					</div>
					<div className="forgotpwd-formelement btn-forgotpwd">
						<Button label="Recover" onClick={this.handleClickReset} />
					</div>
				</div>
				<Growl ref={(el) => { this.growl = el; }}></Growl>
			</div>
		);
	}
}
