import React from 'react';
import AppActions from '../../flux/actions/AppActions.jsx';
import {InputText} from 'primereact/components/inputtext/InputText';
import { Link } from 'react-router-dom';
import { Growl } from 'primereact/components/growl/Growl';
import $ from 'jquery';

export default class Home extends React.Component {
	constructor(props) {
		super(props);
		this.path = '/products/ecloud-messaging';
		this.state = {
			nameContact: '',
			nameContactError: '',
			emailContact: '',
			emailContactError: '',
			messageContact: '',
			messageContactError: ''
		}
		this.handleClickContact = this.handleClickContact.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleKeyUp = this.handleKeyUp.bind(this);
		this.validateEmail = this.validateEmail.bind(this);
		this.handleClickSend = this.handleClickSend.bind(this);
	}
	componentDidMount() {
		window.location.hash = '';
		$('html, body').animate({
          scrollTop: 0
        }, 1000, function() {
        });
	}
	componentDidUpdate() {
		const {success, message} = this.props.data.contactus;
		if (success != null && message.length > 0) {
			if (success)
				this.growl.show({severity: 'success', summary: 'Success', detail: message});
			else
				this.growl.show({severity: 'error', summary: 'Error', detail: message});

			AppActions.changeValue(['contactus', 'success'], null);
			AppActions.changeValue(['contactus', 'message'], '');

		}
	}
	handleClickContact(e) {
		let target =  $('.' + e);
		$('html, body').animate({
          scrollTop: target.offset().top - $('.ecm-header-component').outerHeight() + 1
        }, 1000, function() {
        });
	}
	handleClickSend() {
		const {nameContact, emailContact, emailContactError, messageContact} = this.state;
		if (nameContact.length == 0)
			this.setState({ nameContactError: 'Required field' });
		if (emailContact.length == 0)
			this.setState({ emailContactError: 'Required field' });
		if (messageContact.length == 0)
			this.setState({ messageContactError: 'Required field' });

		if (emailContact.length > 0 && !this.validateEmail(emailContact)) {
			this.setState({ emailContactError: 'Invalid email format' });
		}

		let data = {
			email: emailContact,
			name: nameContact,
			message: messageContact
		}
		if (nameContact.length > 0 && emailContact.length > 0 && messageContact.length > 0 && emailContactError.length == 0) {
			AppActions.sendContactUs(data);
		}
	}
	handleChange(event) {
		let objState = {};
		this.setState({
			nameContactError: '',
			emailContactError: '',
			messageContactError: ''
		});
		objState[event.target.name] = event.target.value;
		this.setState(objState);
		if (event.target.name == 'emailContact') {
			if (!this.validateEmail(event.target.value)) {
				this.setState({
					emailContactError: 'Invalid email format'
				});
			}
		}
	}

	handleKeyUp(event) {
		if (event.keyCode == 13) {
			this.handleClickSend();
		}
	}
	validateEmail(emailtxt) {
		let regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (regexp.test(emailtxt)) {
			return true;
		}
		return false;
	}
	render() {
		let styleBanner = {
			backgroundImage: 'url(./images/vinneren-background.png)',
		}
		const {
			nameContact,
			nameContactError,
			emailContact,
			emailContactError,
			messageContact,
			messageContactError
		} = this.state;
		return (
			<div className="ecm-view">
				<div className="ecm-banner">
					<span className="ecm-banner-title">CLICK. SEND. ENGAGE.</span>
					<span className="ecm-banner-text">Boost your sales by reaching thousands of customers at once</span>
					<div className="ecm-banner-btn">
						<Link to={this.path}>
							<div>Get a free trial</div>
						</Link>
					</div>
				</div>
				<div className="ecm-whyus">
					<div className="ecm-whyus-section">
						<span className="ecm-whyus-section-title">Programmable SMS</span>
						<span className="ecm-whyus-section-text">Local carrier connectivity around the world that’s available with a few clicks and scales up or down with your usage.</span>
					</div>
					<div className="ecm-whyus-section">
						<span className="ecm-whyus-section-title">Programmable Voice</span>
						<span className="ecm-whyus-section-text">Local carrier connectivity around the world that’s available with a few clicks and scales up or down with your usage.</span>
					</div>
					<div className="ecm-whyus-section">
						<span className="ecm-whyus-section-title">Elastic  SIP Trunking</span>
						<span className="ecm-whyus-section-text">Local carrier connectivity around the world that’s available with a few clicks and scales up or down with your usage.</span>
					</div>
				</div>
				<div className="ecm-features">
					<div className="ecm-features-title">
						Features
					</div>
					<div className="ecm-features-section">
						<img className="ecm-features-img01" src="/images/ecm-reengage.png" />
						<span className="ecm-features-section-title">Re-engage lapsed users</span>
						<span className="ecm-features-section-text">Reach out to users who have bought from you earlier and never visited your website after that.</span>
					</div>
					<div className="ecm-features-section">
						<img className="ecm-features-img02" src="/images/ecm-cart.png" />
						<span className="ecm-features-section-title">Cart Abandonment Alert</span>
						<span className="ecm-features-section-text">Shorten purchase journey by recommending last seen product to cart drop-offs.</span>
					</div>
					<div className="ecm-features-section">
						<img className="ecm-features-img03" src="/images/ecm-time.png" />
						<span className="ecm-features-section-title">Time sensitive discounts</span>
						<span className="ecm-features-section-text">Send instant notifications about time-based offers to users who are not accessing your website.</span>
					</div>
					<div className="ecm-features-section">
						<img className="ecm-features-img04" src="/images/ecm-price-drop.png" />
						<span className="ecm-features-section-title">Price drop alert</span>
						<span className="ecm-features-section-text">Notify users when product prices drop. Send alerts to users who have visited or carted those products but haven’t bought.</span>
					</div>
					<div className="ecm-features-section">
						<img className="ecm-features-img05" src="/images/ecm-product-feed.png" />
						<span className="ecm-features-section-title">Product Feed Integration</span>
						<span className="ecm-features-section-text">Send first buy discounts on frequently bought categories /products and prompt the first sale.</span>
					</div>
					<div className="ecm-features-section">
						<img className="ecm-features-img06" src="/images/ecm-analytics.png" />
						<span className="ecm-features-section-title">Analytics</span>
						<span className="ecm-features-section-text">Analyse, optimise and tweak your campaigns based on a day to day basis.</span>
					</div>
				</div>
				<div className="ecm-pricing">
					<div className="ecm-pricing-title">
						Pricing
					</div>
					<div className="ecm-pricing-group-section">
						<div className="ecm-pricing-section">
							<span className="ecm-pricing-section-title">Free Trial</span>
							<span className="ecm-pricing-section-subtitle">Free</span>
							<span className="ecm-pricing-section-text">One month of Libraries & building blocks Customizable relevance model 10K Records/100K Operations.</span>
							<div className="ecm-pricing-section-btn">
								<Link to={this.path}>
								<div>Start your free trial</div>
								</Link>
							</div>
						</div>
						<div className="ecm-pricing-section">
							<span className="ecm-pricing-section-title">Enterprise</span>
							<span className="ecm-pricing-section-subtitle">$9,999</span>
							<span className="ecm-pricing-section-text">One month of Libraries & building blocks Customizable relevance model 10K Records/100K Operations.</span>
							<div className="ecm-pricing-section-btn">
								<Link to={this.path}>
								<div>Contact us</div>
								</Link>
							</div>
						</div>
					</div>
				</div>
				<div className="ecm-start">
					<div className="ecm-start-title">
						Getting started is really easy!
					</div>
					<div className="ecm-start-group-section">
						<div className="ecm-start-section">
							<img src="/images/ecm-start-01.png" />
							<span className="ecm-start-section-text">Add our code on your website</span>
						</div>
						<div className="ecm-start-section">
							<img src="/images/ecm-start-02.png" />
							<span className="ecm-start-section-text">Request users to opt-in for Browser Push</span>
						</div>
						<div className="ecm-start-section">
							<img src="/images/ecm-start-03.png" />
							<span className="ecm-start-section-text">Start sending web push notifications</span>
						</div>
					</div>
				</div>
				<div className="ecm-contact">
					<div className="ecm-contact-title">
						Contact Us
					</div>
					<div className="contact-form">
						<div className="contact-formelement">
							<span className="ui-float-label">
								<InputText name="nameContact"
													type="text"
													value={nameContact}
													className={(nameContact == '' ? '' : 'ui-state-filled') + (nameContactError == '' ? '' : ' error-input')}
													onChange={this.handleChange} onKeyUp={this.handleKeyUp}
													/>
								<label>Name</label>
							</span>
							<span className="contact-error">{nameContactError}</span>
						</div>
						<div className="contact-formelement">
							<span className="ui-float-label">
								<InputText name="emailContact"
													type="text"
													value={emailContact}
													className={(emailContact == '' ? '' : 'ui-state-filled') + (emailContactError == '' ? '' : ' error-input')}
													onChange={this.handleChange} onKeyUp={this.handleKeyUp}
													/>
								<label>Email</label>
							</span>
							<span className="contact-error">{emailContactError}</span>
						</div>
						<div className="contact-formelement">
							<span className="ui-float-label">
								<InputText name="messageContact"
													type="text"
													value={messageContact}
													className={(messageContact == '' ? '' : 'ui-state-filled') + (messageContactError == '' ? '' : ' error-input')}
													onChange={this.handleChange} onKeyUp={this.handleKeyUp}
													/>
								<label>Message</label>
							</span>
							<span className="contact-error">{messageContactError}</span>
						</div>
					</div>
					<div className="ecm-contact-btn">
						<div onClick={this.handleClickSend}>Send</div>
					</div>
				</div>
				<Growl ref={(el) => { this.growl = el; }}></Growl>
			</div>
		);
	}
}
