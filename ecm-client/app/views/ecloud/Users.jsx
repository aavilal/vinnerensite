import React from 'react';
import AppActions from '../../flux/actions/AppActions.jsx';
import { DataGrid } from 'primereact/components/datagrid/DataGrid';
import { Growl } from 'primereact/components/growl/Growl';
import { InputText } from 'primereact/components/inputtext/InputText';
import { Button } from 'primereact/components/button/Button';
import { Panel } from 'primereact/components/panel/Panel';
import { Dropdown } from 'primereact/components/dropdown/Dropdown';
import $ from 'jquery';

export default class Users extends React.Component {
	constructor(props) {
		super(props);
		this.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		this.state = {
			id: null,
			emailUser: '',
			emailUserError: '',
			nameUser: '',
			nameUserError: '',
			profileUser: null,
			profileUserError: ''
		};
		this.validateEmail = this.validateEmail.bind(this);
		this.handleClickAdd = this.handleClickAdd.bind(this);
		this.handleClickRefresh = this.handleClickRefresh.bind(this);
		this.handleClickDelete = this.handleClickDelete.bind(this);
		this.handleClickEdit = this.handleClickEdit.bind(this);
		this.handleClickClose = this.handleClickClose.bind(this);
		this.itemUserTemplate = this.itemUserTemplate.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleKeyUp = this.handleKeyUp.bind(this);
		this.handleClickSave = this.handleClickSave.bind(this);
		this.formatString = this.formatString.bind(this);
		this.handleClickDialog = this.handleClickDialog.bind(this);
		this.onProfileChange = this.onProfileChange.bind(this);
	}
	componentDidMount() {
		AppActions.getProfiles();
		AppActions.getUsersApp();
	}
	componentDidUpdate() {
		const {success, message} = this.props.data.user;
		if (success != null && message.length > 0) {
			if (success) {
				this.growl.show({severity: 'success', summary: 'Success', detail: message});
				this.handleClickClose();
			} else {
				this.growl.show({severity: 'error', summary: 'Error', detail: message});
			}

			AppActions.changeValue(['user', 'success'], null);
			AppActions.changeValue(['user', 'message'], '');

		}
	}
	handleClickRefresh() {
		AppActions.getUsersApp();
	}
	handleClickDialog(ev) {
		if (ev.target.className.indexOf('users-dialog') != -1) {
			this.handleClickClose();
		}
	}
	handleClickAdd() {
		let _this = this;
		let timeOut = $('.dashboard-view').scrollTop() > 0 ? 1000 : 0;
		$('.dashboard-view').animate({
          scrollTop: 0
        }, timeOut, function() {
					_this.setState({
						id: null,
						nameUser: '',
						nameUserError: '',
						emailUser: '',
						emailUserError: '',
						profileUser: null,
						profileUserError: ''
					});

					$(_this.refs.dialog).fadeIn('slow');
        });

	}
	handleClickDelete(event, data) {
		AppActions.deleteUsersApp(data);
	}
	handleClickEdit(event, data) {
		let _this = this;
		let timeOut = $('.dashboard-view').scrollTop() > 0 ? 1000 : 0;
		$('.dashboard-view').animate({
          scrollTop: 0
        }, timeOut, function() {
					_this.setState({
						id: data.id,
						nameUser: data.name,
						nameUserError: '',
						emailUser: data.email,
						emailUserError: '',
						profileUser: data.profileId,
						profileUserError: ''
					});

					$(_this.refs.dialog).fadeIn('slow');
        });

	}
	onProfileChange(e) {
		this.setState({
			emailUserError: '',
			nameUserError: '',
			profileUserError: ''
		});
		this.setState({profileUser: e.value});
	}
	handleClickClose() {
		$(this.refs.dialog).fadeOut('slow');
	}
	formatString(data) {
		return data.length > 25 ? data.substring(0, 25) + '...' : data;
	}
	itemUserTemplate(data) {
		if (!data)
    	return;

		return (
			<div style={{ padding: '5px' }} className="ui-g-12 ui-md-3">
				<Panel header={data.profileName.toUpperCase()} style={{ textAlign: 'center', background: '#FFFFFF', width: '300px', margin: '0 auto', padding: '0px' }}>
					<div className="item-img">
						<div>{data.name.substring(0,1).toUpperCase()}</div>
					</div>
					<div className="item-content">
						<label>{this.formatString(data.email)}</label>
						<span>{this.formatString(data.name)}</span>
					</div>
					<div className="item-btn">
						<Button label="Edit" onClick={(e) => this.handleClickEdit(this, data)}>
						</Button>
						<Button label="Delete" onClick={(e) => this.handleClickDelete(this, data)} >
						</Button>
					</div>
				</Panel>
			</div>
		);
	}

	handleChange(event) {
		if (this.state.id != null) {
			if (event.target.name == 'emailUser' || event.target.name == 'nameUser') {
				return;
			}
		}
		let objState = {};
		this.setState({
			emailUserError: '',
			nameUserError: '',
			profileUserError: ''
		});
		objState[event.target.name] = event.target.value;
		this.setState(objState);
		if (event.target.name == 'emailUser') {
			if (!this.validateEmail(event.target.value)) {
				this.setState({
					emailUserError: 'Invalid email format'
				});
			}
		}
	}

	handleKeyUp(event) {
		if (event.keyCode == 13) {
			this.handleClickSave();
		}
	}

	handleClickSave() {
		const {id, emailUser, emailUserError, nameUser, nameUserError, profileUser, profileUserError} = this.state;
		if (emailUser.length == 0)
			this.setState({ emailUserError: 'Required field' });
		if (nameUser.length == 0)
			this.setState({ nameUserError: 'Required field' });
		if (profileUser == null)
			this.setState({ profileUserError: 'Required field' });

		if (emailUser.length > 0 && !this.validateEmail(emailUser)) {
			this.setState({ emailUserError: 'Invalid email format' });
		}

		if (emailUser.length > 100)
			this.setState({ emailUserError: 'Maximum 100 characters' });
		if (nameUser.length > 100)
			this.setState({ nameUserError: 'Maximum 100 characters' });

		let data = {
			id: id,
			email: emailUser,
			name: nameUser,
			profileId: profileUser
		}
		if (emailUser.length > 0 && nameUser.length > 0 && emailUserError.length == 0 && profileUser != null && emailUser.length <= 100 && nameUser.length <= 100) {
			if (id == null)
				AppActions.setUsersApp(data);
			else
				AppActions.updateUsersApp(data);
		}


	}
	validateEmail(emailtxt) {
		let regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (regexp.test(emailtxt)) {
			return true;
		}
		return false;
	}
	render() {
		let es = {
			firstDayOfWeek: 1,
			dayNames: ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
    	dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
    	dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    	monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    	monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
		};
		const {
			emailUser,
			emailUserError,
			nameUser,
			nameUserError,
			profileUser,
			profileUserError
		} = this.state;
		return (
			<div className="users-view">
				<h2>Users</h2>
				<div className="users-table">
					{this.props.data.usersApp == null || this.props.data.usersApp.length == 0 ? null :
					<DataGrid value={this.props.data.usersApp}
										itemTemplate={this.itemUserTemplate}
										paginator={this.props.data.usersApp != null && this.props.data.usersApp.length > 20 ? true : false} rows={20}
										/> }
				</div>
				<div className="users-bubble" onClick={this.handleClickAdd}>
					<i class="material-icons">&#xE145;</i>
				</div>
				<div ref="dialog" className="users-dialog hide" onClick={this.handleClickDialog}>
					<div className="users-card">
						<div className="users-card-header">
							<span>User Info</span>
							<i class="material-icons" onClick={this.handleClickClose}>&#xE5CD;</i>
						</div>
						<div className="users-card-body">
							<div className="users-card-element">
								<span className="ui-float-label">
									<InputText name="emailUser"
														type="text"
														value={emailUser}
														className={(emailUser == '' ? '' : 'ui-state-filled') + (emailUserError == '' ? '' : ' error-input')}
														onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
									<label>email</label>
								</span>
								<span className="users-error">{emailUserError}</span>
							</div>
							<div className="users-card-element">
								<span className="ui-float-label">
									<InputText name="nameUser"
														type="text"
														value={nameUser}
														className={(nameUser == '' ? '' : 'ui-state-filled') + (nameUserError == '' ? '' : ' error-input')}
														onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
									<label>name</label>
								</span>
								<span className="users-error">{nameUserError}</span>
							</div>
							<div className="users-card-element">
								<Dropdown value={this.state.profileUser} options={this.props.data.profilesAll}
													onChange={this.onProfileChange} placeholder="profile" style={{ width: '100%'}}
													className={profileUserError == '' ? '' : 'error-input'}/>
								<span className="users-error">{profileUserError}</span>
							</div>
							 {this.state.id == null ?
								 <div className="users-card-element">
									 <span className="users-error" style={{ color: 'black'}}>*Remember that for new users the password of their account is <strong style={{fontSize: '12px'}}>ecloud123456</strong></span>
								 </div>
								 : null}
							<div className="users-card-element">
								<Button label="Save" onClick={this.handleClickSave}>
								</Button>
							</div>
						</div>
					</div>
				</div>
				<Growl ref={(el) => { this.growl = el; }}></Growl>
			</div>
		);
	}
}
