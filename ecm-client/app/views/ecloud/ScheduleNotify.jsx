import React from 'react';
import AppActions from '../../flux/actions/AppActions.jsx';
import { DataGrid } from 'primereact/components/datagrid/DataGrid';
import { Growl } from 'primereact/components/growl/Growl';
import { FileUpload } from 'primereact/components/fileupload/FileUpload';
import { InputText } from 'primereact/components/inputtext/InputText';
import { Button } from 'primereact/components/button/Button';
import { Calendar } from 'primereact/components/calendar/Calendar';
import { Panel } from 'primereact/components/panel/Panel';
import $ from 'jquery';

export default class ScheduleNotify extends React.Component {
	constructor(props) {
		super(props);
		this.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		this.state = {
			id: null,
			selectedNotify: null,
			visible: false,
			titleNotify: '',
			titleNotifyError: '',
			bodyNotify: '',
			bodyNotifyError: '',
			urlNotify: '',
			urlNotifyError: '',
			imgNotifyError: '',
			iconNotifyError: '',
			dateNotify: '',
			dateNotifyError: '',
			timeNotify: '',
			timeNotifyError: ''
		};
		this.validateUrl = this.validateUrl.bind(this);
		this.handleClickAdd = this.handleClickAdd.bind(this);
		this.handleClickRefresh = this.handleClickRefresh.bind(this);
		this.handleClickDelete = this.handleClickDelete.bind(this);
		this.handleClickEdit = this.handleClickEdit.bind(this);
		this.handleClickClose = this.handleClickClose.bind(this);
		this.itemNotifyTemplate = this.itemNotifyTemplate.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleChangeDate = this.handleChangeDate.bind(this);
		this.handleChangeTime = this.handleChangeTime.bind(this);
		this.handleKeyUp = this.handleKeyUp.bind(this);
		this.onBeforeUpload = this.onBeforeUpload.bind(this);
		this.onUpload = this.onUpload.bind(this);
		this.onUploadError = this.onUploadError.bind(this);
		this.handleClickSave = this.handleClickSave.bind(this);
		this.formatDate = this.formatDate.bind(this);
		this.formatString = this.formatString.bind(this);
		this.handleClickDialog = this.handleClickDialog.bind(this);
		this.deleteFile = this.deleteFile.bind(this);
	}
	componentDidMount() {
		AppActions.getPendingNofications();
	}
	componentDidUpdate() {
		const {success, message} = this.props.data.pendingNotify;
		if (success != null && message.length > 0) {
			if (success) {
				this.growl.show({severity: 'success', summary: 'Success', detail: message});
				this.handleClickClose();
			} else {
				this.growl.show({severity: 'error', summary: 'Error', detail: message});
			}
			AppActions.changeValue(['pendingNotify', 'success'], null);
			AppActions.changeValue(['pendingNotify', 'message'], '');

		}
	}
	validateUrl(url) {
		let regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
    if (regexp.test(url)) {
			return true;
		}
		return false;
	}
	handleClickRefresh() {
		AppActions.getPendingNofications();
	}
	deleteFile(type) {
		if (type == 'img') {
			AppActions.changeValue(['pendingNotify', 'bucketImgUrl'], '0');
			AppActions.changeValue(['pendingNotify', 'bucketImgName'], '0');
			AppActions.changeValue(['pendingNotify', 'imgName'], '');
		} else {
			AppActions.changeValue(['pendingNotify', 'bucketIconUrl'], '0');
			AppActions.changeValue(['pendingNotify', 'bucketIconName'], '0');
			AppActions.changeValue(['pendingNotify', 'iconName'], '');
		}
	}
	handleClickDialog(ev) {
		if (ev.target.className.indexOf('schedule-notify-dialog') != -1) {
			this.handleClickClose();
		}
	}
	handleClickAdd() {
		let _this = this;
		let timeOut = $('.dashboard-view').scrollTop() > 0 ? 1000 : 0;
		$('.dashboard-view').animate({
          scrollTop: 0
        }, timeOut, function() {
					_this.setState({
						id: null,
						selectedNotify: null,
						titleNotify: '',
						titleNotifyError: '',
						bodyNotify: '',
						bodyNotifyError: '',
						urlNotify: '',
						urlNotifyError: '',
						imgNotifyError: '',
						iconNotifyError: '',
						dateNotify: '',
						dateNotifyError: '',
						timeNotify: '',
						timeNotifyError: ''
					});
					AppActions.changeValue(['pendingNotify', 'bucketImgName'], '0');
					AppActions.changeValue(['pendingNotify', 'bucketIconName'], '0');
					AppActions.changeValue(['pendingNotify', 'imgName'], '');
					AppActions.changeValue(['pendingNotify', 'iconName'], '');

					$(_this.refs.dialog).fadeIn('slow');
        });

	}
	handleClickDelete(event, data) {
		AppActions.deleteScheduleNotify(data);
	}
	handleClickEdit(event, data) {
		let _this = this;
		let timeOut = $('.dashboard-view').scrollTop() > 0 ? 1000 : 0;
		$('.dashboard-view').animate({
          scrollTop: 0
        }, timeOut, function() {
					_this.setState({
						id: data.id,
						selectedNotify: null,
						titleNotify: data.title,
						titleNotifyError: '',
						bodyNotify: data.body,
						bodyNotifyError: '',
						urlNotify: data.url,
						urlNotifyError: '',
						imgNotifyError: '',
						iconNotifyError: '',
						dateNotify: new Date(data.dateSend),
						dateNotifyError: '',
						timeNotify: new Date(data.timeSend),
						timeNotifyError: ''
					});
					AppActions.changeValue(['pendingNotify', 'bucketImgName'], data.bucketImgName);
					AppActions.changeValue(['pendingNotify', 'bucketIconName'], data.bucketIconName);
					AppActions.changeValue(['pendingNotify', 'bucketImgUrl'], data.bucketImgUrl);
					AppActions.changeValue(['pendingNotify', 'bucketIconUrl'], data.bucketIconUrl);
					AppActions.changeValue(['pendingNotify', 'imgName'], data.imgName);
					AppActions.changeValue(['pendingNotify', 'iconName'], data.iconName);

					$(_this.refs.dialog).fadeIn('slow');
        });

	}
	handleClickClose() {
		$(this.refs.dialog).fadeOut('slow');
	}
	formatDate(date, time) {
		let dateObj = new Date(date);
		let dateTxt = this.months[dateObj.getMonth()] + ' ' + dateObj.getDate() + ', ' + dateObj.getFullYear();
		let timeObj = new Date(time);
		let timeTxt = (timeObj.getHours() < 10 ? '0' : '') + timeObj.getHours() + ':' + (timeObj.getMinutes() < 10 ? '0' : '') + timeObj.getMinutes();
		return dateTxt + ' ' + timeTxt;
	}
	formatString(data) {
		return data.length > 25 ? data.substring(0, 25) + '...' : data;
	}
	itemNotifyTemplate(data) {
		if (!data)
    	return;

		return (
			<div style={{ padding: '5px' }} className="ui-g-12 ui-md-3">
				<Panel header={this.formatDate(data.dateSend, data.timeSend)} style={{ textAlign: 'center', background: '#FFFFFF', width: '300px', margin: '0 auto', padding: '0px' }}>
					<div className="item-img">
						<img src={data.bucketImgUrl != '0' ? data.bucketImgUrl : data.bucketIconUrl}/>
					</div>
					<div className="item-content">
						<label>{this.formatString(data.title)}</label>
						<span>{this.formatString(data.body)}</span>
						<span>{this.formatString(data.url)}</span>
					</div>
					<div className="item-btn">
						<Button label="Edit" onClick={(e) => this.handleClickEdit(this, data)}>
						</Button>
						<Button label="Delete" onClick={(e) => this.handleClickDelete(this, data)} >
						</Button>
					</div>
				</Panel>
			</div>
		);
	}
	handleChangeDate(event) {
		this.setState({
			titleNotifyError: '',
			iconNotifyError: '',
			bodyNotifyError: '',
			urlNotifyError: '',
			imgNotifyError: '',
			dateNotifyError: '',
			timeNotifyError: ''
		});
		this.setState({ dateNotify: event.value });
	}
	handleChangeTime(event) {
		this.setState({
			titleNotifyError: '',
			iconNotifyError: '',
			bodyNotifyError: '',
			urlNotifyError: '',
			imgNotifyError: '',
			dateNotifyError: '',
			timeNotifyError: ''
		});
		this.setState({ timeNotify: event.value });
	}
	handleChange(event) {
		let objState = {};
		this.setState({
			titleNotifyError: '',
			iconNotifyError: '',
			bodyNotifyError: '',
			urlNotifyError: '',
			imgNotifyError: '',
			dateNotifyError: '',
			timeNotifyError: ''
		});
		objState[event.target.name] = event.target.value;
		this.setState(objState);
		if (event.target.name == 'urlNotify') {
			if (!this.validateUrl(event.target.value)) {
				this.setState({
					urlNotifyError: 'Invalid url format'
				});
			}
		}
	}

	handleKeyUp(event) {
		if (event.keyCode == 13) {
			this.handleClickSave();
		}
	}
	onBeforeUpload(event) {
		AppActions.changeValue(['loading'], true);
	}
	onUpload(event) {
		this.iconNotifyFU.fileInput.style.display = '';
		this.imgNotifyFU.fileInput.style.display = '';
		let res = JSON.parse(event.xhr.response);

		if (res.type == 'imgNotifyFU') {
			this.setState({
				imgNotifyError: '',
			});
			AppActions.changeValue(['pendingNotify', 'bucketImgUrl'], res.bucketImgUrl);
			AppActions.changeValue(['pendingNotify', 'bucketImgName'], res.bucketImgName);
			AppActions.changeValue(['pendingNotify', 'imgName'], res.imgName);

		} else {
			this.setState({
				iconNotifyError: '',
			});
			AppActions.changeValue(['pendingNotify', 'bucketIconUrl'], res.bucketImgUrl);
			AppActions.changeValue(['pendingNotify', 'bucketIconName'], res.bucketImgName);
			AppActions.changeValue(['pendingNotify', 'iconName'], res.imgName);
		}

		AppActions.changeValue(['loading'], false);
		this.growl.show({life: 5000, severity: 'success', summary: 'Success', detail: 'File Uploaded.'});

	}
	onUploadError(event) {
		this.iconNotifyFU.fileInput.style.display = '';
		this.imgNotifyFU.fileInput.style.display = '';
		let res = {
			message: "Couldn't upload image"
		}
		try {
			res = JSON.parse(event.xhr.response);
			if (res.type == 'imgNotifyFU') {
				this.setState({
					imgNotifyError: 'Error on uploading image.',
				});
				AppActions.changeValue(['pendingNotify', 'bucketImgUrl'], '0');
				AppActions.changeValue(['pendingNotify', 'bucketImgName'], '0');
				AppActions.changeValue(['pendingNotify', 'imgName'], '');
			} else {
				this.setState({
					iconNotifyError: 'Error on uploading image.',
				});
				AppActions.changeValue(['pendingNotify', 'bucketIconUrl'], '0');
				AppActions.changeValue(['pendingNotify', 'bucketIconName'], '0');
				AppActions.changeValue(['pendingNotify', 'iconName'], '');
			}
		} catch(err) {}

		AppActions.changeValue(['loading'], false);
		this.growl.show({life: 5000, severity: 'error', summary: 'Error', detail: res.message});
	}
	handleClickSave() {
		const {id, titleNotify, bodyNotify, dateNotify, timeNotify, urlNotify, urlNotifyError} = this.state;
		if (titleNotify.length == 0)
			this.setState({ titleNotifyError: 'Required field' });
		if (bodyNotify.length == 0)
			this.setState({ bodyNotifyError: 'Required field' });
		if (urlNotify.length == 0)
			this.setState({ urlNotifyError: 'Required field' });
		if (this.props.data.pendingNotify.iconName.length == 0)
			this.setState({ iconNotifyError: 'Required field' });
		if (dateNotify.length == 0)
			this.setState({ dateNotifyError: 'Required field' });
		if (timeNotify.length == 0)
			this.setState({ timeNotifyError: 'Required field' });

		if (urlNotify.length > 0 && !this.validateUrl(urlNotify)) {
			this.setState({ urlNotifyError: 'Invalid url format' });
		}

		if (titleNotify.length > 100)
			this.setState({ titleNotifyError: 'Maximum 100 characters' });
		if (bodyNotify.length > 200)
			this.setState({ bodyNotifyError: 'Maximum 200 characters' });
		if (urlNotify.length > 200)
			this.setState({ urlNotifyError: 'Maximum 200 characters' });

		let data = {
			id: id,
			title: titleNotify,
			body: bodyNotify,
			url: urlNotify,
			date: new Date(dateNotify),
			time: new Date(timeNotify)
		}
		if (titleNotify.length > 0 && bodyNotify.length > 0 && urlNotify.length > 0 && urlNotifyError.length == 0 && this.props.data.pendingNotify.iconName.length > 0 && dateNotify.length == undefined && timeNotify.length == undefined && titleNotify.length <= 100 && bodyNotify.length <= 200 && urlNotify.length <= 200) {
			if (id == null)
				AppActions.setScheduleNotify(data);
			else
				AppActions.updateScheduleNotify(data);
		}


	}
	render() {
		let es = {
			firstDayOfWeek: 1,
			dayNames: ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
    	dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
    	dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    	monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    	monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
		};
		const {
			titleNotify,
			titleNotifyError,
			iconNotifyError,
			bodyNotifyError,
			bodyNotify,
			urlNotifyError,
			urlNotify,
			selectedNotify,
			imgNotifyError,
			dateNotify,
			dateNotifyError,
			timeNotify,
			timeNotifyError
		} = this.state;
		return (
			<div className={"schedule-notify-view" + " " + this.props.deviceType}>
				<h2>Schedule Notify</h2>
				<div className="schedule-table">
					{this.props.data.pendingNotifyAll == null || this.props.data.pendingNotifyAll.length == 0 ? null :
					<DataGrid value={this.props.data.pendingNotifyAll}
										itemTemplate={this.itemNotifyTemplate}
										paginator={this.props.data.pendingNotifyAll != null && this.props.data.pendingNotifyAll.length > 20 ? true : false} rows={20}
										/> }
				</div>
				<div className="schedule-bubble" onClick={this.handleClickAdd}>
					<i class="material-icons">&#xE145;</i>
				</div>
				<div ref="dialog" className="schedule-notify-dialog hide" onClick={this.handleClickDialog}>
					<div className="quick-notify-card">
						<div className="quick-notify-card-header">
							<span>Info</span>
							<i class="material-icons" onClick={this.handleClickClose}>&#xE5CD;</i>
						</div>
						<div className="quick-notify-card-body">
							<div className="quick-notify-card-element">
								<span className="ui-float-label">
									<InputText name="titleNotify"
														type="text"
														value={titleNotify}
														className={(titleNotify == '' ? '' : 'ui-state-filled') + (titleNotifyError == '' ? '' : ' error-input')}
														size="30"
														onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
									<label>title</label>
								</span>
								<span className="notify-error">{titleNotifyError}</span>
							</div>
							<div className="quick-notify-card-element">
								<span className="ui-float-label">
									<InputText name="iconNotify"
														type="text"
														className={iconNotifyError == '' ? '' : 'error-input'}
														size="30"
														value={this.props.data.pendingNotify.iconName} onKeyUp={this.handleKeyUp}/>
									<label>icon</label>
									<i class="material-icons">&#xE226;</i>
									{this.props.data.pendingNotify.iconName == '' ? null : <i class="material-icons deletefile" onClick={(e) => this.deleteFile('icon')}>&#xE872;</i>}
									<FileUpload name="iconNotifyFU" url={"/api/upload/image/" + this.props.data.pendingNotify.bucketIconName}
															onUpload={this.onUpload} onError={this.onUploadError}
															onBeforeUpload={this.onBeforeUpload} auto={true}
															accept="image/*" mode="basic" chooseLabel="" readonly
															ref={(el) => { this.iconNotifyFU = el; }}/>
								</span>
								<span className="notify-error">{iconNotifyError}</span>
							</div>
							<div className="quick-notify-card-element">
								<span className="ui-float-label">
									<InputText name="bodyNotify"
														type="text" value={bodyNotify}
														className={(bodyNotify == '' ? '' : 'ui-state-filled') + (bodyNotifyError == '' ? '' : ' error-input')}
														size="30"
														onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
									<label>body</label>
								</span>
								<span className="notify-error">{bodyNotifyError}</span>
							</div>
							<div className="quick-notify-card-element">
								<span className="ui-float-label">
									<InputText name="urlNotify"
														type="text" value={urlNotify}
														className={(urlNotify == '' ? '' : 'ui-state-filled') + (urlNotifyError == '' ? '' : ' error-input')}
														size="30"
														onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
									<label>url</label>
								</span>
								<span className="notify-error">{urlNotifyError}</span>
							</div>
							<div className="quick-notify-card-element">
								<span className="ui-float-label">
									<InputText name="imgNotify"
														type="text"
														className={imgNotifyError == '' ? '' : 'error-input'}
														size="30"
														value={this.props.data.pendingNotify.imgName} onKeyUp={this.handleKeyUp}/>
									<label>image</label>
									<i class="material-icons">&#xE226;</i>
									{this.props.data.pendingNotify.imgName == '' ? null : <i class="material-icons deletefile" onClick={(e) => this.deleteFile('img')}>&#xE872;</i>}
									<FileUpload name="imgNotifyFU" url={"/api/upload/image/" + this.props.data.pendingNotify.bucketImgName}
															onUpload={this.onUpload} onError={this.onUploadError}
															onBeforeUpload={this.onBeforeUpload} auto={true}
															value={this.props.data.pendingNotify.imgName}
															ref={(el) => { this.imgNotifyFU = el; }}
															accept="image/*" mode="basic" chooseLabel="" readonly/>
								</span>
								<span className="notify-error">{imgNotifyError}</span>
							</div>
							<div className="quick-notify-card-element">
									<div className="quick-notify-calendar">
										<Calendar value={dateNotify} locale={es}
															dateFormat="dd/mm/yy"
															placeholder="date"
															className={dateNotifyError == '' ? '' : 'error-input'}
															minDate={new Date()} readOnlyInput={true}
															onChange={this.handleChangeDate}></Calendar>
										<span className="notify-error">{dateNotifyError}</span>
									</div>
									<div className="quick-notify-calendar">
										<Calendar value={timeNotify}
															timeOnly="true"
															placeholder="time"
															className={timeNotifyError == '' ? '' : 'error-input'}
															minDate={new Date()} readOnlyInput={true}
															onChange={this.handleChangeTime}></Calendar>
										<span className="notify-error">{timeNotifyError}</span>
									</div>
							</div>
							<div className="quick-notify-card-element">
								<Button label="Save" onClick={this.handleClickSave}>
								</Button>
							</div>
						</div>
					</div>
				</div>
				<Growl ref={(el) => { this.growl = el; }}></Growl>
			</div>
		);
	}
}
