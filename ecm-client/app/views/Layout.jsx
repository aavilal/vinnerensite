import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import $ from 'jquery';

import AppActions from '../flux/actions/AppActions.jsx';
import AppStore from '../flux/stores/AppStore.jsx';
//components
import Footer from '../components/footer/Footer.jsx';
import HeaderVinneren from '../components/header/vinneren/Header.jsx';
import HeaderEcloud from '../components/header/ecloud/Header.jsx';
import MenuLeft from '../components/menu/ecloud/MenuLeft.jsx';
import Loading from '../components/loading/Loading.jsx';

//views
import HomeVinneren from './vinneren/Home.jsx';

import HomeEcloud from './ecloud/Home.jsx';
import LoginEcloud from './ecloud/Login.jsx';
import ForgotPassword from './ecloud/ForgotPassword.jsx';
import UpdatePassword from './ecloud/UpdatePassword.jsx';
import HomeDashboard from './ecloud/HomeDashboard.jsx';
import Account from './ecloud/Account.jsx';
import QuickNotify from './ecloud/QuickNotify.jsx';
import ScheduleNotify from './ecloud/ScheduleNotify.jsx';
import History from './ecloud/History.jsx';
import Subscribers from './ecloud/Subscribers.jsx';
import Users from './ecloud/Users.jsx';
import Profiles from './ecloud/Profiles.jsx';
import Settings from './ecloud/Settings.jsx';

let getStates = () => {
	return AppStore.getData();
};

export default class Layout extends React.Component {
	constructor(props) {
		super(props);
		this.path = '/products/ecloud-messaging';
    this.state = {
			deviceType: this.params(),
			data: getStates()
		}
		this.views = {};
		this.getViews = this.getViews.bind(this);
		this._onChange = this._onChange.bind(this);
		this.getRoutes = this.getRoutes.bind(this);
    this.params = this.params.bind(this);
	}
	componentWillMount() {
		this.getViews();
		AppActions.getParams();
		AppActions.getUserInfo();
	}
	componentDidMount() {
		AppStore.addChangeListener(this._onChange);
		$(window).on('resize', () => {
			this.setState({ deviceType: this.params() });
		});
	}
	getViews() {
		this.views['QuickNotify'] = <QuickNotify {...this.state} />;
		this.views['ScheduleNotify'] = <ScheduleNotify {...this.state} />;
		this.views['History'] = <History {...this.state} />;
		this.views['Subscribers'] = <Subscribers {...this.state} />;
		this.views['Users'] = <Users {...this.state} />;
		this.views['Settings'] = <Settings {...this.state} />;
		this.views['Profiles'] = <Profiles {...this.state} />;
	}
	_onChange() {
		this.setState({data: getStates()});
	}
	componentWillUnmount() {
		AppStore.removeChangeListener(this._onChange);
	}

	params() {
		let w = $(window).width();
		if (w > 0 && w < 480) {
			return 'phonev';
		} else if (w > 479 && w < 768) {
			return 'phoneh';
		} else if (w > 467 && w < 1024) {
			return 'tabletv';
		} else if (w > 1023 && w < 1200) {
			return 'tableth';
		}
		return 'desktop';
	}

	getRoutes() {
		let _this = this;
		let routes = [];
		const { menu, session } = this.state.data.user;
		routes.push(<Route exact path='/' render={(props) => <HomeVinneren {...this.state} /> }/>);
		routes.push(<Route exact path='/our-services' render={(props) => <HomeVinneren {...this.state} /> }/>);
		routes.push(<Route exact path='/products' render={(props) => <HomeVinneren {...this.state} /> }/>);
		routes.push(<Route exact path='/contact-us' render={(props) => <HomeVinneren {...this.state} /> }/>);
		if (menu) {
			for (let i = 0; i < menu.length; i++) {
				routes.push(<Route exact path={this.path + menu[i].url} render={(props) => _this.views[menu[i].component] }/>)
			}
		}

		if (session) {
			routes.push(<Route exact path={this.path + '/'} render={(props) => <HomeDashboard {...this.state} /> }/>);
			routes.push(<Route exact path={this.path + '/account'} render={(props) => <Account {...this.state} /> }/>);
		}
		if (!session && localStorage.getItem("ecloud") == null) {
			routes.push(<Route exact path={this.path + '/'} render={(props) => <HomeEcloud {...this.state} /> }/>);
			routes.push(<Route exact path={this.path + '/features'} render={(props) => <HomeEcloud {...this.state} /> }/>);
			routes.push(<Route exact path={this.path + '/pricing'} render={(props) => <HomeEcloud {...this.state} /> }/>);
			routes.push(<Route exact path={this.path + '/contact-us'} render={(props) => <HomeEcloud {...this.state} /> }/>);
			routes.push(<Route exact path={this.path + '/login'} render={(props) => <LoginEcloud {...this.state} /> }/>);
			routes.push(<Route exact path={this.path + '/forgot-password'} render={(props) => <ForgotPassword {...this.state} /> }/>);
			routes.push(<Route exact path={this.path + '/update-password'} render={(props) => <UpdatePassword {...this.state} /> }/>);
		}
		routes.push(<Redirect from={this.path + '/*'} to={this.path + '/'} />);
		routes.push(<Redirect from='*' to='/' />);
		return routes;
	}

	render() {
		const {pathname} = window.location;
		let headerComp = (<HeaderVinneren {...this.state} />);
		if (pathname.indexOf(this.path) != -1) {
			headerComp = (<HeaderEcloud {...this.state} />);
		}
		if (!this.state.data.page)
			return null;
		let noScroll = [this.path + '/login', this.path + '/forgot-password', this.path + '/update-password']
		return (
			<div className={"main-view" + (noScroll.indexOf(pathname) != -1 ? ' no-scroll-view' : '')}>
				{headerComp}
				{this.state.data.user.session && this.state.deviceType.indexOf('phone') == -1 && pathname.indexOf(this.path) != -1 ? <MenuLeft {...this.state} /> : null}
				<div className={(this.state.data.user.session && pathname.indexOf(this.path) != -1 ? "dashboard-view " : "content-view ") + this.state.deviceType}>
					<Switch>
						{this.getRoutes()}
					</Switch>
					<Loading {...this.state} />
				</div>
				{this.state.data.user.session && pathname.indexOf(this.path) != -1 ? null : <Footer {...this.state} />}
			</div>
		);
	}
}
