import AppDispatcher from '../dispatcher/AppDispatcher.jsx';
import System from '../constants/System.jsx';

let AppActions = {
	getLogin(email, password) {
		AppDispatcher.dispatch({
			actionType: System.GET_LOGIN,
			email: email,
			password: password
		});
	},
	updateUser(data) {
		AppDispatcher.dispatch({
			actionType: System.UPDATE_USER,
			data: data
		});
	},
	getUserInfo() {
		AppDispatcher.dispatch({
			actionType: System.GET_USER_INFO
		});
	},
	getLogout() {
		AppDispatcher.dispatch({
			actionType: System.GET_LOGOUT
		});
	},
	changeValue(attributes, value) {
		AppDispatcher.dispatch({
			actionType: System.CHANGE_VALUE,
			attributes: attributes,
			value: value
		});
	},
	sendNotify(data) {
		AppDispatcher.dispatch({
			actionType: System.SEND_NOTIFY,
			data: data
		});
	},
	getPendingNofications() {
		AppDispatcher.dispatch({
			actionType: System.GET_PENDING_NOTIFICATIONS
		});
	},
	setScheduleNotify(data) {
		AppDispatcher.dispatch({
			actionType: System.SCHEDULE_NOTIFY,
			data: data,
			type: 'add'
		});
	},
	deleteScheduleNotify(data) {
		AppDispatcher.dispatch({
			actionType: System.SCHEDULE_NOTIFY,
			data: data,
			type: 'del'
		});
	},
	updateScheduleNotify(data) {
		AppDispatcher.dispatch({
			actionType: System.SCHEDULE_NOTIFY,
			data: data,
			type: 'mod'
		});
	},
	getSubscribers() {
		AppDispatcher.dispatch({
			actionType: System.GET_SUBSCRIBERS
		});
	},
	getClicks() {
		AppDispatcher.dispatch({
			actionType: System.GET_CLICKS
		});
	},
	getHistory() {
		AppDispatcher.dispatch({
			actionType: System.GET_HISTORY
		});
	},
	getUsersApp() {
		AppDispatcher.dispatch({
			actionType: System.GET_USERS_APP
		});
	},
	setUsersApp(data) {
		AppDispatcher.dispatch({
			actionType: System.USERS_APP,
			data: data,
			type: 'add'
		});
	},
	deleteUsersApp(data) {
		AppDispatcher.dispatch({
			actionType: System.USERS_APP,
			data: data,
			type: 'del'
		});
	},
	updateUsersApp(data) {
		AppDispatcher.dispatch({
			actionType: System.USERS_APP,
			data: data,
			type: 'mod'
		});
	},
	getMenuOptions() {
		AppDispatcher.dispatch({
			actionType: System.GET_MENU_OPTIONS
		});
	},
	getProfiles() {
		AppDispatcher.dispatch({
			actionType: System.GET_PROFILES
		});
	},
	setProfiles(data) {
		AppDispatcher.dispatch({
			actionType: System.PROFILES,
			data: data,
			type: 'add'
		});
	},
	deleteProfiles(data) {
		AppDispatcher.dispatch({
			actionType: System.PROFILES,
			data: data,
			type: 'del'
		});
	},
	updateProfiles(data) {
		AppDispatcher.dispatch({
			actionType: System.PROFILES,
			data: data,
			type: 'mod'
		});
	},
	sendContactUs(data)  {
		AppDispatcher.dispatch({
			actionType: System.SEND_CONTACT_US,
			data: data
		});
	},
	setForgotPassword(email) {
		AppDispatcher.dispatch({
			actionType: System.SEND_FORGOT_PWD,
			email: email
		});
	},
	validateURL(id) {
		AppDispatcher.dispatch({
			actionType: System.VALIDATE_URL,
			id: id
		});
	},
	updatePassword(id, password) {
		AppDispatcher.dispatch({
			actionType: System.UPDATE_PASSWORD,
			id: id,
			password: password
		});
	},
	getParams() {
		AppDispatcher.dispatch({
			actionType: System.GET_PARAMS
		});
	},
	updateParams(data) {
		AppDispatcher.dispatch({
			actionType: System.UPDATE_PARAMS,
			data: data
		});
	}
};

module.exports = AppActions;
