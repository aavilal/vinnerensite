import keyMirror from 'keymirror';

module.exports = keyMirror({
  GET_LOGIN: null,
  UPDATE_USER: null,
  GET_USER_INFO: null,
  GET_LOGOUT: null,
  CHANGE_VALUE: null,
  SEND_NOTIFY: null,
  GET_PENDING_NOTIFICATIONS: null,
  SCHEDULE_NOTIFY: null,
  GET_SUBSCRIBERS: null,
  GET_CLICKS: null,
  GET_HISTORY: null,
  GET_USERS_APP: null,
  USERS_APP: null,
  GET_MENU_OPTIONS: null,
  GET_PROFILES: null,
  PROFILES: null,
  SEND_CONTACT_US: null,
  SEND_FORGOT_PWD: null,
  VALIDATE_URL: null,
  UPDATE_PASSWORD: null,
  GET_PARAMS: null,
  UPDATE_PARAMS: null
});
