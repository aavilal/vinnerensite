import AppDispatcher from '../dispatcher/AppDispatcher.jsx';
import System from '../constants/System.jsx';
import { EventEmitter } from 'events';
import assign from 'object-assign';
import $ from 'jquery';

let CHANGE_EVENT = 'change';


$.ajaxSetup({
  async: true,
	crossDomain: true,
  statusCode: {
    203: () => {
        AppData.data.loading = false;
        AppData.data.user = {
          email: '',
          name: '',
          avatar: '0',
          profile: '',
          token: '',
          session: false,
          menu: [],
          messageError: ''
        };
        AppStore.emitChange();
    }
  }
});

let AppData = {
	data: {
    page: false,
    contactus: {
      success: null,
      message: ''
    },
    updatepwd: {
      show: null,
      success: true,
      updated: false,
      message: ''
    },
    loading: false,
    ask: {
      success: null,
      message: '',
      bucketImgUrl: '0',
      bucketImgName: '0',
      imgName: ''
    },
    params: [],
    notify: {
      success: null,
      message: '',
      bucketIconUrl: '0',
      bucketIconName: '0',
      iconName: '',
      bucketImgUrl: '0',
      bucketImgName: '0',
      imgName: ''
    },
    pendingNotify: {
      success: null,
      message: '',
      bucketIconUrl: '0',
      bucketIconName: '0',
      iconName: '',
      bucketImgUrl: '0',
      bucketImgName: '0',
      imgName: ''
    },
    pendingNotifyAll: [],
    profiles: {
      success: null,
      message: ''
    },
    menuOptions: [],
    profilesAll: [],
    usersApp: [],
    subscribers: [],
    history: [],
    home: {
      today: 0,
      week: 0,
      month: 0
    },
		user: {
      id: 0,
      email: '',
      name: '',
      avatar: '0',
      avatarName: '0',
      profile: '',
      token: '',
      session: false,
      menu: [],
      success: true,
      message: '',
      messageError: '',
      invalidPassword: false
    }
	},
	getLogin(action) {
    AppData.data.loading = true;
    AppStore.emitChange();
    $.ajax({
        type: 'POST',
        url: '/api/login',
        dataType: 'json',
        data: {
          email: action.email,
          password: action.password
        },
        success: (response) => {
          if (response.success) {
            const {avatar, name, email, id_cat_profile, id_ent_users, token, avatar_name} = response.user;
            AppData.data.user = {
              avatar, name, email, avatarName: avatar_name,
              profile: id_cat_profile, id: id_ent_users, token
            };
            AppData.data.user.session = true;
            try {
              localStorage.setItem("ecloud", AppData.data.user.token);
            } catch(e){}

            $.ajax({
                type: 'POST',
                url: '/api/menu',
                dataType: 'json',
                headers : {
                  authorization: 'Bearer ' + localStorage.getItem("ecloud")
                },
                data: {
                  profileuser: AppData.data.user.profile
                },
                success: (response) => {
                  if (response.success) {
                    AppData.data.user.menu = response.profile.menu
                    AppStore.emitChange();
                  }
                }
            });

          } else {
            AppData.data.user.messageError = response.message;
            AppData.data.user.session = false;
          }
          AppData.data.loading = false;
          AppStore.emitChange();
        },
        error: (e) => {
          AppData.data.user.session = false;
          AppData.data.user.messageError = 'An error ocurred on Log in.';
          AppData.data.loading = false;
          AppStore.emitChange();
        }
    });
	},
  updateUser(action) {
    AppData.data.loading = true;
    AppStore.emitChange();
    $.ajax({
        type: 'POST',
        url: '/api/update/user',
        dataType: 'json',
        data: action.data,
        headers : {
          authorization: 'Bearer ' + localStorage.getItem("ecloud")
        },
        success: (response) => {
          AppData.data.user.invalidPassword = false;
          if (response.success) {
            AppData.data.user.success = true;
            AppData.data.user.name = response.user.name;
            AppData.data.user.avatar = response.user.avatar;
            AppData.data.user.avatarName = response.user.avatarName;
            AppData.data.user.message = response.message;
          }
          if (response.pwdinvalid) {
            AppData.data.user.invalidPassword = true;
          }
          AppData.data.loading = false;
          AppStore.emitChange();
        },
        error: (err) => {
          AppData.data.user.invalidPassword = false;
          AppData.data.user.success = false;
          AppData.data.user.message = 'Error ocurred in update user.';
          AppData.data.loading = false;
          AppStore.emitChange();
        }
    });
  },
  getUserInfo() {
    let userId;
    let token = localStorage.getItem("ecloud");
    if (token == null) {
      AppData.data.page = true;
      AppData.data.user.session = false;
      try {
        localStorage.removeItem("ecloud");
      } catch(e) {}
      AppStore.emitChange();
      return;
    }
    $.ajax({
        type: 'POST',
        url: '/api/logininfo',
        dataType: 'json',
        headers : {
          authorization: 'Bearer ' + localStorage.getItem("ecloud")
        },
        success: (response) => {
          if (response.success) {
            const {avatar, name, email, id_cat_profile, id_ent_users, token, avatar_name} = response.user;
            AppData.data.user = {
              avatar, name, email, avatarName: avatar_name,
              profile: id_cat_profile, id: id_ent_users, token
            };
            AppData.data.user.session = true;
            try {
              localStorage.setItem("ecloud", AppData.data.user.token);
            } catch(e){}

            $.ajax({
                type: 'POST',
                url: '/api/menu',
                dataType: 'json',
                data: {
                  profileuser: AppData.data.user.profile
                },
                headers : {
                  authorization: 'Bearer ' + localStorage.getItem("ecloud")
                },
                success: (response) => {
                  AppData.data.page = true;
                  if (response.success) {
                    AppData.data.user.menu = response.profile.menu
                    AppStore.emitChange();
                  }
                }
            });

          } else {
            AppData.data.page = true;
            localStorage.removeItem("ecloud");
            AppData.data.user.session = false;
            AppStore.emitChange();
          }

        },
        error: (e) => {
          AppData.data.page = true;
          try {
            localStorage.removeItem("ecloud");
          } catch(e) {}
          AppData.data.user.session = false;
          AppStore.emitChange();
        }
    });
  },
  getLogout() {
    try {
      localStorage.removeItem("ecloud");
    } catch(e) {}

    AppData.data.user = {
      email: '',
      name: '',
      avatar: '',
      profile: '',
      token: '',
      session: false,
      menu: [],
      messageError: ''
    }
    AppStore.emitChange();
  },
  changeValue(action) {
    const {attributes} = action;
    switch (attributes.length) {
      case 1:
        AppData.data[attributes[0]] = action.value;
        break;
      case 2:
        AppData.data[attributes[0]][attributes[1]] = action.value;
        break;
      case 3:
        AppData.data[attributes[0]][attributes[1]][attributes[2]] = action.value;
        break;
      case 4:
        AppData.data[attributes[0]][attributes[1]][attributes[2]][attributes[3]] = action.value;
        break;
      default:
    }
    AppStore.emitChange();
  },
  sendNotify(action) {
    AppData.data.loading = true;
    AppStore.emitChange();
    action.data.bucketImgUrl = AppData.data.notify.bucketImgUrl;
    action.data.bucketImgName = AppData.data.notify.bucketImgName;
    action.data.imgName = AppData.data.notify.imgName;
    action.data.bucketIconUrl = AppData.data.notify.bucketIconUrl;
    action.data.bucketIconName = AppData.data.notify.bucketIconName;
    action.data.iconName = AppData.data.notify.iconName;

    $.ajax({
        type: 'POST',
        url: '/api/quick-notify',
        dataType: 'json',
        data: action.data,
        success: (response) => {
          AppData.data.notify.success = response.success;
          AppData.data.notify.message = response.message;
          AppData.data.loading = false;
          AppStore.emitChange();
        },
        error: (err) => {
          AppData.data.notify.success = false;
          AppData.data.notify.message = 'Error ocurred sending notification.';
          AppData.data.loading = false;
          AppStore.emitChange();
        }
      });
  },
  getPendingNofications() {
    AppData.data.loading = true;
    AppStore.emitChange();
    $.ajax({
        type: 'GET',
        url: '/api/schedule-notify',
        dataType: 'json',
        success: (response) => {
          if (response.success) {
            AppData.data.pendingNotify.success = null;
            AppData.data.pendingNotifyAll = [];
            for (let i = 0; i < response.pending.length; i++) {
                AppData.data.pendingNotifyAll.push({
                  id: response.pending[i].hash,
                  dateSend: response.pending[i].date_send,
                  timeSend: response.pending[i].time_send,
                  title: response.pending[i].title,
                  body: response.pending[i].body,
                  iconName: response.pending[i].icon_name,
                  bucketIconName: response.pending[i].bucket_icon_name,
                  bucketIconUrl: response.pending[i].bucket_icon_url,
                  imgName: response.pending[i].img_name,
                  bucketImgName: response.pending[i].bucket_img_name,
                  bucketImgUrl: response.pending[i].bucket_img_url,
                  type: response.pending[i].type,
                  url: response.pending[i].url,
                  clicks: response.pending[i].clicks
                });
            }
          } else {
            AppData.data.pendingNotify.success = false;
            AppData.data.pendingNotifyAll = [];
            AppData.data.pendingNotify.message = response.message;
          }

          AppData.data.loading = false;
          AppStore.emitChange();
        },
        error: (err) => {
          AppData.data.pendingNotify.success = false;
          AppData.data.pendingNotify.message = 'Error ocurred getting pending notifications.';
          AppData.data.loading = false;
          AppStore.emitChange();
        }
      });
  },
  scheduleNotify(action) {
    AppData.data.loading = true;
    AppStore.emitChange();
    let date;
    let time;
    if (action.type != 'del') {
      date = action.data.date.getFullYear() + '-';
      date += (action.data.date.getMonth() + 1 < 10 ? '0' : '') + (action.data.date.getMonth() + 1) + '-';
      date += (action.data.date.getSeconds() < 10 ? '0' : '') + action.data.date.getDate();

      time = action.data.time.getFullYear() + '-';
      time += (action.data.time.getMonth() + 1 < 10 ? '0' : '') + (action.data.time.getMonth() + 1) + '-';
      time += (action.data.time.getSeconds() < 10 ? '0' : '') + action.data.time.getDate() + ' ';
      time += (action.data.time.getHours() < 10 ? '0' : '') + action.data.time.getHours() + ':';
      time += (action.data.time.getMinutes() < 10 ? '0' : '') + action.data.time.getMinutes() + ':';
      time += (action.data.time.getSeconds() < 10 ? '0' : '') + action.data.time.getSeconds();
    }

    let data = {
      id: action.data.id,
      title: action.data.title,
      body: action.data.body,
      url: action.data.url,
      iconName: AppData.data.pendingNotify.iconName,
      bucketIconName: AppData.data.pendingNotify.bucketIconName,
      bucketIconUrl: AppData.data.pendingNotify.bucketIconUrl,
      imgName: AppData.data.pendingNotify.imgName,
      bucketImgName: AppData.data.pendingNotify.bucketImgName,
      bucketImgUrl: AppData.data.pendingNotify.bucketImgUrl,
      dateSend: date,
      timeSend: time,
    }
    $.ajax({
        type: 'POST',
        url: '/api/' + action.type + '/schedule-notify',
        dataType: 'json',
        data: data,
        success: (response) => {
          if (response.success) {
            AppData.data.pendingNotify.success = true;
            AppData.data.pendingNotify.message = response.message;
            AppData.data.pendingNotifyAll = [];
            for (let i = 0; i < response.pending.length; i++) {
                AppData.data.pendingNotifyAll.push({
                  id: response.pending[i].hash,
                  dateSend: response.pending[i].date_send,
                  timeSend: response.pending[i].time_send,
                  title: response.pending[i].title,
                  body: response.pending[i].body,
                  iconName: response.pending[i].icon_name,
                  bucketIconName: response.pending[i].bucket_icon_name,
                  bucketIconUrl: response.pending[i].bucket_icon_url,
                  imgName: response.pending[i].img_name,
                  bucketImgName: response.pending[i].bucket_img_name,
                  bucketImgUrl: response.pending[i].bucket_img_url,
                  type: response.pending[i].type,
                  url: response.pending[i].url,
                  clicks: response.pending[i].clicks
                });
            }
          } else {
            AppData.data.pendingNotify.success = false;
            AppData.data.pendingNotifyAll = [];
            AppData.data.pendingNotify.message = response.message;
          }

          AppData.data.loading = false;
          AppStore.emitChange();
        },
        error: (err) => {
          AppData.data.pendingNotify.success = false;
          AppData.data.pendingNotify.message = 'Error ocurred in pending notifications.';
          AppData.data.loading = false;
          AppStore.emitChange();
        }
    });
  },
  getSubscribers() {
    AppData.data.loading = true;
    AppStore.emitChange();
    $.ajax({
        type: 'GET',
        url: '/api/subscribers',
        dataType: 'json',
        success: (response) => {
          AppData.data.subscribers = [];
          for (let i = 0; i < response.subscribers.length; i++) {
              AppData.data.subscribers.push({
                token: response.subscribers[i].token,
                osName: response.subscribers[i].os_name,
                osVersion: response.subscribers[i].os_version,
                browserName: response.subscribers[i].browser_name,
                browserVersion: response.subscribers[i].browser_version,
                dateRegistered: response.subscribers[i].date_registered
              });
          }
          AppData.data.loading = false;
          AppStore.emitChange();
        },
        error: (err) => {
          AppData.data.subscribers = [];
          AppData.data.loading = false;
          AppStore.emitChange();
        }
      });
  },
  getClicks() {
    AppData.data.loading = true;
    AppStore.emitChange();

    $.ajax({
        type: 'GET',
        url: '/api/count-clicks',
        dataType: 'json',
        success: (response) => {
          AppData.data.home = {
            today: response.clicks.clicks_today,
            week: response.clicks.clicks_week,
            month: response.clicks.clicks_month
          };
          AppData.data.loading = false;
          AppStore.emitChange();
        },
        error: (err) => {
          AppData.data.home = {
            today: 0,
            week: 0,
            month: 0
          };
          AppData.data.loading = false;
          AppStore.emitChange();
        }
      });
  },
  getHistory() {
    AppData.data.loading = true;
    AppStore.emitChange();

    $.ajax({
        type: 'GET',
        url: '/api/history',
        dataType: 'json',
        success: (response) => {
          AppData.data.history = [];
          for (let i = 0; i < response.history.length; i++) {
              AppData.data.history.push({
                id: response.history[i].hash,
                dateSend: response.history[i].date_send,
                timeSend: response.history[i].time_send,
                title: response.history[i].title,
                body: response.history[i].body,
                iconName: response.history[i].icon_name,
                bucketIconName: response.history[i].bucket_icon_name,
                bucketIconUrl: response.history[i].bucket_icon_url,
                imgName: response.history[i].img_name,
                bucketImgName: response.history[i].bucket_img_name,
                bucketImgUrl: response.history[i].bucket_img_url,
                type: response.history[i].type,
                url: response.history[i].url,
                clicks: response.history[i].clicks
              });
          }
          AppData.data.loading = false;
          AppStore.emitChange();
        },
        error: (err) => {
          AppData.data.history = [];
          AppData.data.loading = false;
          AppStore.emitChange();
        }
      });
  },
  getUsersApp() {
    AppData.data.loading = true;
    AppStore.emitChange();
    $.ajax({
        type: 'POST',
        url: '/api/users-app',
        dataType: 'json',
        success: (response) => {
          if (response.success) {
            AppData.data.user.success = null;
            AppData.data.usersApp = [];
            for (let i = 0; i < response.usersapp.length; i++) {
                AppData.data.usersApp.push({
                  id: response.usersapp[i].id_ent_users,
                  email: response.usersapp[i].email,
                  name: response.usersapp[i].name,
                  profileId: response.usersapp[i].id_cat_profile,
                  profileName: response.usersapp[i].profile_name
                });
            }
          } else {
            AppData.data.user.success = false;
            AppData.data.usersApp = [];
            AppData.data.user.message = response.message;
          }

          AppData.data.loading = false;
          AppStore.emitChange();
        },
        error: (err) => {
          AppData.data.user.success = false;
          AppData.data.user.message = 'Error ocurred getting users.';
          AppData.data.loading = false;
          AppStore.emitChange();
        }
      });
  },
  usersApp(action) {
    AppData.data.loading = true;
    AppStore.emitChange();

    $.ajax({
        type: 'POST',
        url: '/api/' + action.type + '/users-app',
        dataType: 'json',
        data: {
          id: action.data.id,
          name: action.data.name,
          email:  action.data.email,
          profileId: action.data.profileId
        },
        success: (response) => {
          AppData.data.user.success = response.success;
          AppData.data.user.message = response.message;
          if (response.usersapp != null) {
            AppData.data.usersApp = [];
            for (let i = 0; i < response.usersapp.length; i++) {
                AppData.data.usersApp.push({
                  id: response.usersapp[i].id_ent_users,
                  email: response.usersapp[i].email,
                  name: response.usersapp[i].name,
                  profileId: response.usersapp[i].id_cat_profile,
                  profileName: response.usersapp[i].profile_name
                });
            }
          }
          AppData.data.loading = false;
          AppStore.emitChange();
        },
        error: (err) => {
          AppData.data.user.success = false;
          AppData.data.user.message = 'Error ocurred in users.';
          AppData.data.loading = false;
          AppStore.emitChange();
        }
    });
  },
  getMenuOptions() {
    AppData.data.loading = true;
    AppStore.emitChange();
    $.ajax({
        type: 'POST',
        url: '/api/menu-options',
        dataType: 'json',
        success: (response) => {
          if (response.success) {
            AppData.data.menuOptions = [];
            for (let i = 0; i < response.menuOptions.length; i++) {
                AppData.data.menuOptions.push({
                  id: response.menuOptions[i].id_cat_menu,
                  name: response.menuOptions[i].name
                });
            }
          } else {
            AppData.data.menuOptions = [];
          }

          AppData.data.loading = false;
          AppStore.emitChange();
        },
        error: (err) => {
          AppData.data.loading = false;
          AppStore.emitChange();
        }
      });
  },
  getProfiles() {
    AppData.data.loading = true;
    AppStore.emitChange();
    $.ajax({
        type: 'POST',
        url: '/api/profiles',
        dataType: 'json',
        success: (response) => {
          if (response.success) {
            AppData.data.profiles.success = null;
            AppData.data.profilesAll = [];
            for (let i = 0; i < response.profiles.length; i++) {
                AppData.data.profilesAll.push({
                  id: response.profiles[i].id_cat_profile,
                  name: response.profiles[i].name,
                  description: response.profiles[i].description,
                  menuOptions: response.profiles[i].menu_options,
                  label: response.profiles[i].name,
                  value: response.profiles[i].id_cat_profile
                });
            }
          } else {
            AppData.data.profiles.success = false;
            AppData.data.profilesAll = [];
            AppData.data.profiles.message = response.message;
          }

          AppData.data.loading = false;
          AppStore.emitChange();
        },
        error: (err) => {
          AppData.data.profiles.success = false;
          AppData.data.profiles.message = 'Error ocurred getting users.';
          AppData.data.loading = false;
          AppStore.emitChange();
        }
      });
  },
  profiles(action) {
    AppData.data.loading = true;
    AppStore.emitChange();
    $.ajax({
        type: 'POST',
        url: '/api/' + action.type + '/profiles',
        dataType: 'json',
        data: {
          id: action.data.id,
          name: action.data.name,
          description:  action.data.description,
          menuOptions: action.data.menuOptions.toString()
        },
        success: (response) => {
          AppData.data.profiles.success = response.success;
          AppData.data.profiles.message = response.message;
          if (response.profiles != null) {
            AppData.data.profilesAll = [];
            for (let i = 0; i < response.profiles.length; i++) {
                AppData.data.profilesAll.push({
                  id: response.profiles[i].id_cat_profile,
                  name: response.profiles[i].name,
                  description: response.profiles[i].description,
                  menuOptions: response.profiles[i].menu_options
                });
            }
          }

          AppData.data.loading = false;
          AppStore.emitChange();
        },
        error: (err) => {
          AppData.data.profiles.success = false;
          AppData.data.profiles.message = 'Error ocurred getting users.';
          AppData.data.loading = false;
          AppStore.emitChange();
        }
    });
  },
  sendContactUs(action) {
    AppData.data.loading = true;
    AppStore.emitChange();
    $.ajax({
        type: 'POST',
        url: '/api/contact-us',
        dataType: 'json',
        data: {
          name: action.data.name,
          email:  action.data.email.trim(),
          message: action.data.message
        },
        success: (response) => {
          AppData.data.contactus.success = response.success;
          AppData.data.contactus.message = response.message;
          AppData.data.loading = false;
          AppStore.emitChange();
        },
        error: (err) => {
          AppData.data.contactus.success = false;
          AppData.data.contactus.message = 'Error sending message.';
          AppData.data.loading = false;
          AppStore.emitChange();
        }
    });
  },
  setForgotPassword(action) {
    AppData.data.loading = true;
    AppStore.emitChange();
    $.ajax({
        type: 'POST',
        url: '/api/forgot-pwd',
        dataType: 'json',
        data: {
          email:  action.email.trim()
        },
        success: (response) => {
          AppData.data.user.success = response.success;
          AppData.data.user.message = response.message;
          AppData.data.loading = false;
          AppStore.emitChange();
        },
        error: (err) => {
          AppData.data.user.success = false;
          AppData.data.user.message = 'Error sending email for password recovery.';
          AppData.data.loading = false;
          AppStore.emitChange();
        }
    });
  },
  validateURL(action) {
    AppData.data.loading = true;
    AppStore.emitChange();
    $.ajax({
        type: 'POST',
        url: '/api/validate-url',
        dataType: 'json',
        data: {
          id:  action.id
        },
        success: (response) => {
          AppData.data.updatepwd.success = response.success;
          AppData.data.updatepwd.show = response.success;
          AppData.data.updatepwd.message = response.message;
          AppData.data.loading = false;
          AppStore.emitChange();
        },
        error: (err) => {
          AppData.data.updatepwd.success = false;
          AppData.data.updatepwd.show = null;
          AppData.data.updatepwd.message = 'Error validating token from url.';
          AppData.data.loading = false;
          AppStore.emitChange();
        }
    });
  },
  updatePassword(action) {
    AppData.data.loading = true;
    AppStore.emitChange();
    $.ajax({
        type: 'POST',
        url: '/api/update-password',
        dataType: 'json',
        data: {
          id:  action.id,
          password:  action.password
        },
        success: (response) => {
          AppData.data.updatepwd.success = response.success;
          AppData.data.updatepwd.show = response.success;
          AppData.data.updatepwd.updated = response.success;
          AppData.data.updatepwd.message = response.message;
          AppData.data.loading = false;
          AppStore.emitChange();
        },
        error: (err) => {
          AppData.data.updatepwd.success = false;
          AppData.data.updatepwd.show = null;
          AppData.data.updatepwd.message = 'Error validating token from url.';
          AppData.data.loading = false;
          AppStore.emitChange();
        }
    });
  },
  updateParams(action) {
    AppData.data.loading = true;
    AppStore.emitChange();
    $.ajax({
        type: 'POST',
        url: '/api/params',
        dataType: 'json',
        data: {
          params: JSON.stringify(action.data)
        },
        success: (response) => {
          AppData.data.ask.success = response.success;
          AppData.data.ask.message = response.message;
          AppData.data.loading = false;
          AppStore.emitChange();
        },
        error: (e) => {
          AppData.data.ask.success = false;
          AppData.data.ask.message = 'Error saving params.';
          AppData.data.loading = false;
          AppStore.emitChange();
        }
    });
	},
  getParams(action) {
    $.ajax({
        type: 'GET',
        url: '/api/params',
        dataType: 'json',
        success: (response) => {
          AppData.data.params = response.params;
          AppStore.emitChange();
        },
        error: (e) => {
          AppData.data.params = [];
          AppStore.emitChange();
        }
    });
	}
};

let AppStore = assign({}, EventEmitter.prototype, {
	emitChange: function() {
		this.emit(CHANGE_EVENT);
	},
	addChangeListener: function(callback) {
		this.on(CHANGE_EVENT, callback);
	},
	removeChangeListener: function (callback) {
		this.removeListener(CHANGE_EVENT, callback);
	}
});

AppStore = assign({}, AppStore, {
	getData: () => {
		return AppData.data;
	}
});

AppDispatcher.register((action) => {
	switch (action.actionType) {
		case System.GET_LOGIN:
			AppData.getLogin(action);
			break;
    case System.UPDATE_USER:
			AppData.updateUser(action);
			break;
    case System.GET_USER_INFO:
      AppData.getUserInfo();
      break;
    case System.GET_LOGOUT:
  		AppData.getLogout();
      break;
    case System.CHANGE_VALUE:
  		AppData.changeValue(action);
  		break;
    case System.SEND_NOTIFY:
  		AppData.sendNotify(action);
  		break;
    case System.GET_PENDING_NOTIFICATIONS:
      AppData.getPendingNofications();
      break;
    case System.SCHEDULE_NOTIFY:
      AppData.scheduleNotify(action);
      break;
    case System.GET_SUBSCRIBERS:
      AppData.getSubscribers();
      break;
    case System.GET_CLICKS:
      AppData.getClicks();
      break;
    case System.GET_HISTORY:
      AppData.getHistory();
      break;
    case System.GET_USERS_APP:
      AppData.getUsersApp();
      break;
    case System.USERS_APP:
      AppData.usersApp(action);
      break;
    case System.GET_PROFILES:
      AppData.getProfiles();
      break;
    case System.PROFILES:
      AppData.profiles(action);
      break;
    case System.GET_MENU_OPTIONS:
      AppData.getMenuOptions(action);
      break;
    case System.SEND_CONTACT_US:
      AppData.sendContactUs(action);
      break;
    case System.SEND_FORGOT_PWD:
      AppData.setForgotPassword(action);
      break;
    case System.VALIDATE_URL:
      AppData.validateURL(action);
      break;
    case System.UPDATE_PASSWORD:
      AppData.updatePassword(action);
      break;
    case System.GET_PARAMS:
      AppData.getParams();
      break;
    case System.UPDATE_PARAMS:
      AppData.updateParams(action);
      break;
		default:
				// no op
	}
});

module.exports = AppStore;
