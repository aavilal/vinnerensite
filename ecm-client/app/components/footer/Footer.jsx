import React from 'react';
import { Link } from 'react-router-dom';

export default class Footer extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
      <div className={"vinneren-footer " + this.props.deviceType}>
        <div className="vinneren-footer-social">
          <Link to="https://www.facebook.com/atencion.vinneren.9" target="_blank">
            <i className="fa fa-facebook-square"></i>
          </Link>
          <Link to="https://twitter.com/VinnerenMX" target="_blank">
            <i className="fa fa-twitter-square"></i>
          </Link>
          <Link to="https://www.linkedin.com/company/vinneren-co/" target="_blank">
            <i className="fa fa-linkedin-square"></i>
          </Link>
        </div>
        <div className="vinneren-footer-copyright">
          <span>© 2018 Vinneren | All Rights Reserved.</span>
        </div>
        <div className="vinneren-footer-links">
          <Link to="/">
            <span>Legal Stuff</span>
          </Link>
          <Link to="/">
            <span>Privacy Policy</span>
          </Link>
        </div>
      </div>
		);
	}
}
