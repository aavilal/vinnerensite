import React from 'react';

export default class Loading extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		const {loading} = this.props.data;
		return (
			<div className={"loading-component " + (loading ? 'show' : 'hide')}>
        <div className="loading-content">
					<img src="./images/loading.gif"/>
        </div>
			</div>
		);
	}
}
