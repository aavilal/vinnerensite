import React from 'react';
import { Link } from 'react-router-dom';
import MenuTop from '../../menu/vinneren/MenuTop.jsx';
import MenuMobile from '../../menu/vinneren/MenuMobile.jsx';
import HamburgerMenu from 'react-hamburger-menu';
import $ from 'jquery';

export default class Header extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			open: false
		}
		this.handleClickLogo = this.handleClickLogo.bind(this);
		this.handleClickMenuMobile = this.handleClickMenuMobile.bind(this);
	}
	handleClickLogo(e) {
		$('.main-view').animate({
          scrollTop: 0
        }, 1000, function() {
    });
	}
	handleClickMenuMobile() {
		this.setState({open: !this.state.open});
	}
	render() {
		let menuBtnMobile = (
			<HamburgerMenu
				isOpen={this.state.open}
				menuClicked={this.handleClickMenuMobile.bind(this)}
				width={20}
				height={10}
				strokeWidth={2}
				rotate={0}
				color='white'
				borderRadius={3}
				animationDuration={0.5}
			/>
		);
		let menu = this.props.deviceType.indexOf('phone') != -1 ? <MenuMobile toogle={this.handleClickMenuMobile} open={this.state.open} {...this.props} /> : <MenuTop {...this.props} />
		return (
			<div className="header-component">
				<div className="header-menu-btn">
					{this.props.deviceType.indexOf('phone') != -1 ? menuBtnMobile : null}
				</div>
				<Link to="/" onClick={()=>this.handleClickLogo()}>
					<img src="./images/vinneren-logo.png"/>
				</Link>
				{menu}
			</div>
		);
	}
}
