import React from 'react';
import { Link } from 'react-router-dom';
import $ from 'jquery';

export default class MenuLeft extends React.Component {
	constructor(props) {
		super(props);
		this.path = '/products/ecloud-messaging';
		this.active = false;
		this.showMenu = this.showMenu.bind(this);
		this.getMenuIcon = this.getMenuIcon.bind(this);
	}
	componentDidMount() {
		this.showMenu();
	}
	componentDidUpdate() {
		this.showMenu();
	}
	showMenu() {
		if (!this.active && this.props.data.user.session) {
			$('.menu-component').animate({
				left: 0
			}, 500);
			this.active = true;
		}
		if (!this.props.data.user.session)
			this.active = false;
	}
	getMenuIcon(icon) {
		switch (icon) {
			case 'send':
				return <i class="material-icons">&#xE163;</i>
			case 'schedule':
				return <i class="material-icons">&#xE8B5;</i>
			case 'phonelink':
				return <i class="material-icons">&#xE326;</i>
			case 'history':
				return <i class="material-icons">&#xE889;</i>
			case 'chart':
				return <i class="material-icons">&#xE24B;</i>
			case 'persons':
				return <i class="material-icons">&#xE8D3;</i>
			case 'person':
				return <i class="material-icons">&#xE7FD;</i>
			case 'settings':
				return <i class="material-icons">&#xE8B8;</i>
			default:
				return <i class="material-icons">&#xE80E;</i>

		}
	}
	render() {
		const {menu, session, name, avatar} = this.props.data.user;
		let styleAvatar = {
			backgroundImage: avatar == '0' ? 'url(./images/avatar.jpg)' : 'url(' + avatar + ')',
		}
		let styleAccount = {
			backgroundImage: 'url(./images/background-account.png)',
		}
    let menuoptions = [];
		if (menu != undefined)
			for (let i = 0; i < menu.length; i++) {
				menuoptions.push(
					<div className="menu-option">
						<Link to={this.path + menu[i].url} className={window.location.pathname == (this.path + menu[i].url) ? "active" : ''}>
							{this.getMenuIcon(menu[i].icon)}
							<span className={window.location.pathname == (this.path + menu[i].url) ? "active" : ''}>{menu[i].name}</span>
						</Link>
					</div>);
			}

		let menucomponent = (
			<div className="menu-component">
				<div className="menu-account" style={styleAccount}>
					<Link to={this.path + "/account"}>
						<div className="menu-account-avatar" style={styleAvatar}>
						</div>
						<span className={window.location.pathname == (this.path + '/account') ? "active" : ''}>{name}</span>
					</Link>
				</div>
				{menuoptions}
			</div>
		);
		return session ? menucomponent : null;
	}
}
