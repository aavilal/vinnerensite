import React from 'react';
import { Link, withRouter  } from 'react-router-dom';
import $ from 'jquery';
import AppActions from '../../../flux/actions/AppActions.jsx';

class MenuMobile extends React.Component {
	constructor(props) {
		super(props);
		this.path = '/products/ecloud-messaging';
		this.handleScroll = this.handleScroll.bind(this);
		this.showMenu = this.showMenu.bind(this);
		this.handleClickMenu = this.handleClickMenu.bind(this);
		this.handleClickLogout = this.handleClickLogout.bind(this);
		this.getMenuIcon = this.getMenuIcon.bind(this);
		this.scrollSection = this.scrollSection.bind(this);
	}
	componentDidMount() {
		const {pathname} = window.location;
    document.getElementsByClassName('main-view')[0].addEventListener("scroll", this.handleScroll, false);
		this.showMenu();
		let classEl = '';
		if (pathname == this.path + '/features')
			classEl = 'ecm-features';
		if (pathname == this.path + '/pricing')
			classEl = 'ecm-pricing';
		if (pathname == this.path +  '/contact-us')
			classEl = 'ecm-contact';
		setTimeout(() => {
				this.scrollSection(classEl);
		}, 500);
	}
	componentWillUnmount() {
		document.getElementsByClassName('main-view')[0].removeEventListener("scroll", this.handleScroll, false);
	}
	componentDidUpdate() {
		this.showMenu();
	}
	handleClickLogout() {
		this.props.toogle();
		AppActions.getLogout();
	}
	handleScroll() {
		const {pathname} = window.location;
		let urlsAllowed = [this.path + '/', this.path + '/features', this.path + '/pricing', this.path + '/contact-us'];
		if (urlsAllowed.indexOf(pathname) == -1)
			return;

		let urlScroll = '/';

		let paddingTop = parseInt($('.content-view').css('padding-top').replace('px'));
		let offsetTop = $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop;

		let headerHeight = $('.main-view').offset().top + $('.main-view').scrollTop();


		if (headerHeight >= $('.ecm-features').offset().top - $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop) {
			urlScroll = '/features';
		}
		if (headerHeight >= $('.ecm-pricing').offset().top - $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop) {
			urlScroll = '/pricing';
		}
		if (headerHeight >= $('.ecm-contact').offset().top - $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop) {
			urlScroll = '/contact-us';
		}
		if ($('.main-view').height() + $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop >= $('.content-view').height()) {
			urlScroll = '/contact-us';
		}
		if (pathname != this.path + urlScroll) {
			this.props.history.push(this.path + urlScroll);
		}

	}
	showMenu() {
		if (this.props.open) {
			$('.ecm-menu-mob-component').show(500);
		} else {
			$('.ecm-menu-mob-component').hide(500);
		}
	}
	handleClickMenu(e) {
		this.props.toogle();
		this.scrollSection(e);
	}
	scrollSection(e) {
		let scroll;
		if (e == '') {
			scroll = 0;
		} else {
			let paddingTop = parseInt($('.content-view').css('padding-top').replace('px'));
			scroll = $('.' + e).offset().top - $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop;
			scroll = scroll + 1;
		}

		$('.main-view').animate({
          scrollTop: scroll
        }, 1000, function() {
    });
	}
	getMenuIcon(icon) {
		switch (icon) {
			case 'send':
				return <i class="material-icons">&#xE163;</i>
			case 'schedule':
				return <i class="material-icons">&#xE8B5;</i>
			case 'phonelink':
				return <i class="material-icons">&#xE326;</i>
			case 'history':
				return <i class="material-icons">&#xE889;</i>
			case 'chart':
				return <i class="material-icons">&#xE24B;</i>
			case 'persons':
				return <i class="material-icons">&#xE8D3;</i>
			case 'person':
				return <i class="material-icons">&#xE7FD;</i>
			case 'settings':
				return <i class="material-icons">&#xE8B8;</i>
			default:
				return <i class="material-icons">&#xE80E;</i>

		}
	}
	render() {
		const {menu, session, name, avatar} = this.props.data.user;
		const {pathname} = window.location;

		let menucomponent = (
			<div className="ecm-menu-mob-component hide">
				<div className="menu-option">
					<Link to={this.path + "/features"} className={pathname == this.path + "/features" ? "active" : ''} onClick={()=>this.handleClickMenu('ecm-features')}>
						<span className={pathname == this.path + "/features" ? "active" : ''}>Features</span>
					</Link>
				</div>
				<div className="menu-option">
					<Link to={this.path + "/pricing"} className={pathname == this.path + "/pricing" ? "active" : ''} onClick={()=>this.handleClickMenu('ecm-pricing')}>
						<span className={pathname == this.path + "/pricing" ? "active" : ''}>Pricing</span>
					</Link>
				</div>
				<div className="menu-option">
					<Link to={this.path + "/contact-us"} className={pathname == this.path + "/contact-us" ? "active" : ''} onClick={()=>this.handleClickMenu('ecm-contact')}>
						<span className={pathname == this.path + "/contact-us" ? "active" : ''}>Contact-us</span>
					</Link>
				</div>
				<div className="menu-option">
					<Link to={this.path + "/login"} className={pathname == (this.path + '/login') ? "active" : ''} onClick={()=>this.handleClickMenu('')}>
						<span className={pathname == (this.path + '/login') ? "active" : ''}>Log in</span>
					</Link>
				</div>
			</div>
		);

		let styleAvatar = {
			backgroundImage: avatar == '0' ? 'url(./images/avatar.jpg)' : 'url(' + avatar + ')',
		}
		let styleAccount = {
			backgroundImage: 'url(./images/background-account.png)',
		}

		let menuoptions = [];
		if (menu != undefined)
			for (let i = 0; i < menu.length; i++) {
				menuoptions.push(
					<div className="menu-option">
						<Link to={this.path + menu[i].url} className={window.location.pathname == (this.path + menu[i].url) ? "active" : ''} onClick={()=>this.handleClickMenu('')}>
							{this.getMenuIcon(menu[i].icon)}
							<span className={window.location.pathname == (this.path + menu[i].url) ? "active" : ''}>{menu[i].name}</span>
						</Link>
					</div>);
			}

		menuoptions.push(
			<div className="menu-option">
				<Link to={this.path + "/"} onClick={()=>this.handleClickLogout()}>
					<i class="material-icons">&#xE8AC;</i>
					<span>Log out</span>
				</Link>
			</div>);

		let styleHeight = {
			height: window.innerHeight - 45 > 435 ? 435 : window.innerHeight - 60,
			overflow: window.innerHeight - 45 > 435 ? 'none' : 'scroll'
		}
		let menudashboard = (
			<div className="ecm-menu-mob-component hide" style={styleHeight}>
				<div className="menu-account" style={styleAccount} onClick={()=>this.handleClickMenu('')}>
					<Link to={this.path + "/account"}>
						<div className="menu-account-avatar" style={styleAvatar}>
						</div>
						<span className={window.location.pathname == (this.path + '/account') ? "active" : ''}>{name}</span>
					</Link>
				</div>
				{menuoptions}
			</div>
		);
		return session && window.location.pathname.indexOf(this.path) != -1 ? menudashboard : menucomponent;
	}
}
export default withRouter(MenuMobile);
