import React from 'react';
import { Link, withRouter  } from 'react-router-dom';
import $ from 'jquery';
import AppActions from '../../../flux/actions/AppActions.jsx';

class MenuTop extends React.Component {
	constructor(props) {
		super(props);
		this.path = '/products/ecloud-messaging';
		this.handleClickMenu = this.handleClickMenu.bind(this);
		this.handleScroll = this.handleScroll.bind(this);
		this.handleClickLogout = this.handleClickLogout.bind(this);
		this.scrollSection = this.scrollSection.bind(this);
	}
	componentDidMount() {
		const {pathname} = window.location;
    document.getElementsByClassName('main-view')[0].addEventListener("scroll", this.handleScroll, false);
		let classEl = '';
		if (pathname == this.path + '/features')
			classEl = 'ecm-features';
		if (pathname == this.path + '/pricing')
			classEl = 'ecm-pricing';
		if (pathname == this.path +  '/contact-us')
			classEl = 'ecm-contact';
		setTimeout(() => {
				this.scrollSection(classEl);
		}, 500);
	}
	componentWillUnmount() {
		document.getElementsByClassName('main-view')[0].removeEventListener("scroll", this.handleScroll, false);
	}
	handleClickLogout() {
		AppActions.getLogout();
	}
	handleScroll() {
		const {pathname} = window.location;
		let urlsAllowed = [this.path + '/', this.path + '/features', this.path + '/pricing', this.path + '/contact-us'];
		if (urlsAllowed.indexOf(pathname) == -1)
			return;

		let urlScroll = '/';

		let paddingTop = parseInt($('.content-view').css('padding-top').replace('px'));
		let offsetTop = $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop;

		let headerHeight = $('.main-view').offset().top + $('.main-view').scrollTop();


		if (headerHeight >= $('.ecm-features').offset().top - $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop) {
			urlScroll = '/features';
		}
		if (headerHeight >= $('.ecm-pricing').offset().top - $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop) {
			urlScroll = '/pricing';
		}
		if (headerHeight >= $('.ecm-contact').offset().top - $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop) {
			urlScroll = '/contact-us';
		}
		if ($('.main-view').height() + $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop >= $('.content-view').height()) {
			urlScroll = '/contact-us';
		}
		if (pathname != this.path + urlScroll) {
			this.props.history.push(this.path + urlScroll);
		}

	}
	handleClickMenu(e) {
		this.scrollSection(e);
	}
	scrollSection(e) {
		let scroll;
		if (e == '') {
			scroll = 0;
		} else {
			let paddingTop = parseInt($('.content-view').css('padding-top').replace('px'));
			scroll = $('.' + e).offset().top - $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop;
			scroll = scroll + 1;
		}

		$('.main-view').animate({
          scrollTop: scroll
        }, 1000, function() {
    });
	}
	render() {
		let menu = [];
		const {session} = this.props.data.user;
		const {pathname} = window.location;
		menu.push(
			<div className="menu-top-element">
				<Link to={this.path + "/features"} onClick={()=>this.handleClickMenu('ecm-features')}>
					<span className={pathname == this.path + "/features" ? 'active' : ''}>Features</span>
				</Link>
			</div>);
		menu.push(
			<div className="menu-top-element">
				<Link to={this.path + "/pricing"} onClick={()=>this.handleClickMenu('ecm-pricing')}>
					<span className={pathname == this.path + "/pricing" ? 'active' : ''}>Pricing</span>
				</Link>
			</div>);
		menu.push(
			<div className="menu-top-element">
				<Link to={this.path + "/contact-us"} onClick={()=>this.handleClickMenu('ecm-contact')}>
					<span className={pathname == this.path + "/contact-us" ? 'active' : ''}>Contact-us</span>
				</Link>
			</div>);
		menu.push(
			<div className="menu-top-element">
				<Link to={this.path + "/login"}>
					<span className={pathname == (this.path + '/login') ? 'active' : ''}>Log in</span>
				</Link>
			</div>);
		let logout = [];
		logout.push(
			<div className="menu-top-element" onClick={this.handleClickLogout}>
				<Link to={this.path + "/"}>
					<i class="material-icons">&#xE8AC;</i>
					<span>Log out</span>
				</Link>
			</div>);
		return (
			<div className="ecm-menu-top-component">
				{session ? logout : menu}
			</div>
		);
	}
}
export default withRouter(MenuTop);
