import React from 'react';
import { Link, withRouter  } from 'react-router-dom';
import $ from 'jquery';

class MenuTop extends React.Component {
	constructor(props) {
		super(props);
		this.handleClickMenu = this.handleClickMenu.bind(this);
		this.handleScroll = this.handleScroll.bind(this);
		this.scrollSection = this.scrollSection.bind(this);
	}
	componentDidMount() {
		const {pathname} = window.location;
    document.getElementsByClassName('main-view')[0].addEventListener("scroll", this.handleScroll, false);
		let classEl = '';
		if (pathname == '/our-services')
			classEl = 'vinneren-services';
		//if (pathname == '/products')
		//	classEl = 'vinneren-products';
		if (pathname == '/contact-us')
			classEl = 'vinneren-contact';
		setTimeout(() => {
				this.scrollSection(classEl);
		}, 500);
	}
	componentWillUnmount() {
		document.getElementsByClassName('main-view')[0].removeEventListener("scroll", this.handleScroll, false);
	}
	handleScroll() {

		let urlScroll = '/';

		let paddingTop = parseInt($('.content-view').css('padding-top').replace('px'));
		let offsetTop = $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop;

		let headerHeight = $('.main-view').offset().top + $('.main-view').scrollTop();


		if (headerHeight >= $('.vinneren-services').offset().top - $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop) {
			urlScroll = '/our-services';
		}
		//if (headerHeight >= $('.vinneren-products').offset().top - $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop) {
		//	urlScroll = '/products';
		//}
		if (headerHeight >= $('.vinneren-contact').offset().top - $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop) {
			urlScroll = '/contact-us';
		}
		if ($('.main-view').height() + $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop >= $('.content-view').height()) {
			urlScroll = '/contact-us';
		}
		if (window.location.pathname != urlScroll) {
			this.props.history.push(urlScroll);
		}


	}
	handleClickMenu(e) {
		this.scrollSection(e);
	}
	scrollSection(e) {
		if (e == '')
			return;
		let paddingTop = parseInt($('.content-view').css('padding-top').replace('px'));
		let scroll = $('.' + e).offset().top - $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop;
		$('.main-view').animate({
          scrollTop: scroll + 1
        }, 1000, function() {
    });
	}
	render() {
		let menu = [];
		const {pathname} = window.location;
		menu.push(
			<div className="menu-top-element">
				<Link to="/our-services" onClick={()=>this.handleClickMenu('vinneren-services')}>
					<span className={pathname == '/our-services' ? 'active' : ''}>Our Services</span>
				</Link>
			</div>);
		menu.push(
			<div className="menu-top-element" style={{display: 'none'}}>
				<Link to="/products" onClick={()=>this.handleClickMenu('vinneren-products')}>
					<span className={pathname == '/products' ? 'active' : ''}>Products</span>
				</Link>
			</div>);
		menu.push(
			<div className="menu-top-element">
				<Link to="/contact-us" onClick={()=>this.handleClickMenu('vinneren-contact')}>
					<span className={pathname == '/contact-us' ? 'active' : ''}>Contact-us</span>
				</Link>
			</div>);

		return (
			<div className="menu-top-component">
				{menu}
			</div>
		);
	}
}
export default withRouter(MenuTop);
