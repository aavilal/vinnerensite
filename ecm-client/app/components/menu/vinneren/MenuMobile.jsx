import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import $ from 'jquery';

class MenuMobile extends React.Component {
	constructor(props) {
		super(props);
		this.handleScroll = this.handleScroll.bind(this);
		this.showMenu = this.showMenu.bind(this);
		this.handleClickMenu = this.handleClickMenu.bind(this);
		this.scrollSection = this.scrollSection.bind(this);
	}
	componentDidMount() {
		const {pathname} = window.location;
		document.getElementsByClassName('main-view')[0].addEventListener("scroll", this.handleScroll, false);
		this.showMenu();

		let classEl = '';
		if (pathname == '/our-services')
			classEl = 'vinneren-services';
		//if (pathname == '/products')
		//	classEl = 'vinneren-products';
		if (pathname == '/contact-us')
			classEl = 'vinneren-contact';
		setTimeout(() => {
				this.scrollSection(classEl);
		}, 500);

	}
	componentWillUnmount() {
		document.getElementsByClassName('main-view')[0].removeEventListener("scroll", this.handleScroll, false);
	}
	componentDidUpdate() {
		this.showMenu();
	}
	handleScroll() {
		let urlScroll = '/';

		let paddingTop = parseInt($('.content-view').css('padding-top').replace('px'));
		let offsetTop = $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop;

		let headerHeight = $('.main-view').offset().top + $('.main-view').scrollTop();


		if (headerHeight >= $('.vinneren-services').offset().top - $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop) {
			urlScroll = '/our-services';
		}
		//if (headerHeight >= $('.vinneren-products').offset().top - $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop) {
		//	urlScroll = '/products';
		//}
		if (headerHeight >= $('.vinneren-contact').offset().top - $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop) {
			urlScroll = '/contact-us';
		}
		if ($('.main-view').height() + $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop >= $('.content-view').height()) {
			urlScroll = '/contact-us';
		}
		if (window.location.pathname != urlScroll) {
			this.props.history.push(urlScroll);
		}

	}
	showMenu() {
		if (this.props.open) {
			$('.menu-mob-component').show(500);
		} else {
			$('.menu-mob-component').hide(500);
		}
	}
	handleClickMenu(e) {
		this.props.toogle();
		this.scrollSection(e);
	}
	scrollSection(e) {
		if (e == '')
			return;
		let paddingTop = parseInt($('.content-view').css('padding-top').replace('px'));
		let scroll = $('.' + e).offset().top - $('.main-view').offset().top + $('.main-view').scrollTop() - paddingTop;
		$('.main-view').animate({
          scrollTop: scroll + 1
        }, 1000, function() {
    });
	}
	render() {
		const {pathname} = window.location;
		let menucomponent = (
			<div className="menu-mob-component hide">
				<div className="menu-option">
					<Link to="/our-services" className={pathname == '/our-services' ? "active" : ''} onClick={()=>this.handleClickMenu('vinneren-services')}>
						<span className={pathname == '/our-services' ? "active" : ''}>Our Services</span>
					</Link>
				</div>
				<div className="menu-option" style={{display: 'none'}}>
					<Link to="/products" className={pathname == '/products' ? "active" : ''} onClick={()=>this.handleClickMenu('vinneren-products')}>
						<span className={pathname == '/products' ? "active" : ''}>Products</span>
					</Link>
				</div>
				<div className="menu-option">
					<Link to="/contact-us" className={pathname == '/contact-us' ? "active" : ''} onClick={()=>this.handleClickMenu('vinneren-contact')}>
						<span className={pathname == '/contact-us' ? "active" : ''}>Contact-us</span>
					</Link>
				</div>
			</div>
		);
		return menucomponent;
	}
}
export default withRouter(MenuMobile);
