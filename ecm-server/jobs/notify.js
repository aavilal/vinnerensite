

module.exports = (app, redisClient, admin) => {
  let actualDate = new Date();
  let pendingFinish = [];
  redisClient.get('pending', (err, reply) => {
    let pendings = JSON.parse(reply);
    pendingFinish = pendingFinish.concat(pendings);
    for (let i = 0; i < pendings.length; i++) {
      let pendingDate = new Date(pendings[i].dateSend);
      let pendingTime = new Date(pendings[i].timeSend);
      if  (actualDate.getFullYear() == pendingDate.getFullYear() && actualDate.getMonth() == pendingDate.getMonth() &&
          actualDate.getDate() == pendingDate.getDate() && actualDate.getHours() == pendingTime.getHours() && actualDate.getMinutes() == pendingTime.getMinutes()) {
          let subscribersSend = [];
          redisClient.get('subscribers', (err, reply) => {
            let replyObj = JSON.parse(reply);
            /*COMMENT*/
            /*
            replyObj.push({
              token: 'fgFrm2ftLBY:APA91bEXad-Jtf_g9sylHjBvSJEUh0a2mXovmyG6HdFbsstI1uWgWeXdN-lqMBE_dNqrkjPejaY7XLaRr6rdyO0Ne4Mggk8pXMpo4f9qqWygEWdJygdTK0rHIXaLIV6g6E06vRbx2P8n'
            });
            */
            if (replyObj.length > 0) {
              let index;
              for (let i = 0; i < replyObj.length; i++) {
                index = Math.trunc(i/900);
                subscribersSend[index] = subscribersSend[index] == undefined ? [] : subscribersSend[index];
                subscribersSend[index].push(replyObj[i].token);
              }
            }

            if (subscribersSend.length == 0) {
              redisClient.get('history', (err, reply) => {
                let history = JSON.parse(reply);
                history.push(pendings[i]);
                redisClient.set('history', JSON.stringify(history), (err, reply) => {
                  let indexPending = pendingFinish.indexOf(pendings[i]);
                  if (indexPending != -1) {
                    pendingFinish.splice(indexPending, 1);
                    redisClient.set('pending', JSON.stringify(pendingFinish), (err, reply) => {});
                  }
                });
              });
            } else {

              for (let i = 0; i < subscribersSend.length; i++) {

                let messagePN = {
                  title: pendings[i].title,
                  body: pendings[i].body,
                  bucketIconUrl: pendings[i].bucketIconUrl,
                  bucketImgUrl: pendings[i].bucketImgUrl,
                  url: pendings[i].url,
                  id: pendings[i].id
                }

                admin.messaging().sendToDevice(subscribersSend[i], {data: messagePN})
                .then(function(response) {
                  redisClient.get('history', (err, reply) => {
                    let history = JSON.parse(reply);
                    history.push(pendings[i]);
                    redisClient.set('history', JSON.stringify(history), (err, reply) => {
                      let indexPending = pendingFinish.indexOf(pendings[i]);
                      if (indexPending != -1) {
                        pendingFinish.splice(indexPending, 1);
                        redisClient.set('pending', JSON.stringify(pendingFinish), (err, reply) => {});
                      }
                    });
                  });
                })
                .catch(function(error) {
                });
              }
            }

          });
      }
    }
  });
}
