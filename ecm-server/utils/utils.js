import jwt from 'jsonwebtoken';

export function getDateId() {
  let date = new Date();
  let id = date.getFullYear() + '';
  id += (date.getMonth() > 9 ? '' : '0') + (date.getMonth() + 1);
  id += (date.getDate() > 9 ? '' : '0') + date.getDate();
  id += (date.getHours() > 9 ? '' : '0') + date.getHours();
  id += (date.getMinutes() > 9 ? '' : '0') + date.getMinutes();
  id += (date.getSeconds() > 9 ? '' : '0') + date.getSeconds();
  id += (date.getMilliseconds() < 10 ? '00' : date.getMilliseconds() < 100 ? '0' : '0') + date.getSeconds();
  return id;
}

export function getToken(data, secret) {
  let token = jwt.sign({
    exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24),
    email: data,
  }, secret);
  return token;
}

export function getTokenPwd(data, secret) {
  let token = jwt.sign({
    exp: Math.floor(Date.now() / 1000) + (60 * 30),
    id: data,
  }, secret);
  return token;
}

export function checksession(mysqlConn, data, secret, _callback) {
  let email = '';
  if (data == undefined) {
    _callback(null);
    return;
  } else {
    try {
      email = data.split(' ')[1];
    } catch(err) {
      _callback(null);
      return;
    }
  }

  jwt.verify(email, secret, (errjwt, decoded) => {
    if (errjwt) {
      _callback(null);
    } else {
      let response;
      mysqlConn.query('SELECT * FROM ent_users WHERE email = "' + decoded.email + '"', (err, result, fields) => {
    		if (err) {
          _callback(null);
          return;
        }
        if (result.length > 0) {
          delete result[0].password;
          result[0].token = getToken(result[0].email, secret);
          _callback(result[0]);
          return;
        } else {
          _callback(null);
          return;
        }
    	});
    }
  });
}
/*
export function sendNotify(redisClient, admin, message) {
  let subscribersSend = [];
  let response;

  redisClient.get('subscribers', (err, reply) => {
    let replyObj = JSON.parse(reply);
    if (replyObj.length == 0) {
      response = {
        success: false,
        message: 'No subscribers'
      }
      return response;
    }
    let index;
    for (let i = 0; i < replyObj.length; i++) {
      index = Math.trunc(i/900);
      subscribersSend[index].push(replyObj[i].token);
    }

    if (subscribersSend.length == 0) {
      response = {
        success: false,
        message: 'No subscribers'
      }
      return response;
    }

    for (let i = 0; i < subscribersSend.length; i++) {
      admin.messaging().sendToDevice(subscribersSend[i], message)
      .then(function(response) {
        redisClient.get('history', (err, reply) => {
          let history = JSON.parse(reply);
          let dateReg = new Date();
          history.push({
            message: message,
            date: dateReg
          });
          redisClient.set('history', JSON.stringify(history), (err, reply) => {
            response = {
              success: true,
              message: 'Notification sent.'
            }
            return response;
          });
        });
      })
      .catch(function(error) {
        console.log('ERR', error);
        response = {
          success: false,
          message: 'Error sending notification.'
        }
        return response;
      });
    }

  });
}
*/
