
module.exports = (app, mysqlConn) => {
  app.post('/api/menu-options', (req, res) => {
    let response;
    mysqlConn.query('SELECT me.* FROM cat_menu me WHERE me.active = 1', (err, result, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error getting menu options.',
          menuOptions: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      response = {
        success: true,
        message: '',
        menuOptions: result
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(response));
      return;
    });
  });
  app.post('/api/profiles', (req, res) => {
    let response;
    mysqlConn.query('SELECT p.*, IFNULL(pm.id_cat_menu, 0) id_cat_menu FROM cat_profile p LEFT JOIN rel_cat_profile_cat_menu pm ON p.id_cat_profile = pm.id_cat_profile ORDER BY p.id_cat_profile, p.name', (err, result, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error getting profiles.',
          profiles: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      let obj = [];
      let lastProfile = null;
      for (let i = 0; i < result.length; i++) {
        if (lastProfile != result[i].id_cat_profile) {
          obj.push({
            id_cat_profile: result[i].id_cat_profile,
            name: result[i].name,
            description: result[i].description,
            menu_options: []
          });
          lastProfile = result[i].id_cat_profile;
        }
        if (result[i].id_cat_menu != 0) {
          obj[obj.length -1].menu_options.push(result[i].id_cat_menu);
        }

      }
      response = {
        success: true,
        message: '',
        profiles: obj
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(response));
      return;
    });
  });
  app.post('/api/add/profiles', (req, res) => {
    const { name, description, menuOptions} = req.body;

    let response;
    let query = 'INSERT INTO cat_profile (name, description) VALUES ("';
    query += name + '","' + description + '")';
    mysqlConn.query(query, (err, result, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error saving profile.',
          profiles: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      let menuOp = menuOptions.split(',')
      query = 'INSERT INTO rel_cat_profile_cat_menu (id_cat_profile, id_cat_menu) VALUES ?';
      let values = [];
      for (let i = 0; i < menuOp.length; i++) {
        values.push([result.insertId, menuOp[i]]);
      }
      mysqlConn.query(query, [values], (err, result, fields) => {
        mysqlConn.query('SELECT p.*, IFNULL(pm.id_cat_menu, 0) id_cat_menu FROM cat_profile p LEFT JOIN rel_cat_profile_cat_menu pm ON p.id_cat_profile = pm.id_cat_profile ORDER BY p.id_cat_profile, p.name', (err, result, fields) => {
          let obj = [];
          let lastProfile = null;
          for (let i = 0; i < result.length; i++) {
            if (lastProfile != result[i].id_cat_profile) {
              obj.push({
                id_cat_profile: result[i].id_cat_profile,
                name: result[i].name,
                description: result[i].description,
                menu_options: []
              });
              lastProfile = result[i].id_cat_profile;
            }
            if (result[i].id_cat_menu != 0) {
              obj[obj.length -1].menu_options.push(result[i].id_cat_menu);
            }

          }
          response = {
            success: true,
            message: 'Profile added.',
            profiles: obj
          }
          res.status(200);
          res.setHeader('Content-Type', 'application/json');
          res.send(JSON.stringify(response));
          return;
        });
      });

    });

  });
  app.post('/api/mod/profiles', (req, res) => {
    const {id, name, description, menuOptions} = req.body;

    let response;
    let query = 'UPDATE cat_profile set name = "' + name + '", description = "' + description + '" WHERE id_cat_profile=' + id;
    mysqlConn.query(query, (err, result, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error updating profile.',
          profiles: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      query = 'DELETE FROM rel_cat_profile_cat_menu WHERE id_cat_profile = ' + id;
      mysqlConn.query(query, (err, result, fields) => {
        let menuOp = menuOptions.split(',')
        query = 'INSERT INTO rel_cat_profile_cat_menu (id_cat_profile, id_cat_menu) VALUES ?';
        let values = [];
        for (let i = 0; i < menuOp.length; i++) {
          values.push([id, menuOp[i]]);
        }
        mysqlConn.query(query, [values], (err, result, fields) => {
          mysqlConn.query('SELECT p.*, IFNULL(pm.id_cat_menu, 0) id_cat_menu FROM cat_profile p LEFT JOIN rel_cat_profile_cat_menu pm ON p.id_cat_profile = pm.id_cat_profile ORDER BY p.id_cat_profile, p.name', (err, result, fields) => {
            let obj = [];
            let lastProfile = null;
            for (let i = 0; i < result.length; i++) {
              if (lastProfile != result[i].id_cat_profile) {
                obj.push({
                  id_cat_profile: result[i].id_cat_profile,
                  name: result[i].name,
                  description: result[i].description,
                  menu_options: []
                });
                lastProfile = result[i].id_cat_profile;
              }
              if (result[i].id_cat_menu != 0) {
                obj[obj.length -1].menu_options.push(result[i].id_cat_menu);
              }

            }
            response = {
              success: true,
              message: 'Profile updated.',
              profiles: obj
            }
            res.status(200);
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(response));
            return;
          });
        });
      });


    });


  });

  app.post('/api/del/profiles', (req, res) => {
    const { id } = req.body;
    let response;
    let query = "SELECT COUNT(*) count_users FROM ent_users WHERE id_cat_profile = " + id;
    mysqlConn.query(query, (err, result_count, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error deleting profile.',
          profiles: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      if (result_count[0].count_users > 0) {
        response = {
          success: false,
          message: 'Error deleting profile, ' + result_count[0].count_users + ' users has this profile.',
          profiles: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;

      } else {
        query = "DELETE FROM rel_cat_profile_cat_menu WHERE id_cat_profile = " + id;
        mysqlConn.query(query, (err, result, fields) => {
          query = "DELETE FROM cat_profile WHERE id_cat_profile = " + id;
          mysqlConn.query(query, (err, result, fields) => {
            mysqlConn.query('SELECT p.*, IFNULL(pm.id_cat_menu, 0) id_cat_menu FROM cat_profile p LEFT JOIN rel_cat_profile_cat_menu pm ON p.id_cat_profile = pm.id_cat_profile ORDER BY p.id_cat_profile, p.name', (err, result, fields) => {
              let obj = [];
              let lastProfile = null;
              for (let i = 0; i < result.length; i++) {
                if (lastProfile != result[i].id_cat_profile) {
                  obj.push({
                    id_cat_profile: result[i].id_cat_profile,
                    name: result[i].name,
                    description: result[i].description,
                    menu_options: []
                  });
                  lastProfile = result[i].id_cat_profile;
                }
                if (result[i].id_cat_menu != 0) {
                  obj[obj.length -1].menu_options.push(result[i].id_cat_menu);
                }

              }
              response = {
                success: true,
                message: 'Profile deleted.',
                profiles: obj
              }
              res.status(200);
              res.setHeader('Content-Type', 'application/json');
              res.send(JSON.stringify(response));
              return;
            });
          });
        });
      }


    });


  });

}
