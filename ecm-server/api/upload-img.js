import path from 'path';
import { getDateId } from '../utils/utils.js';

const MAX_SIZE = 1000000;
const MAX_WIDTH = 300;

module.exports = (app, upload, s3) => {
  app.post('/api/upload/image/:id', upload, (req, res) => {

    if (!req.files) {
      res.status(415);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify({
        success: false,
        type: req.files.imgNotifyFU ? 'imgNotifyFU' : req.files.iconNotifyFU ? 'iconNotifyFU' : req.files.imageAskFU ? 'imageAskFU' : 'avatar',
        message: "Couldn't upload file"
      }));
      return;
    }
    if (req.files.iconNotifyFU || req.files.imgNotifyFU || req.files.imageAskFU || req.files.avatarAccount) {

      if (req.params.id != '0') {
        let deleteParams = {
          Bucket: process.env.S3_BUCKET,
          Delete: {
            Objects: [{
              Key: (req.files.iconNotifyFU || req.files.imgNotifyFU ? 'pushed/' : req.files.imageAskFU ? 'ask/' : 'avatars/') + req.params.id
            }]
          }
        };
        s3.deleteObjects(deleteParams, (err, data) => {
        });
      }

      let bucketFolder = req.files.iconNotifyFU || req.files.imgNotifyFU ? 'pushed/' : req.files.imageAskFU ? 'ask/' : 'avatars/';

        let origin = req.files.iconNotifyFU ? req.files.iconNotifyFU[0] : req.files.imgNotifyFU ? req.files.imgNotifyFU[0] : req.files.imageAskFU ? req.files.imageAskFU[0] : req.files.avatarAccount[0];
        let uploadParams = {
          Bucket: process.env.S3_BUCKET,
          Key: bucketFolder + getDateId() + origin.originalname.substr(origin.originalname.lastIndexOf('.')),
          Body: origin.buffer,
          ACL: 'public-read',
          ContentType: 'application/octet-stream'
        };

        s3.upload(uploadParams, (err, data) => {
          if (err) {
            res.status(415);
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify({
              success: false,
              type: req.files.imgNotifyFU ? 'imgNotifyFU' : req.files.iconNotifyFU ? 'iconNotifyFU' : req.files.imageAskFU ? 'imageAskFU' : 'avatar',
              message: "Couldn't upload file"
            }));
            return;
          } else {
            let response = {
              type: req.files.imgNotifyFU ? 'imgNotifyFU' : req.files.iconNotifyFU ? 'iconNotifyFU' : req.files.imageAskFU ? 'imageAskFU' : 'avatar',
              bucketImgUrl: data.Location,
              bucketImgName: path.basename(data.Location),
              imgName: origin.originalname
            }
            res.status(200);
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(response));
            return;
          }
        });
      } else {
        res.status(415);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify({
          success: false,
          type: req.files.imgNotifyFU ? 'imgNotifyFU' : req.files.iconNotifyFU ? 'iconNotifyFU' : req.files.imageAskFU ? 'imageAskFU' : 'avatar',
          message: "Couldn't upload file"
        }));
        return;
      }
  });
}
