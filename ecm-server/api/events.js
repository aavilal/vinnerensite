
module.exports = (app, mysqlConn) => {

  app.post('/api/click', (req, res) => {
    const { ecmid } = req.body;
    let response;
    let query = 'UPDATE ent_history set clicks = clicks + 1 WHERE hash="' + ecmid + '"';
    mysqlConn.query(query, (err, result, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error updating click notification.'
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      response = {
        success: true,
        message: 'Click saved.',
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(response));
      return;
    });
  });
}
