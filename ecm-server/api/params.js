
module.exports = (app, mysqlConn) => {
  app.get('/api/params', (req, res) => {
    let response;
    mysqlConn.query('SELECT * FROM cat_param WHERE type = "ASK_NOT" ORDER BY type ASC, id_cat_param ASC', (err, result, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error getting params.',
          params: []
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      response = {
        success: true,
        message: '',
        params: result
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(response));
      return;
    });
  });

  app.post('/api/params', (req, res) => {
      const { params } = req.body;
      let paramObj = JSON.parse(params);
      let response;
      let query = '';
      for (let i = 0; i < paramObj.length; i++) {
        query += 'UPDATE cat_param SET value = "' + paramObj[i].value + '" WHERE type = "ASK_NOT" AND param = "' + paramObj[i].param + '"; ';
      }
      mysqlConn.query(query, (err, result, fields) => {
    		if (err) {
          response = {
            success: false,
            message: 'Error ocurred updating params.'
          }
          res.status(200);
          res.setHeader('Content-Type', 'application/json');
          res.send(JSON.stringify(response));
          return;
        }
        response = {
          success: false,
          message: 'Params updated.'
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
    	});
  });

}
