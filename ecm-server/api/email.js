import config from '../config/config.js';
import nodemailer from 'nodemailer';
import { getTokenPwd } from '../utils/utils.js';

module.exports = (app, mysqlConn, secret) => {

  app.post('/api/contact-us', (req, res) => {
    const { name, email, message } = req.body;

    let transporter = nodemailer.createTransport({
      host: config.data.smtp.host,
      port: config.data.smtp.port,
      secureConnection: true,
      auth: {
          user: config.data.contact,
          pass: config.data.contactpwd
      }
    });

    let mailOptions = {
      from: config.data.contact,
      to: config.data.emails.toString(),
      subject: 'Vinneren Contact',
      html: '<div style="width: 100%; padding: 10px 5px; background: #132A4F;"><img src="https://s3.us-east-2.amazonaws.com/ecloud-messaging/logo/vinneren-logo.png" style="height: 40px;"/></div><div><h1>Client Contact</h1></br></br></br><h4>Name:</h4> <span>' + name + '</span></br></br><h4>Email:</h4> <span>' + email + '</span></br></br><h4>Message:</h4> <span>' + message + '</span></div>'
    };
    let response;
    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        response = {
          success: false,
          message: 'Email not sent.'
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      } else {
        response = {
          success: true,
          message: 'Email sent.'
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
    });

  });

  app.post('/api/forgot-pwd', (req, res) => {
    const { email } = req.body;

    let response;
    mysqlConn.query('SELECT * FROM ent_users WHERE email = "' + email + '"', (err, result, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error ocurred validating email.'
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      if (result.length > 0) {

        let linkPwd = (req.headers.host.indexOf('localhost') != -1 ? 'http://' : 'https://') + req.headers.host + '/products/ecloud-messaging/update-password?u=' + getTokenPwd(result[0].id_ent_users, secret);

        let transporter = nodemailer.createTransport({
          host: config.data.smtp.host,
          port: config.data.smtp.port,
          secureConnection: true,
          auth: {
              user: config.data.contact,
              pass: config.data.contactpwd
          }
        });

        let mailOptions = {
          from: config.data.contact,
          to: email,
          subject: 'Vinneren Password Recovery',
          html: '<div style="width: 100%; padding: 10px 5px; background: #132A4F;"><img src="https://s3.us-east-2.amazonaws.com/ecloud-messaging/logo/ecm-logo.png" style="width: 50%;"/></div><div><h1>User Password Recovery</h1></br></br></br><h5>Click in link to reset user password:</h5> <span>' + linkPwd + '</span></br></br><h5>*Link is only valid for the next 30 minutes.</h5></div>'
        };

        transporter.sendMail(mailOptions, function(error, info){
          if (error) {
            response = {
              success: false,
              message: 'Email for password recovery not sent.'
            }
            res.status(200);
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(response));
            return;
          } else {
            response = {
              success: true,
              message: 'Check email for password recovery.'
            }
            res.status(200);
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(response));
            return;
          }
        });

      } else {
        response = {
          success: false,
          message: 'Email user invalid.'
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
    });

  });
}
