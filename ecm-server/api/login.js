import bcrypt from 'bcrypt';
import { getToken, checksession } from '../utils/utils.js';
import jwt from 'jsonwebtoken';
//let h = bcrypt.hashSync('admin123456', 10);
//console.log(bcrypt.compareSync('admin1234567', h2));
module.exports = (app, mysqlConn, secret) => {

  app.post('/api/login', (req, res) => {
    const { email, password } = req.body;
      let response;

      mysqlConn.query('SELECT * FROM ent_users WHERE email = "' + email + '"', (err, result, fields) => {
    		if (err) {
          response = {
            success: false,
            message: 'Error ocurred retrieving data',
            user: null
          }
          res.status(200);
          res.setHeader('Content-Type', 'application/json');
          res.send(JSON.stringify(response));
          return;
        }
        if (result.length > 0 && bcrypt.compareSync(password, result[0].password)) {
          delete result[0].password;
          result[0].token = getToken(result[0].email, secret);
          response = {
            success: true,
            message: '',
            user: result[0]
          }
          res.status(200);
          res.setHeader('Content-Type', 'application/json');
          res.send(JSON.stringify(response));
          return;
        } else {
          response = {
            success: false,
            message: 'User or password invalid',
            user: null
          }
          res.status(200);
          res.setHeader('Content-Type', 'application/json');
          res.send(JSON.stringify(response));
          return;
        }
    	});

  });

  app.post('/api/update/user', (req, res) => {
    checksession(mysqlConn, req.headers.authorization, secret, (userAuth) => {
      if (userAuth != null) {
        const { name, password, passwordold, avatar, avatarName } = req.body;

        let response;

        mysqlConn.query('SELECT * FROM ent_users WHERE email = "' + userAuth.email + '"', (err, result, fields) => {
      		if (err) {
            response = {
              success: false,
              message: 'Error ocurred updating user.',
              user: null
            }
            res.status(200);
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(response));
            return;
          }
          if (bcrypt.compareSync(passwordold, result[0].password)) {
            mysqlConn.query('UPDATE ent_users SET name = "' + name + '", password = "' + bcrypt.hashSync(password, 10) + '", avatar = "' + avatar + '" WHERE email = "' + userAuth.email + '"', (err, result, fields) => {
              response = {
                success: true,
                message: 'User updated.',
                user: {
                  name, password, avatar, avatarName
                }
              }
              res.status(200);
              res.setHeader('Content-Type', 'application/json');
              res.send(JSON.stringify(response));
              return;
          	});
          } else {
            response = {
              success: false,
              pwdinvalid: true,
              message: 'Invalid password',
              user: null
            }
            res.status(200);
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(response));
            return;
          }
        });


      } else {
        res.status(401);
        res.send('Unauthorized.');
      }
    });
  });

  app.post('/api/logininfo', (req, res) => {
    checksession(mysqlConn, req.headers.authorization, secret, (user) => {
      if (user != null) {
        let response = {
          success: true,
          user: user
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
      } else {
        res.status(401);
        res.send('Unauthorized.');
      }
    });
  });

  app.post('/api/menu', (req, res) => {
    const { profileuser } = req.body;
    checksession(mysqlConn, req.headers.authorization, secret, (user) => {
      if (user != null) {
        let response;
        mysqlConn.query('SELECT me.* FROM rel_cat_profile_cat_menu cm, cat_menu me WHERE me.id_cat_menu = cm.id_cat_menu AND me.active = 1 AND cm.id_cat_profile = "' + profileuser + '" ORDER BY me.order', (err, result, fields) => {
      		if (err) {
            response = {
              success: false,
              message: 'Error ocurred retrieving menu',
              profile: null
            }
            res.status(200);
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(response));
            return;
          }
          response = {
            success: true,
            message: null,
            profile: {
              menu: result
            }
          }
          res.status(200);
          res.setHeader('Content-Type', 'application/json');
          res.send(JSON.stringify(response));
          return;
      	});
      } else {
        res.status(401);
        res.send('Unauthorized.');
      }
    });
  });

  app.post('/api/validate-url', (req, res) => {
    const { id } = req.body;
    let response;
    jwt.verify(id, secret, (errjwt, decoded) => {
      if (errjwt) {
        response = {
          success: false,
          message: errjwt.message.indexOf('expired') != -1 ? 'Time expired.' : 'Invalid url.'
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      } else {
        let response;
        mysqlConn.query('SELECT * FROM ent_users WHERE id_ent_users = "' + decoded.id + '"', (err, result, fields) => {
      		if (err) {
            response = {
              success: false,
              message: 'Error ocurred validating url.'
            }
            res.status(200);
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(response));
            return;
          }
          if (result.length > 0) {
            response = {
              success: true,
              message: ''
            }
            res.status(200);
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(response));
            return;
          } else {
            response = {
              success: false,
              message: "Invalid token for reset password."
            }
            res.status(200);
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(response));
            return;
          }
      	});
      }
    });
  });

  app.post('/api/update-password', (req, res) => {
    const { id, password } = req.body;
    let response;
    jwt.verify(id, secret, (errjwt, decoded) => {
      if (errjwt) {
        response = {
          success: false,
          message: errjwt.message.indexOf('expired') != -1 ? 'Time expired.' : 'Invalid url.'
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      } else {
        let response;
        mysqlConn.query('SELECT * FROM ent_users WHERE id_ent_users = "' + decoded.id + '"', (err, result, fields) => {
      		if (err) {
            response = {
              success: false,
              message: 'Error ocurred validating user.'
            }
            res.status(200);
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(response));
            return;
          }
          if (result.length > 0) {
            mysqlConn.query('UPDATE ent_users SET password = "' + bcrypt.hashSync(password, 10) + '" WHERE id_ent_users = "' + decoded.id + '"', (err, result, fields) => {
              response = {
                success: true,
                message: 'Password reset successfully.'
              }
              res.status(200);
              res.setHeader('Content-Type', 'application/json');
              res.send(JSON.stringify(response));
              return;
            });
          } else {
            response = {
              success: false,
              message: "User doesn't exists."
            }
            res.status(200);
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(response));
            return;
          }
      	});
      }
    });
  });
}
