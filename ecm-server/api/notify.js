import { sendNotify } from '../utils/utils.js';
import uniqid from 'uniqid';

module.exports = (app, mysqlConn, admin) => {
  app.get('/api/schedule-notify', (req, res) => {
    let response;
    mysqlConn.query('SELECT * FROM ent_pending ORDER BY date_send ASC, time_send ASC', (err, result, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error getting pending notifications.',
          pending: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      response = {
        success: true,
        message: '',
        pending: result
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(response));
      return;
    });
  });
  app.post('/api/add/schedule-notify', (req, res) => {
    const { title, body, iconName, bucketIconName, bucketIconUrl, imgName, bucketImgName, bucketImgUrl, dateSend, timeSend, url } = req.body;
    let id = uniqid();
    let type = 'Scheduled';
    let clicks = 0;
    let message = {
      id,
      dateSend,
      timeSend,
      title,
      body,
      iconName,
      bucketIconName,
      bucketIconUrl,
      imgName,
      bucketImgName,
      bucketImgUrl,
      type,
      url,
      clicks
    }

    let response;
    let query = 'INSERT INTO ent_pending (hash, date_send, time_send, title, body, icon_name, bucket_icon_name, bucket_icon_url, img_name, bucket_img_name, bucket_img_url, type, url, clicks) VALUES ("';
    query += id + '","' + dateSend + '","' + timeSend + '","' + title + '","' + body + '","' + iconName + '","' + bucketIconName + '","' + bucketIconUrl + '","' + imgName + '","' + bucketImgName + '","' + bucketImgUrl + '","' + type + '","' + url + '",0)';
    mysqlConn.query(query, (err, result, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error saving pending notification.',
          pending: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      mysqlConn.query('SELECT * FROM ent_pending ORDER BY date_send ASC, time_send ASC', (err, result, fields) => {
        response = {
          success: true,
          message: 'Notification added.',
          pending: result
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      });
    });
  });
  app.post('/api/mod/schedule-notify', (req, res) => {
    const { id, title, body, iconName, bucketIconName, bucketIconUrl, imgName, bucketImgName, bucketImgUrl, dateSend, timeSend, type, url } = req.body;
    let clicks = 0;
    let message = {
      id,
      dateSend,
      timeSend,
      title,
      body,
      iconName,
      bucketIconName,
      bucketIconUrl,
      imgName,
      bucketImgName,
      bucketImgUrl,
      type,
      url,
      clicks
    }

    let response;
    let query = 'UPDATE ent_pending set date_send="' + dateSend + '", time_send="' + timeSend + '", title="' + title + '", body="' + body + '", icon_name="' + iconName + '", bucket_icon_name="' + bucketIconName + '", bucket_icon_url="' + bucketIconUrl + '", img_name="' + imgName + '", bucket_img_name="' + bucketImgName + '", bucket_img_url="' + bucketImgUrl + '", type="' + type + '", url="' + url + '"';
    query += " WHERE hash='" + id + "';"
    mysqlConn.query(query, (err, result, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error updating pending notification.',
          pending: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      mysqlConn.query('SELECT * FROM ent_pending ORDER BY date_send ASC, time_send ASC', (err, result, fields) => {
        response = {
          success: true,
          message: 'Notification schedule updated.',
          pending: result
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      });
    });

  });

  app.post('/api/del/schedule-notify', (req, res) => {
    const { id } = req.body;
    let response;
    let query = "DELETE FROM ent_pending WHERE hash='" + id + "'";
    mysqlConn.query(query, (err, result, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error deleting pending notification.',
          pending: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      mysqlConn.query('SELECT * FROM ent_pending ORDER BY date_send ASC, time_send ASC', (err, result, fields) => {
        response = {
          success: true,
          message: 'Notification schedule deleted.',
          pending: result
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      });
    });


  });

  app.post('/api/quick-notify', (req, res) => {
    const { title, body, iconName, bucketIconName, bucketIconUrl, imgName, bucketImgName, bucketImgUrl, url } = req.body;
    let id = uniqid();
    let dateSend = new Date();
    let timeSend = new Date();

    let dateS = dateSend.getFullYear() + '-';
    dateS += (dateSend.getMonth() + 1 < 10 ? '0' : '') + (dateSend.getMonth() + 1) + '-';
    dateS += (dateSend.getSeconds() < 10 ? '0' : '') + dateSend.getDate();

    let timeS = timeSend.getFullYear() + '-';
    timeS += (timeSend.getMonth() + 1 < 10 ? '0' : '') + (timeSend.getMonth() + 1) + '-';
    timeS += (timeSend.getSeconds() < 10 ? '0' : '') + timeSend.getDate() + ' ';
    timeS += (timeSend.getHours() < 10 ? '0' : '') + timeSend.getHours() + ':';
    timeS += (timeSend.getMinutes() < 10 ? '0' : '') + timeSend.getMinutes() + ':';
    timeS += (timeSend.getSeconds() < 10 ? '0' : '') + timeSend.getSeconds();

    dateSend = dateS;
    timeSend = timeS;

    let type = 'Quick';
    let clicks = 0;
    let message = {
      id,
      dateSend,
      timeSend,
      title,
      body,
      iconName,
      bucketIconName,
      bucketIconUrl,
      imgName,
      bucketImgName,
      bucketImgUrl,
      type,
      url,
      clicks
    }
    let subscribersSend = [];
    let response;
    mysqlConn.query('SELECT * FROM subscribers ', (err, result, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error getting subscribers.'
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      if (result.length == 0) {
        response = {
          success: false,
          message: 'No subscribers.'
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }

      let index;
      for (let i = 0; i < result.length; i++) {
        index = Math.trunc(i/900);
        subscribersSend[index] = subscribersSend[index] == undefined ? [] : subscribersSend[index];
        subscribersSend[index].push(result[i].token);
      }

      if (subscribersSend.length == 0) {
        response = {
          success: false,
          message: 'No subscribers.'
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }

      let messagePN = {
        title: message.title,
        body: message.body,
        bucketIconUrl: message.bucketIconUrl,
        bucketImgUrl: message.bucketImgUrl,
        url: message.url,
        id: message.id
      }
      for (let i = 0; i < subscribersSend.length; i++) {
        admin.messaging().sendToDevice(subscribersSend[i], {data: messagePN})
        .then(function(response) {
          let query = 'INSERT INTO ent_history (hash, date_send, time_send, title, body, icon_name, bucket_icon_name, bucket_icon_url, img_name, bucket_img_name, bucket_img_url, type, url, clicks) VALUES ("';
          query += message.id + '","' + message.dateSend + '","' + message.timeSend + '","' + message.title + '","' + message.body + '","' + message.iconName + '","' + message.bucketIconName + '","' + message.bucketIconUrl + '","' + message.imgName + '","' + message.bucketImgName + '","' + message.bucketImgUrl + '","' + message.type + '","' + message.url + '",0)';
          mysqlConn.query(query, (err, result, fields) => {
            response = {
              success: true,
              message: 'Notification sent.'
            }
            res.status(200);
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(response));
            return;
          });
        })
        .catch(function(error) {
          response = {
            success: false,
            message: 'Error sending notification.'
          }
          res.status(200);
          res.setHeader('Content-Type', 'application/json');
          res.send(JSON.stringify(response));
          return;
        });
      }

    });

  });

}
