import { sendNotify } from '../utils/utils.js';
import uniqid from 'uniqid';

module.exports = (app, mysqlConn) => {
  app.get('/api/history', (req, res) => {

    let response;
    mysqlConn.query('SELECT * FROM ent_history ORDER BY date_send ASC, time_send ASC', (err, result, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error getting history notifications.',
          history: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      response = {
        success: true,
        message: '',
        history: result
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(response));
      return;
    });
  });

  app.get('/api/count-clicks', (req, res) => {

    let response;
    let actualDate = new Date();
    let firstDayWeek = new Date();
    let lastDayWeek = new Date();
    let firstday = firstDayWeek.getDay();
    let lastday = lastDayWeek.getDay();
    if (firstday != 0) {
      firstDayWeek.setHours(-24 * firstday);
    }
    if (lastday != 6) {
      lastDayWeek.setHours(24 * (6 - lastday));
    }

    let firstDayMonth = new Date(actualDate.getFullYear(), actualDate.getMonth(), 1);
    let lastDayMonth = new Date(actualDate.getFullYear(), actualDate.getMonth() + 1, 0);

    let actualDateS = actualDate.getFullYear() + '-' + (actualDate.getMonth() + 1 < 10 ? '0' : '') + (actualDate.getMonth() + 1) + '-' + (actualDate.getDate() < 10 ? '0' : '') + actualDate.getDate();
    let firstDayWeekS = firstDayWeek.getFullYear() + '-' + (firstDayWeek.getMonth() + 1 < 10 ? '0' : '') + (firstDayWeek.getMonth() + 1) + '-' + (firstDayWeek.getDate() < 10 ? '0' : '') + firstDayWeek.getDate();
    let lastDayWeekS = lastDayWeek.getFullYear() + '-' + (lastDayWeek.getMonth() + 1 < 10 ? '0' : '') + (lastDayWeek.getMonth() + 1) + '-' + (lastDayWeek.getDate() < 10 ? '0' : '') + lastDayWeek.getDate();
    let firstDayMonthS = firstDayMonth.getFullYear() + '-' + (firstDayMonth.getMonth() + 1 < 10 ? '0' : '') + (firstDayMonth.getMonth() + 1) + '-' + (firstDayMonth.getDate() < 10 ? '0' : '') + firstDayMonth.getDate();
    let lastDayMonthS = lastDayMonth.getFullYear() + '-' + (lastDayMonth.getMonth() + 1 < 10 ? '0' : '') + (lastDayMonth.getMonth() + 1) + '-' + (lastDayMonth.getDate() < 10 ? '0' : '') + lastDayMonth.getDate();

    let query = 'SELECT * FROM (SELECT IFNULL(SUM(clicks), 0) clicks_month';
    query += ' FROM ent_history WHERE date_send BETWEEN "' + firstDayMonthS + '" AND "' + lastDayMonthS + '") m,';
    query += ' (SELECT IFNULL(SUM(clicks), 0) clicks_week FROM ent_history WHERE date_send BETWEEN "' + firstDayWeekS + '" AND "' + lastDayWeekS + '") w,';
    query += ' (SELECT IFNULL(SUM(clicks), 0) clicks_today FROM ent_history WHERE date_send = "' + actualDateS + '") t';

    mysqlConn.query(query, (err, result, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error getting clicks notifications.'+ err,
          clicks: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      response = {
        success: true,
        message: '',
        clicks: result[0]
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(response));
      return;
    });

  });

}
