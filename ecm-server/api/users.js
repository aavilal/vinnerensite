import bcrypt from 'bcrypt';

module.exports = (app, mysqlConn) => {
  app.post('/api/users-app', (req, res) => {
    let response;
    mysqlConn.query('SELECT p.name profile_name, u.* FROM ent_users u, cat_profile p WHERE u.id_cat_profile = p.id_cat_profile', (err, result, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error getting users.',
          usersapp: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      response = {
        success: true,
        message: '',
        usersapp: result
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(response));
      return;
    });
  });

  app.post('/api/add/users-app', (req, res) => {
    const { name, email, profileId } = req.body;
    const password = 'ecloud123456';
    let response;
    mysqlConn.query('SELECT COUNT(*) count_users FROM ent_users WHERE email = "' + email + '"', (err, result, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error adding user.',
          usersapp: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      if (result[0].count_users  > 0) {
        response = {
          success: false,
          message: 'This user already exists.',
          usersapp: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      let query = 'INSERT INTO ent_users (email, password, name, avatar, id_cat_profile, avatar_name) VALUES ("';
      query += email + '","' + bcrypt.hashSync(password, 10) + '","' + name + '","0",' + profileId+ ',"0")';
      mysqlConn.query(query, (err, result, fields) => {
        mysqlConn.query('SELECT p.name profile_name, u.* FROM ent_users u, cat_profile p WHERE u.id_cat_profile = p.id_cat_profile', (err, result, fields) => {
          response = {
            success: true,
            message: 'User added.',
            usersapp: result
          }
          res.status(200);
          res.setHeader('Content-Type', 'application/json');
          res.send(JSON.stringify(response));
          return;
        });
      });
    });

  });

  app.post('/api/mod/users-app', (req, res) => {
    const { id,  profileId } = req.body;

    let response;
    mysqlConn.query('SELECT COUNT(*) count_users FROM ent_users WHERE id_ent_users = ' + id, (err, result, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error updating user.',
          usersapp: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      if (result[0].count_users == 0) {
        response = {
          success: false,
          message: "This user doesn't exist.",
          usersapp: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      let query = 'UPDATE ent_users SET id_cat_profile = ' + profileId+ ' WHERE id_ent_users = ' + id;
      mysqlConn.query(query, (err, result, fields) => {
        mysqlConn.query('SELECT p.name profile_name, u.* FROM ent_users u, cat_profile p WHERE u.id_cat_profile = p.id_cat_profile', (err, result, fields) => {
          response = {
            success: true,
            message: 'User updated.',
            usersapp: result
          }
          res.status(200);
          res.setHeader('Content-Type', 'application/json');
          res.send(JSON.stringify(response));
          return;
        });
      });
    });

  });

  app.post('/api/del/users-app', (req, res) => {
    const { id } = req.body;

    let response;
    mysqlConn.query('SELECT COUNT(*) count_users FROM ent_users WHERE id_ent_users = ' + id, (err, result, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error deleting user.',
          usersapp: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      if (result[0].count_users == 0) {
        response = {
          success: false,
          message: "This user doesn't exist.",
          usersapp: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      let query = 'DELETE FROM ent_users WHERE id_ent_users = ' + id;
      mysqlConn.query(query, (err, result, fields) => {
        mysqlConn.query('SELECT p.name profile_name, u.* FROM ent_users u, cat_profile p WHERE u.id_cat_profile = p.id_cat_profile', (err, result, fields) => {
          response = {
            success: true,
            message: 'User deleted.',
            usersapp: result
          }
          res.status(200);
          res.setHeader('Content-Type', 'application/json');
          res.send(JSON.stringify(response));
          return;
        });
      });
    });

  });

}
