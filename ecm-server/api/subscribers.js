module.exports = (app, mysqlConn) => {
  app.get('/api/subscribers', (req, res) => {
    let response;
    mysqlConn.query('SELECT * FROM ent_subscribers', (err, result, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error getting subscribers.',
          subscribers: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      response = {
        success: true,
        message: '',
        subscribers: result
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(response));
      return;
    });
  });

  app.post('/api/subscribers', (req, res) => {
    const { token, osname, osversion, browsername, browserversion } = req.body;
    let response;
    mysqlConn.query('SELECT * FROM ent_subscribers WHERE token = "' + token + '"', (err, result, fields) => {
      if (err) {
        response = {
          success: false,
          message: 'Error saving subscriber.',
          subscribers: null
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      }
      if (result.length > 0) {
        response = {
          success: true,
          message: 'Subscriber already exists.'
        }
        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(response));
        return;
      } else {
        let actualDate = new Date();
        let dateRegistered = actualDate.getFullYear() + '-' + (actualDate.getMonth() + 1 < 10 ? '0' : '') + (actualDate.getMonth() + 1) + '-' + (actualDate.getDate() < 10 ? '0' : '') + actualDate.getDate();
        let query = 'INSERT INTO ent_subscribers (token, os_name, os_version, browser_name, browser_version, date_registered) VALUES ("';
        query += token + '","' + osname + '","' + osversion + '","' + browsername + '","' + browserversion + '","' + dateRegistered + '")';
        mysqlConn.query(query, (err, result, fields) => {

          response = {
            success: true,
            message: 'Subscriber added.'
          }
          res.status(200);
          res.setHeader('Content-Type', 'application/json');
          res.send(JSON.stringify(response));
          return;
        });
      }

    });
  });
}
