import express from 'express';
import multer from 'multer';
import bodyParser from 'body-parser';
import methodOverride from 'method-override';
import bcrypt from 'bcrypt';
import mysql from 'mysql';
import AWS from 'aws-sdk';
import paramsApi from './api/params.js';
import emailApi from './api/email.js';
import loginApi from './api/login.js';
import uploadApi from './api/upload-img.js';
import subscribersApi from './api/subscribers.js';
import notifyApi from './api/notify.js';
import eventsApi from './api/events.js';
import historyApi from './api/history.js';
import usersApi from './api/users.js';
import profilesApi from './api/profiles.js';
import notifyJob from './jobs/notify.js';
import admin from 'firebase-admin';
import serviceAccount from './config/ecloud-messaging-firebase-adminsdk-iimm1-eae5587407.json';
import cron from 'node-cron';
const secret = 'alan140402';

const app = express();
const port = process.env.PORT;

let dirname = __dirname.replace('/ecm-server', '');
dirname = dirname.replace('\\ecm-server', '');

let Router = express.Router();
let enableCrossDomain = (req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Credentials", "true");
	next();
};

let mysqlConn = mysql.createConnection({
	host: process.env.DB_ENDPOINT,
	user: process.env.DB_USER,
	password: process.env.DB_PWD,
	database: process.env.DB_NAME,
	multipleStatements: true
});


AWS.config.update({
	region: 'us-east-2'
});
let s3 = new AWS.S3({
	apiVersion: '2006-03-01'
});

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

app.use(bodyParser.urlencoded({ extended: false }))
	 .use(bodyParser.json())
	 .use(bodyParser.json({ type: 'application/vnd.api+json' }))
	 .use(methodOverride('X-HTTP-Method-Override'))
	 .use(enableCrossDomain)
	 .use(Router);

let upload = multer().fields([{ name: 'imgNotifyFU'}, {name:'iconNotifyFU'}, {name:'avatarAccount'}, { name: 'imageAskFU'}]);

loginApi(app, mysqlConn, secret);
uploadApi(app, upload, s3);
notifyApi(app, mysqlConn, admin);
subscribersApi(app, mysqlConn);
historyApi(app, mysqlConn);
eventsApi(app, mysqlConn);
usersApi(app, mysqlConn);
profilesApi(app, mysqlConn);
emailApi(app, mysqlConn, secret);
paramsApi(app, mysqlConn);
app.use(express.static(dirname + '/ecm-client/static/'))
	.get('*', (req, res) => {
		//res.writeHead(200);
		//if (req.protocol.indexOf('https') == -1 && process.env.NODE_ENV == 'production') {
		//	res.redirect('https://' + req.headers.host + req.url);
		//}
		res.sendFile(dirname + '/ecm-client/static/index.html')
	}).listen(port, () => {
		console.log(`[APP] Listening on port => ${port}`)
	});
/*
cron.schedule('* * * * *', () => {
	notifyJob(app, redisClient, admin);
});
*/
