var https = require('https');
var fs = require('fs');
var https_options = {
 ca: fs.readFileSync("gd_bundle-g2-g1.crt"),
 key: fs.readFileSync("privateKey.key"),
 cert: fs.readFileSync("d42c69f9f1959245.crt")
};
https.createServer(https_options, function (req, res) {
 res.writeHead(200);
 res.end("Welcome to Node.js HTTPS Server");
}).listen(9000, function() {
  console.log('Server Start');
})
